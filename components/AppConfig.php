<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\Url;
use app\modules\settings\modules\core\models\Config;
use app\modules\settings\modules\core\models\School;
/**
 * Mailer.
 *
 * @author AJ
 */
class AppConfig extends Component
{
    public function schoolLogoUrl(){
      $model=Config::find()->where(['field'=>'school_logo'])->one();
       if(($model==null) || ($model->value==null) ||(!isset($model->value))){
       return false;
    }else{
      $base=Url::base(true);
      return $base.$model->value;
     }
    }
    

    public function districtLogoUrl(){
      $model=Config::find()->where(['field'=>'district_logo'])->one();
      if(($model==null) || ($model->value==null) ||(!isset($model->value))){
       return false;
      }else{
      $base=Url::base(true);
      return $base.$model->value;
     }
    }
    public function setConfigDateTime($datetime){
    	$model=Config::find()->where(['field'=>'date_format'])->one();
		  if(($model==null) || ($model->value==null) ||(!isset($model->value))){
       return date('Y-m-d',strtotime($dt)); 
      }else{
   
     return date(''.$date->value.' '.'H:i:s',strtotime($$date->value));
     }
    }

    public function setConfigDate($dt){
      $model=Config::find()->where(['field'=>'date_format'])->one();
      if(($model==null) || ($model->value==null) ||(!isset($model->value))){
       return date('Y-m-d',strtotime($dt)); 
      }else{
        $base=Url::base(true);
      return date(''.$model->value.'',strtotime($dt)); 
     }
    }

    public function getGoogleAnalyticId(){
      $model=Config::find()->where(['field'=>'google_analytic'])->one();
     if(($model==null) || ($model->value==null) ||(!isset($model->value))){
       return false;
    }else{
       return $model->value;
     }
    }

    public function getFavicon(){
      $model=Config::find()->where(['field'=>'favicon'])->one();
     if(($model==null) || ($model->value==null) ||(!isset($model->value))){
       return false;
    }else{
      $base=Url::base(true);
      return $base.$model->value;
     }
    }

     public function getComapnyLogo(){
      $model=Config::find()->where(['field'=>'company_logo'])->one();
    if(($model==null) || ($model->value==null) ||(!isset($model->value))){
       return false;
    }else{
      $base=Url::base(true);
      return $base.$model->value;
     }
    }

    public function getMailHeaderLogo(){
      $model=Config::find()->where(['field'=>'mail_header_logo'])->one();
    if(($model==null) || ($model->value==null) ||(!isset($model->value))){
       return false;
    }else{
      $base=Url::base(true);
      return $base.$model->value;
     }
    }

     public function getMailFooterLogo(){
      $model=Config::find()->where(['field'=>'mail_footer_logo'])->one();
     if(($model==null) || ($model->value==null) ||(!isset($model->value))){
       return false;
    }else{
      $base=Url::base(true);
      return $base.$model->value;
     }
    }

    public function getConfirmAccountExpiryTime(){
      $model=Config::find()->where(['field'=>'confirm_account_time'])->one();

      if(($model==null) || ($model->value==null) ||(!isset($model->value))){
        return 259200;
      }else{
        return $model->value;
      }
    }

    public function getRecoveryAccountExpiryTime(){
      $model=Config::find()->where(['field'=>'recovery_account_time'])->one();

      if(($model==null) || ($model->value==null) ||(!isset($model->value))){
        return 259200;
      }else{
        return $model->value;
      }
    }
    

      
}