<?php
/*
* This file is part of the Dektrium project.
*
* (c) Dektrium project <http://github.com/dektrium/>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/
namespace app\components;
use Yii;
use yii\base\Component;
use yii\helpers\Url;
use app\models\SocialPlugins;
use yii\base\BootstrapInterface;
use app\modules\settings\modules\core\models\Config;
use \kartik\datecontrol\Module;
/**
* Social Plugins for login,likes.
*
* @author AJ
*/
class YiiConfig  implements BootstrapInterface
{
 static $viewurl=1;
public function bootstrap($app)
{
$date=Config::find()->where(['field'=>'date_format'])->one();
if($date!==null){
\Yii::$app->formatter->dateFormat= $date->value;
}
$date=Config::find()->where(['field'=>'date_format'])->one();
if($date!==null){
\Yii::$app->formatter->datetimeFormat= $date->value.' '.'hh:ii';
}
$language=Config::find()->where(['field'=>'langauges'])->one();
if($language!==null){
\Yii::$app->language = $language->value;
}
$timezone=Config::find()->where(['field'=>'time_zone'])->one();
if($timezone!==null){
\Yii::$app->timeZone=$timezone->value;
\Yii::$app->formatter->timeZone=$timezone->value;
}

      
}

 
}