<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\Url;
use app\modules\social_plugins\modules\core\models\SocialPlugins;
use yii\base\BootstrapInterface;
//use yii\authClient\Collection;

/**
 * Social Plugins for login,likes.
 *
 * @author AJ
 */
class Sociallogin implements BootstrapInterface
{
    
    public function bootstrap($app)
    {
       $model= SocialPlugins::find()->where(['status'=>SocialPlugins::ACTIVE])->asArray()->all(); 
       $retVal=[];
       $data=[];
       for ($i=0; $i < count($model); $i++) { 
           $title=$model[$i]['title'];
           $retVal[$i]['class']=$model[$i]['class'];
           $retVal[$i]['clientId']=$model[$i]['client_id'];
           $retVal[$i]['clientSecret']=$model[$i]['client_secret_key'];
           $data[$title]=$retVal[$i];
       }

       $app->set('authClientCollection', [
        'class' =>'yii\authclient\Collection',
        'clients' => $data,
       
     ]);
    }
   

   

    
}