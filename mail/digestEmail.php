<?php
use yii\helpers\Url;
use yii\helpers\Html;

?>


<div id="email-template-container" style="background-color: #FAFCFE;box-shadow: 0 0 8px rgba(0, 0, 0, 0.5);
    margin: 0 auto;
    max-width: 1250px;
    min-height: 100%;
    position: relative;
    margin-top: 10px;
    ">
<tr><td height="30"></td></tr>
<ul id="foxeslab-template-elements" class="ui-sortable" style="list-style-type: none;">

<li style="width: 1140px; list-style-type: none;" class="ui-sortable-handle">


<!-- section-1 "navbar" -->
<table class="table_full editable-bg-color bg_color_ffffff editable-bg-image" mc:repeatable="castellab" mc:variant="Header" style="background-image: url(#); background-position: top center; background-repeat: no-repeat; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" bgcolor="#ffffff" background="#" width="920" cellspacing="0" cellpadding="0" align="center">
	<tbody><tr>
		<td >
			<table class="table1" style="margin: 0 auto;" border="0" width="1140" cellspacing="0" cellpadding="0" align="center">
				<tbody><tr><td height="30"></td></tr>
				<tr>
					<td style="background-color: #ffffff;" >
						<!-- Logo -->
						<table class="tablet_no_float" border="0" width="300" cellspacing="0" cellpadding="0" align="left">
							<tbody><tr>
								<td class="editable-img">
									<a href="https://dems.pulsedu.com">
										<img editable="true" mc:edit="image101" class="center_image" src="https://dems.pulsedu.com/images/logo-final.png" style="display:block; line-height:0; font-size:0; border:0;margin-top:10px;margin-bottom:10px;margin-left:60px;" alt="image" border="0">
									</a>
								</td>

							</tr>
							
						</tbody></table><!-- END logo -->
                        <table class="tablet_no_float" border="0" width="200" cellspacing="0" cellpadding="0" align="right">
							<tbody><tr>
								<td class="editable-img">
									<a href="https://dems.pulsedu.com">
										<img editable="true" mc:edit="image101" class="center_image" src="http://dems.pulsedu.com/images/signin.jpg" style="display:block; line-height:0; font-size:0; border:0;margin-top:10px;margin-bottom:10px;margin-left:10px;height:59px;width:140px;" alt="image" border="0">
									</a>
								</td>
								
							</tr>
							
						</tbody></table>

						<!-- Nav menu -->
						<!-- END nav menu -->
					</td>
				</tr>
			</tbody></table>
		</td>
	</tr>
</tbody></table><!-- END wrapper -->

</li>
<li style="width: 1140px; list-style-type: none;" class="ui-sortable-handle">
<div style="background-color: #f5f5f5;height: 10px;"></div>
</li>
<li style="width: 1140px; list-style-type: none;" class="ui-sortable-handle">


<!-- section-3 -->
<table class="table_full editable-bg-color bg_color_ffffff editable-bg-image" mc:repeatable="castellab" mc:variant="Header" style="background-image: url(#); background-position: top center; background-repeat: no-repeat; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" border="0" bgcolor="#ffffff" background="#" width="1140" cellspacing="0" cellpadding="0" align="center">
	<tbody><tr>
		<td>
			<table class="table1" style="margin: 0 auto;" border="0" width=940 cellspacing="0" cellpadding="0" align="center">
				<!-- padding-top -->
				<tbody><tr><td height="60"></td></tr>
				<!-- heading -->
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="0" align="center">
							<tbody><tr>
								<td style="padding-right: 10px;" align="left"><img src="http://emails.castellab.com/shop/images/line-1.png" alt=""></td>
								<td mc:edit="text301" class="text_color_282828" style="color: #282828; font-size: 24px; font-weight: 500; font-family: Raleway, Helvetica, sans-serif; mso-line-height-rule: exactly;" align="center">
									<div class="editable-text"><multiline>
										<span class="text_container">Notifications</span>
									</multiline></div>
								</td>
								<td style="padding-right: 10px;" align="left"><img src="http://emails.castellab.com/shop/images/line-1.png" alt=""></td>
							</tr>
						</tbody></table>
					</td>
				</tr>
				<tr>
				<td>
				
<?php for ($i=0,$j=1; $i < count($params) ; $i++,$j++) { ?>

    
 <div style="border-top:2px solid #eceeef;
    padding: -1.25rem;
    vertical-align: top;font-family:Arial,sans-serif;
    font-size: 16px;
    font-weight: 400;
    line-height: 1.5;
    background-color: #ffffff;
    
     
  "><img editable="true" mc:edit="image101" class="center_image" src="http://dems.pulsedu.com/images/bell.jpeg"
 style=" line-height:0; font-size:0; border:0;width:45px;height:20px;" alt="image" border="0"><?php echo $params[$i];?></div>
				<?php }?>
				</td>
				</tr>
				
</li>
<tr><td height="30"></td></tr>

</ul></div>
