<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;

/**
 * @var dektrium\user\models\User  $user
 * @var dektrium\user\models\Token $token
 */
?>
<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title> Email</title>
    <style>
    /* -------------------------------------
        INLINED WITH htmlemail.io/inline
    ------------------------------------- */
    /* -------------------------------------
        RESPONSIVE AND MOBILE FRIENDLY STYLES
    ------------------------------------- */
    @media only screen and (max-width: 620px) {
      table[class=body] h1 {
        font-size: 28px !important;
        margin-bottom: 10px !important;
      }
      table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
        font-size: 16px !important;
      }
      table[class=body] .wrapper,
            table[class=body] .article {
        padding: 10px !important;
      }
      table[class=body] .content {
        padding: 0 !important;
      }
      table[class=body] .container {
        padding: 0 !important;
        width: 100% !important;
      }
      table[class=body] .main {
        border-left-width: 0 !important;
        border-radius: 0 !important;
        border-right-width: 0 !important;
      }
      table[class=body] .btn table {
        width: 100% !important;
      }
      table[class=body] .btn a {
        width: 100% !important;
      }
      table[class=body] .img-responsive {
        height: auto !important;
        max-width: 100% !important;
        width: auto !important;
      }
    }
    /* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
    ------------------------------------- */
    @media all {
      .ExternalClass {
        width: 100%;
      }
      .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
        line-height: 100%;
      }
      .apple-link a {
        color: inherit !important;
        font-family: inherit !important;
        font-size: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
        text-decoration: none !important;
      }
      .btn-primary table td:hover {
        background-color: #34495e !important;
      }
      .btn-primary a:hover {
        background-color: #34495e !important;
        border-color: #34495e !important;
      }
    }
    </style>
  </head>
  <body class="" style="background-color: #f6f6f6; font-family: serif; -webkit-font-smoothing: antialiased; font-size: 15px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="background:#f3f5f9;border:0;height:100%!important;margin:0;padding:0;width:100%!important">
      <tr>
        <td style="font-family: serif; font-size: 15px; vertical-align: top;">&nbsp;</td>
        <td class="container" style="font-family: serif; font-size: 15px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
          <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">

            <!-- START CENTERED WHITE CONTAINER -->
            <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">This is preheader text. Some clients will show this text as a preview.</span>
            <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">

              <!-- START MAIN CONTENT AREA -->
              <tr>

                <td class="wrapper" style="font-family: serif; font-size: 15px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                    <tr>
                      <td style="font-family: serif; font-size: 15px; vertical-align: top;">
                          <table style="background:#ffffff;border:0" width="600" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
                      <tbody><tr>
                      <td class="m_-1543331826549450111m_6392379812293050482headerContent" style="border:none;font-family:'Segoe UI','Trebuchet MS',arial,Helvetica,serif;font-size:34px;font-weight:bold;line-height:100%;padding:10px;text-align:center;vertical-align:middle;background-color: #dff0d8;" valign="middle" align="center">
                      <img alt="" src="<?php echo Yii::$app->appconfig->getMailHeaderLogo() ?>" style="border:0;height:60px;line-height:100%;max-width:600px;outline:none;text-decoration:none" align="absmiddle" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 879.8px; top: 317.6px;"><div id=":km" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" ><div class="aSK J-J5-Ji aYr"></div></div></div>
                      </td>
                      </tr>
                      </tbody></table>
                      &nbsp;&nbsp;
                      
                        <p style="font-family: serif; font-size: 15px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Dear User<br></p>
                        <p style="font-family: serif; font-size: 15px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Thank you for signing up. In order to complete your registration, please click the button below.</p>
                      
						<table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;">
                          <tbody>
                            <tr>


                              <td align="left" style="font-family: serif; font-size: 15px; vertical-align: top; padding-bottom: 15px;">
                                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
                                  <tbody>
                                    <tr>
                                      <td style="font-family: serif; font-size: 15px; vertical-align: top; background-color: #3498db; border-radius: 5px; text-align: center;"> <a href="<?=  $message; ?>" target="_blank" style="display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 15px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db;">Activate My Account</a> </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <p style="font-family: serif; font-size: 15px; font-weight: normal; margin: 0; Margin-bottom: 15px;">If you cannot click the link, please try pasting the text into your browser.</p>
                        <p style="font-family: serif; font-size: 15px; font-weight: normal; margin: 0; Margin-bottom: 15px;">If you did not make this request you can ignore this email.</p>
                      </td>
                    </tr>
                  </table>
                  <hr>
                    <table style="background:#ffffff;border:0" width="600" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
<tbody><tr>
<td class="m_-1543331826549450111m_6392379812293050482headerContent" style="border:none;font-family:'Segoe UI','Trebuchet MS',arial,Helvetica,serif;font-size:34px;font-weight:bold;line-height:100%;padding:0;text-align:center;vertical-align:middle;background-color: #dff0d8; " valign="middle" align="center">
<img alt="" src="<?php echo Yii::$app->appconfig->getMailfooterLogo() ?>" style="border:0;height:60px;line-height:100%;max-width:600px;outline:none;text-decoration:none" align="absmiddle" class="CToWUd a6T" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 879.8px; top: 317.6px;"><div id=":km" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" ><div class="aSK J-J5-Ji aYr"></div></div></div>
</td>
</tr>
</tbody></table>
&nbsp;&nbsp;
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->
            </table>

            

          <!-- END CENTERED WHITE CONTAINER -->
          </div>
        </td>
        <td style="font-family: serif; font-size: 15px; vertical-align: top;">&nbsp;</td>
      </tr>
    </table>
  </body>
</html>

