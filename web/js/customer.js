$(function(){
    var filemanager = $('.filemanager'),
		breadcrumbs = $('.breadcrumbs'),
		fileList = filemanager.find('.data');
    var id = $('a.folderBtn').attr("data-id"); 
    
        $.ajax({
          type:'POST',
          dataType: 'json',
          url:baseUrl+'/site/get-files?id='+id,      
          success: function(data){
             fileList.empty();
                console.log(data); 
                if(data.list.length==0){
                	icon = '<span class="icon file f-file">.file</span>';
                	var file=$('<div class="alert alert-info alert-block">'+
				             
				             '<h4 class="alert-heading">Info!</h4>'+
				              'No record found Please create folder or upload files'+
			                '</div>');
				
                    file.appendTo(fileList); 
                       $(".backBtn").css("visibility", "hidden");
                       $(".fileBtn").css("visibility", "hidden");
                }
                if(data.id==0){
                	 $(".backBtn").css("visibility", "hidden");
                   $(".fileBtn").css("visibility", "hidden");
                }
                $.each(data.list, function(i) {
                	console.log(data.list[i].name);
                	var fileType=data.list[i].type
                	var name=data.list[i].name
                	var id=data.list[i].id
                	var fileSize=111;
                  var ext=data.list[i].mime_type;
                  var type='file f-'+data.list[i].mime_type;
                  if(fileType=='folder'){
                    ext='';
                    type='folder full';
                  }
                	icon = '<span class="icon '+type+'">'+ext+'</span>';
					        var file = $('<li class="'+fileType+'"><a href="#" data="'+id+'" title="'+ name +'" class="files" id="'+fileType+'">'+icon+'<span class="name">'+ name +'</span> <span class="details">'+fileSize+'</span></a></li>');
					        file.appendTo(fileList); 
                });
                
                   }
        });
        fileList.on('dblclick', 'a#folder', function(e){
			     e.preventDefault();
          var id = $(this).attr("data");
          $.ajax({
	          type:'POST',
	          dataType: 'json',
	          url:baseUrl+'/site/get-files?id='+id,     
	          success: function(data){
             fileList.empty();
                if(data.list.length==0){
                	icon = '<span class="icon file f-file">.file</span>';
                	var file=$('<div class="alert alert-info alert-block">'+
				             
				            '<h4 class="alert-heading">Info!</h4>'+
				              'No record found Please create folder or upload files'+
			                '</div>');
			
                    file.appendTo(fileList); 
                    
                }
                if(data.id!=0){
                	$(".backBtn").css("visibility", "visible");
                  $(".fileBtn").css("visibility", "visible");
					        $('.backBtn').attr('data-id', id);
				        	$('.folderBtn').attr('data-id', id);
				        	$('.fileBtn').attr('data-id', data.id);
                }
                $.each(data.list, function( i) {
                
                	var fileType=data.list[i].type
                	var name=data.list[i].name
                	var id=data.list[i].id
                	var fileSize=111;
                    
                  var ext=data.list[i].mime_type;
                  var type='file f-'+data.list[i].mime_type;
                  if(fileType=='folder'){
                    ext='';
                    type='folder full';
                  }
                  icon = '<span class="icon '+type+'">'+ext+'</span>';
        					var file = $('<li class="'+fileType+'"><a href="#" data="'+id+'" title="'+ name +'" class="files" id="'+fileType+'">'+icon+'<span class="name">'+ name +'</span> <span class="details">'+fileSize+'</span></a></li>');
				        	file.appendTo(fileList); 
                });
                
                   }
        });

		});

		$('.backBtn').on('click',function(){
			var id = $(this).attr("data-id");
		
           $.ajax({
	          type:'POST',
	          dataType: 'json',
	          url:baseUrl+'/site/get-back-files?id='+id,     
	          success: function(data){
             fileList.empty();
                if(data.list.length==0){
                	icon = '<span class="icon file f-file">.file</span>';
                	var file=$('<div class="alert alert-info alert-block">'+
				              '<h4 class="alert-heading">Info!</h4>'+
				              'No record found Please create folder or upload files'+
			                '</div>');
			                file.appendTo(fileList); 
                    
                }
                $('.backBtn').attr('data-id', data.id);
                $('.folderBtn').attr('data-id', data.id);
                $('.fileBtn').attr('data-id', data.id);
                if(data.id!=0){
                	$(".backBtn").css("visibility", "visible");
                  $(".fileBtn").css("visibility", "visible");
					
                }else{
                	$(".backBtn").css("visibility", "hidden");
                  $(".fileBtn").css("visibility", "hidden");
                }
                $.each(data.list, function( i) {
               
                	var fileType=data.list[i].type
                	var name=data.list[i].name
                	var id=data.list[i].id
                	var fileSize=111;
                  var ext=data.list[i].mime_type;
                  var type='file f-'+data.list[i].mime_type;
                  if(fileType=='folder'){
                    ext='';
                    type='folder full';
                  }
                  icon = '<span class="icon '+type+'">'+ext+'</span>';

					var file = $('<li class="'+fileType+'"><a href="#" data="'+id+'" title="'+ name +'" class="files" id="'+fileType+'">'+icon+'<span class="name">'+ name +'</span> <span class="details">'+fileSize+'</span></a></li>');
					file.appendTo(fileList); 
                });
                
                   }
        });
  });

});
function getFolders(){

  var filemanager = $('.filemanager'),
    breadcrumbs = $('.breadcrumbs'),
    fileList = filemanager.find('.data');
    fileList = filemanager.find('.data');

  // Start by fetching the file data from scan.php with an AJAX request
        var id = $('a.folderBtn').attr("data-id");
 
        $.ajax({
          type:'POST',
          dataType: 'json',
          url:baseUrl+'/site/get-files?id='+id, 
          success: function(data){
             fileList.empty();
                console.log(data); 
                if(data.list.length==0){
                  icon = '<span class="icon file f-file">.file</span>';
                  var file=$('<div class="alert alert-info alert-block">'+
                     
                     '<h4 class="alert-heading">Info!</h4>'+
                      'No record found Please create folder or upload files'+
                      '</div>');
        
                    file.appendTo(fileList); 

                }
                if(id==0){
                   $(".backBtn").css("visibility", "hidden");
                   $(".fileBtn").css("visibility", "hidden");
                }
                $.each(data.list, function(i) {
                  console.log(data.list[i].name);
                  var fileType=data.list[i].type
                  var name=data.list[i].name
                  var id=data.list[i].id
                  var fileSize=111;
                   var ext=data.list[i].mime_type;
                   var type='file f-'+data.list[i].mime_type;
                  if(fileType=='folder'){
                    ext='';
                    type='folder full';
                  }
                  icon = '<span class="icon '+type+'">'+ext+'</span>';
          var file = $('<li class="'+fileType+'"><a href="#" data="'+id+'" title="'+ name +'"  id="'+fileType+'">'+icon+'<span class="name">'+ name +'</span> <span class="details">'+fileSize+'</span></a></li>');
          file.appendTo(fileList); 
                });
                
                   }
        });

}
$(function(){
    $.contextMenu({
        selector: '#file', 
        trigger: 'right',
        callback: function(key, options) {
             if(key=='share'){
              var id=$(this).attr("data");
              $.ajax({
                type:'POST',
                url:baseUrl+'/site/share-file',  
                data:"&id=" +id,     
                success: function(data)
                   {
                  
                        $('.modal-body').html(data);
                        $('#draggable').modal();
                       // $('#tbltree-id').val(id);
                   }
         });
          }
        },
        items: {
            "edit": {name: "Edit", icon: "edit"},
            "rename": {name: "Rename", icon: "rename"},
            "share": {name: "Share", icon: "share"},
            "cut": {name: "Cut", icon: "cut"},
            "copy": {name: "Copy", icon: "copy"},
            "paste": {name: "Paste", icon: "paste"},
            "delete": {name: "Delete", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Quit", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
    $.contextMenu({
        selector: '#folder', 
        trigger: 'right',
        
        callback: function(key, options) {
          alert(key);
          if(key=='share'){
            alert('w');
             $.ajax({
           type:'POST',
          url:baseUrl+'/site/share-folder',  
          data:"&id=" +id,     
          success: function(data)
                   {
                  
                        $('.modal-body').html(data);
                        $('#draggable').modal();
                       // $('#tbltree-id').val(id);
                   }
         });
          }
            // var m = "clicked: " + key;
            // window.console && console.log(m) || alert(m); 
        },
        items: {
            "edit": {name: "Edit", icon: "edit"},
            "rename": {name: "Rename", icon: "rename"},
            "share": {name: "Share", icon: "share"},
            "delete": {name: "Delete", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Quit", icon: function($element, key, item){ return 'context-menu-icon context-menu-icon-quit'; }}
        }
    });
});
$(document).ready(function(){
$('.folderBtn').on('click',function(){
      var id = $(this).attr("data-id");
      $.ajax({
          type:'POST',
          url:baseUrl+'/site/create-folder',  
          data:"&id=" +id,     
          success: function(data)
                   {
                  
                        $('.modal-body').html(data);
                        $('#draggable').modal();
                        $('#tbltree-id').val(id);
                   }
        });
    })
$('.fileBtn').on('click',function(){
      var id = $(this).attr("data-id");
      $.ajax({
          type:'POST',
          url:baseUrl+'/site/file-upload',  
          data:"&id=" +id,     
          success: function(data)
                   {
                  
                        $('.modal-body').html(data);
                        $('#draggable').modal();
                        $('#tbltree-id').val(id);
                   }
        });
    })


   });