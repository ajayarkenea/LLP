/*
* @Author: ajkosh
* @Date:   2018-07-12 13:34:05
* @Last Modified by:   ajkosh
* @Last Modified time: 2018-09-01 12:39:27
*/
$(document).ready(function(){
 if (active) {
 	$("#tabactive1").addClass("active");
 }

 $('#contact-form').on('beforeSubmit', function(e) {
      var form = $(this);
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
          return false;
     }
     // submit form
     $.ajax({
          url: baseUrl+'/site/contact',  
          type: 'post',
          data: form.serialize(),
          success: function (response) {
          	console.log(response);
          	   	if(response=='message'){
          	   	   $(form).trigger("reset");
          	 		toastr.remove();
              		toastr["success"]('Thank you for contacting us,Our team will get back you shortly');
             	}else{
              toastr.remove();
               toastr["error"]('error occured..Please Try Again');
              }
              toastr.options = {
              "closeButton": true,
              "debug": false,
              "newestOnTop": false,
              "progressBar": true,
              "positionClass": "toast-bottom-left",
              "preventDuplicates": true,
              "onclick": null,
              "showDuration": "900",
              "hideDuration": "1000",
              "timeOut": "9000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "slideDown",
              "hideMethod": "slideUp"
               }
          	    var $submit = form.find(':submit');
    			$submit.html('Submit');
    			$submit.prop('disabled', false);
          }
     });
     return false;
});

$('.tab').on('click',function(){
     var id = $(this).attr("data");    
      $.ajax({
          type:'POST',
          url:baseUrl+'/training/technology-group/get-technologies?id='+id,      
          success: function(data)
          {
            $('.tab-content').html(data);
          }
      });
  });

 $('#registeration-user_type').on('change',function(){
      var type= $('#registeration-user_type').val();
        if(type==2){
           $('.company_name').hide();
        }else{
          $('.company_name').show();
        }
    })
});
