
<?php
use \kartik\datecontrol\Module;
$config = [
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
    'log',
   

    // [
    //       'class' => \mgcode\sessionWarning\components\SessionWarningBootstrap::className(),
    //       'initMessages' => true,
    //   ],
    ],
  //   'controllerMap' => [
  //   'session-warning' => [
  //     'class' => 'mgcode\sessionWarning\controllers\SessionWarningController',
  //   ],
  // ],
    //'language' => 'en-US',
   // 'sourceLanguage' => 'en-US',

    //Module
    'modules' => [
        //RBAC Module
        'admin' => [
          'class' => 'mdm\admin\Module',
        ],

        'gridview'=>[
        'class'=>'\kartik\grid\Module',
        // other module settings
        ],
         'datecontrol' =>  [
      'class' => 'kartik\datecontrol\Module',
      
    ],
       'audit' => [
      'class' => 'bedezign\yii2\audit\Audit',
      // the layout that should be applied for views within this module
      'layout' => 'main',
      // Name of the component to use for database access
      'db' => 'db', 
      // List of actions to track. '*' is allowed as the last character to use as wildcard
      'trackActions' => ['*'], 
      // Actions to ignore. '*' is allowed as the last character to use as wildcard (eg 'debug/*')
      'ignoreActions' => ['audit/*', 'debug/*'],
      // Maximum age (in days) of the audit entries before they are truncated
      'maxAge' => 'debug',
      // IP address or list of IP addresses with access to the viewer, null for everyone (if the IP matches)
      'accessIps' => ['127.0.0.1', '192.168.*'], 
      // Role or list of roles with access to the viewer, null for everyone (if the user matches)
      'accessRoles' => ['admin','Admission Manager Role'],
      // User ID or list of user IDs with access to the viewer, null for everyone (if the role matches)
      'accessUsers' => [1],
      // Compress extra data generated or just keep in text? For people who don't like binary data in the DB
      'compressData' => true,
      // The callback to use to convert a user id into an identifier (username, email, ...). Can also be html.
      'userIdentifierCallback' => ['app\modules\user_management\modules\core\models\User', 'userIdentifierCallback'],
      // If the value is a simple string, it is the identifier of an internal to activate (with default settings)
      // If the entry is a '<key>' => '<string>|<array>' it is a new panel. It can optionally override a core panel or add a new one.
      'panels' => [
        'audit/log',
        'audit/request',
        'audit/error',
        'audit/trail',
        'audit/mail',
        'audit/db',
        'audit/profiling',
        'audit/javascript',
        'audit/extra',
        'audit/curl',
        
        
      ],
      'panelsMerge' => [
        // ... merge data (see below)
        ]
      ],
      'treemanager' =>  [
        'class' => '\kartik\tree\Module',

        // other module settings, refer detailed documentation
    ],

    ],

    'components' => [
       's3' => [
        'class' => 'frostealth\yii2\aws\s3\Service',
        'credentials' => [ // Aws\Credentials\CredentialsInterface|array|callable
            'key' => 'AKIAIOEMNY2ANX7KRBKA',
            'secret' => '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',
        ],
        'region' => 'us-west-2',
        'defaultBucket' => 'llp-arkenea',
        'defaultAcl' => 'public-read',
    ],
       'cache' => [
        'class' => 'yii\caching\FileCache',
      ],
          'assetManager' => [
        'bundles' => [
            'dosamigos\google\maps\MapAsset' => [
                'options' => [
                    'key' => 'AIzaSyCMM-WfN9XyfjZ92R_kGmtPve2XcXj-NGM',
                    'language' => 'id',
                    'version' => '3.1.18'
                ]
            ]
        ]
    ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '/messages',
                ],
            ],
        ],

        'db' => require(__DIR__ . '/db.php'),
        'authManager' => [
        'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\DbManager'
        ],

         'urlManager' => [
           'enablePrettyUrl' => true,
           'showScriptName' => false,
        ],

        
        'log' => [
            'targets' => 
            [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                ],
            ],
        ],

        'user' => [
            'identityClass' => 'app\modules\user_management\modules\core\models\User',
            'enableAutoLogin' => false,
            'authTimeout' => 3600,
        ],

        'session' => [
            'class' => 'yii\web\Session',
            'cookieParams' => ['httponly' => true, 'lifetime' => 3600],
            'timeout' => 3600,
            'useCookies' => true,
        ],

        
         'appconfig' => [
              'class' => 'app\components\AppConfig',
              ] ,

      
        

    ],  //end component

    'as initializer' => [
        'class' => \app\components\ApplicationInitializerBehavior::class,
    ],
    'params' => require(__DIR__ . '/params.php'),
          'as access' => [
              'class' => 'mdm\admin\components\AccessControl',
              'allowActions' => [
                '*',
               
                // The actions listed here will be allowed to everyone including guests.
                // So, 'admin/*' should not appear here in the production, of course.
                // But in the earlier stages of your development, you may probably want to
                // add a lot of actions here until you finally completed setting up rbac,
                // otherwise you may not even take a first step.
              ]
        ],
        
];
   

    if(!YII_ENV_TEST) {
      // configuration adjustments for 'dev' environment
      $config['bootstrap'][] = 'debug';
      $config['modules']['debug'] = 'yii\debug\Module';

      $config['bootstrap'][] = 'gii';
      $config['modules']['gii']=[
        'class' =>  'yii\gii\Module',
        'allowedIPs' => ['*'],
      ];
    }
      return $config;