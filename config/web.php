<?php

return \yii\helpers\ArrayHelper::merge(require(__DIR__ . '/common.php'), [
    'id' => 'yii2-module-app-web',
    'components' => [
        'urlManager' => [
            'class' => \yii\web\UrlManager::class,
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
        ],
        'request' => [
            'enableCookieValidation' => true,
            'enableCsrfValidation' => true,
            'cookieValidationKey' => 'xdr5^TFC',
        ],
    ],
    
    
]);
