<?php
Yii::setAlias('@assetsDir', realpath(dirname(__FILE__).'/../data/'));
Yii::setAlias('@assetsStudentDir', realpath(dirname(__FILE__).'/../data/tmp/student/'));
Yii::setAlias('@assetsUserDir', realpath(dirname(__FILE__).'/../data/tmp/user/'));
Yii::setAlias('@assetsSchoolDir', realpath(dirname(__FILE__).'/../data/tmp/school/'));
Yii::setAlias('@assetsInquiryDir', realpath(dirname(__FILE__).'/../data/tmp/inquiry/'));


return [
	// DONOT change this key once code is in production,
	// this will lead to currupt all assets
    'secretKey' => 'btKxLG4MRq',
    'adminEmail' => 'no-reply@notifications.xeois.com',
    // maximum tracker log file size in MB
    'maxLogFileSize' => 100,
	'langauges'=>[
      'english-uk'=>'english-UK',
      'english-us'=>'english-US',
      'russian'=>'russian',
      'french'=>'french',
      'hindi'=>'hindi',
      'punjabi'=>'punjabi',
	],
	'dateFormats'=>[
	'dd-mm-yyyy'=>'d/m/Y',
    'mm/dd/yyyy'=>'m/d/Y',
    'yyyy/dd/mm'=>'Y/d/m',
    'dd/mm/yyyy'=>'d/M/Y',
    'dd-mm-yyyy'=>'d-M-Y',
    'dd-mm-yyyy'=>'d-m-Y',
    'mm-dd-yyyy'=>'m-d-Y',
    'yyyy-dd-mm'=>'Y-d-m'],
    'emsPlugins'=>[
      'Enquiry Enable On login'=>'/admission/enquiry/create-enquiry',
      'Application Enable On login'=>'/admission/applications/direct-apply',
      'Tour Enable On login'=>'/todo/tour/direct-apply',
    ],
    'supportEmail'=>'isgsupport@xeois.com', 
];

