<?php

$db =  require(__DIR__ . '/db.php');
$params = require(__DIR__ . '/params.php');
    
return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
     'bootstrap' => ['log','app\modules\smtp_config\modules\core\components\MailConfig','app\components\YiiConfig'],
    'controllerNamespace' => 'app\commands',
    'controllerMap' => [
        'migrate' => [
            'class' => \app\commands\MigrateController::class,
        ],
        'test'=> [
            'class' => \app\commands\TestController::class,
        ],
    ],
    'modules' => [
     'audit' => [
            'class' => 'bedezign\yii2\audit\Audit',
            // the layout that should be applied for views within this module
            'layout' => 'main',
            // Name of the component to use for database access
            'db' => 'db', 
            // List of actions to track. '*' is allowed as the last character to use as wildcard
            'trackActions' => ['*'], 
            // Actions to ignore. '*' is allowed as the last character to use as wildcard (eg 'debug/*')
            'ignoreActions' => ['audit/*', 'debug/*'],
            // Maximum age (in days) of the audit entries before they are truncated
            'maxAge' => 30,
            // IP address or list of IP addresses with access to the viewer, null for everyone (if the IP matches)
            'accessIps' => ['127.0.0.1', '192.168.*'], 
            // Role or list of roles with access to the viewer, null for everyone (if the user matches)
            'accessRoles' => ['superadmin'],
            // User ID or list of user IDs with access to the viewer, null for everyone (if the role matches)
            'accessUsers' => [1],
            // Compress extra data generated or just keep in text? For people who don't like binary data in the DB
            'compressData' => true,
            // The callback to use to convert a user id into an identifier (username, email, ...). Can also be html.
            'userIdentifierCallback' => ['app\modules\user_management\modules\core\models\User', 'userIdentifierCallback'],
            // If the value is a simple string, it is the identifier of an internal to activate (with default settings)
            // If the entry is a '<key>' => '<string>|<array>' it is a new panel. It can optionally override a core panel or add a new one.
            'panels' => [
                'audit/log',
                'audit/request',
                'audit/error',
                'audit/trail',
                'audit/mail',
                'audit/db',
                'audit/profiling',
                'audit/javascript',
                'audit/extra',
                'audit/curl',
               
                
                
            ],
            'panelsMerge' => [
               // ... merge data (see below)
            ]
        ],
        
    ],
    'components' => [
        'encryptor' => [
                  'class' => 'app\components\Encryptor',
              ] ,
        'sendmail' => [
                  'class' => 'app\components\SendMail',
              ] ,
               'appconfig' => [
                  'class' => 'app\components\AppConfig',
              ] ,
        'urlManager' => [
           'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
              
    ],
    'params' => $params,
];
