<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class EmployeeAsset extends AppAsset
{
	public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
	'css/fileupload.css',
	'css/employee.css',
    ];
    public $js = [
    'js/employee.js',
  ];
}
