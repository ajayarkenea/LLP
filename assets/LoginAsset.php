<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AppAsset
{
	public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
   //  'landing/css/bootstrap.min.css',
     'landing/css/bootstrap-theme.min.css',
     'landing/css/style.css',
     'landing/js/jquery.fancybox.css',
     'css/login.css',

    ];
    public $js = [
        //'landing/js/jquery-latest.min.js',
      
        'landing/js/bootstrap.min.js',
        'landing/js/jquery.bxslider.min.js',
        'landing/js/isotope.pkgd.min.js',
        'landing/js/jquery.fancybox.pack.js',
        'landing/js/jquery.sticky-kit.min.js',
        'landing/js/script.js',
        'js/login.js',
        'js/gmap.js',
  ];

    public $depends = [
        'app\assets\AppAsset',
     
    ];
}
