<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
 
  'theme/css/font-awesome.min.css',
  'theme/css/smartadmin-production-plugins.min.css',
  'theme/css/smartadmin-production.min.css',
  'theme/css/smartadmin-skins.min.css',
  'theme/css/smartadmin-rtl.min.css',
  'css/toastr.css',
  'css/site.css',
  'context-menu/jquery.contextMenu.min.css',
  'css/custom.css',
    ];
    public $js = [
        
      'theme/js/plugin/pace/pace.min.js',
      'theme/js/app.config.js',
      'theme/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js',
      'theme/js/smartwidgets/jarvis.widget.min.js',
      //'theme/js/plugin/jquery-validate/jquery.validate.min.js',
      'theme/js/plugin/sparkline/jquery.sparkline.min.js',
      //'theme/default/html/js/plugin/bootstrap-slider/bootstrap-slider.min.js',
      'theme/js/plugin/msie-fix/jquery.mb.browser.min.js',
      'theme/js/plugin/fastclick/fastclick.min.js',
      'theme/js/app.min.js',
      'js/bootbox.min.js',
      'context-menu/jquery.contextMenu.min.js',
      'context-menu/jquery.ui.position.min.js',
      //'theme/js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
    //  'js/toastr.js',
      'js/custom.js',
      
    ];
    public $depends = [
        'yii\web\YiiAsset',
         'yii\bootstrap\BootstrapPluginAsset',
    ];
}
