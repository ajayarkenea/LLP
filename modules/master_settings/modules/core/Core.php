<?php

namespace app\modules\master_settings\modules\core;

use app\components\modules\VersionModule;
use app\modules\master_settings\Master_Settings;
use app\modules\master_settings\modules\core\components\EventHandler;

class Core extends VersionModule
{
    /** @inheritdoc */
    public static function getEventHandlers()
    {
        return [
            Master_Settings::class=>[
                Master_Settings::SAMPLE_EVENT=> [EventHandler::class, 'sampleEventHandler'],
            ],
        ];
    }

    /** @inheritdoc */
    public static function getUrlRules()
    {
        return [
            'GET /master-setting.xeo' => '/master_settings/master-settings/index',
            'GET /usertype.xeo' => '/master_settings/usertype/index',
          
        ];
    }
}