<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\Usertype */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usertype-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'id'                     => 'password-form',
        // 'enableAjaxValidation'   => true,
        // 'enableClientValidation' => false,
        'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-xs-8\">{input}{error}</div>",
        'labelOptions' => ['class' => 'col-xs-4 control-label'],
        ],
    ]); ?>
        <div class="row">
            <h4 class="col-md-8" >
               <i class="lg fa fa-info"></i>
               <strong> User Type</strong>
            </h4>
            <div class="col-md-12 " style="height: 10px;"></div>
            <div class="form-column col-md-6" > 

    <?= $form->field($model, 'title')->textInput(['maxlength' => 50]) ?>

    
    </div>
        </div>

     <div class="form-actions">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ace-icon fa fa-plus"> Create</i>') : Yii::t('app', '<i class="ace-icon fa fa-pencil"> Update</i>'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

                <?= Html::a('<i class="ace-icon fa fa-remove"> Cancel</i>', ['usertype/index'], ['class'=>'btn btn-danger']) ?>

                <?php if ($model->isNewRecord): ?>
                <button type="reset" class="btn btn-info">
                    <i class="ace-icon fa fa-undo bigger-110"></i>
                    Reset
                </button>
                <?php endif ?>
            </div>

    <?php ActiveForm::end(); ?>

</div>



                