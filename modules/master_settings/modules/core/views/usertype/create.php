<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Usertype */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Usertype',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Usertypes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usertype-create">

 <div class="grey panel panel-info">
    <div class="panel-heading">
    	  <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
     </div>
    <div class="panel-body">
     
        <div class="col-lg-12">
          
<?= $this->render('_form', [
        'model' => $model,
       
        
        
    ]) ?>
</div>
</div>
</div>
</div>

