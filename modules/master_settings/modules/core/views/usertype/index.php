<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsertypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = Yii::t('app', 'Usertypes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usertype-index">

  
      <div class="col-lg-12">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],


           
            'title',
            'role',
            
            
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
            // 'is_delete',

            ['class' => '\kartik\grid\ActionColumn',
                                        
                  'template' => '{view}&nbsp;{delete}',
                    'buttons' => [
                         'view' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                             'title' => Yii::t('app', 'View'),
                            ]);
                        },
                        //  'update' => function ($url, $model) {
                        //         return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                        //      'title' => Yii::t('app', 'Update'),
                        //     ]);
                        // },
                        'delete' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                             'title' => Yii::t('app', 'Delete'),'data-method'=>"post",'data-confirm'=>"Are you sure you want to delete this item?",
                            ]);
                        }, 
                       
                    ],
                    'urlCreator' => function ($action, $model, $url, $index) {
                         $hash=Yii::$app->encryptor->encrypt($model->id); 
                         if ($action === 'view') {
                            return Url::to(['usertype/view','id'=>$hash]);
                         }else if($action === 'delete'){
                          return Url::to(['usertype/delete','id'=>$hash]);
                         }
                    },
            ],

        ],
         'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="white fa fa-users "></i> UserType </h3>',
            'type'=>'primary',
            'before'=>Html::a(Yii::t('app', '<i class="glyphicon glyphicon-plus"></i> Create {modelClass}', ['modelClass' => 'UserType',]), ['create'], ['class' => 'btn btn-primary']) ,
            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Grid', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>true,
            'pager'=>false
            ],
            'toolbar' => [
              '{export}'
              ],
            'exportConfig' => [
            GridView::HTML =>[],
            //  GridView::PDF =>[],
            GridView::TEXT => [],
            GridView::EXCEL => [],
            ]
    ]); ?>

</div>

</div>
