<?php

namespace app\modules\master_settings\modules\core\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\BaseModel;
/**
 * This is the model class for table "cities".
 *
 * @property int $id
 * @property string $name
 * @property int $state_id
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $is_deleted 1=not_delete, 0=deleted
 * @property int $status 1=active, 0=inactive
 */
class Cities extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'state_id'], 'required'],
            [['state_id', 'created_by', 'updated_by', 'is_deleted', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Name'),
            'state_id' => Yii::t('app', 'State ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'is_deleted' => Yii::t('app', '1=not_delete, 0=deleted'),
            'status' => Yii::t('app', '1=active, 0=inactive'),
        ];
    }

     /**
   * [getDropDowData generic function to get drop down data]
   *  in the respective model there should be a function called
   *  getFullName which should return the value for drop down
   * @return [array]
   */
  public static function getDropDownData($params=null)
  {
    $res = ArrayHelper::map(self::find()->where(['status'=>Self::ACTIVE,'is_deleted'=>Self::ISNOTDELETE])->all(), 'id', "fullName");

    //Sorting based on value
    asort($res);

    return $res;

  }
}
