<?php

namespace app\modules\master_settings\modules\core\models;

use Yii;
use yii\helpers\ArrayHelper;
use mdm\admin\models\AuthItem;
use app\models\BaseModel;
/**
 * This is the model class for table "usertype".
 *
 * @property integer $id
 * @property string $title
 * @property string $role
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $is_delete
 *
 * @property User[] $users
 */
class Usertype extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usertype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['status', 'created_by', 'updated_by', 'is_deleted'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 50],
            [['role'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'role' => Yii::t('app', 'Role'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'is_delete' => Yii::t('app', 'Is Delete'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['user_type' => 'id']);
    }
   
    /**
     * Aftersave called everytime any save happen, create or update
     * Assign role to new or updated users based on user type
     * @param $insert
     * @param $changedAttributes
     * @return [boolean]
     */
    public function afterSave($insert,$changedAttributes)
    {
       
        $authItem = new AuthItem(null);
        $authItem->name=$this->role;
        $authItem->type=1;
       // $authItem->created_at=time(); 
        $authItem->save(false);

        if (parent::afterSave($insert,$changedAttributes)) {
            return true;
        } else {
            return false;
        }
    }

    public static function getDropDownData($params = null) 
    { 
      return ArrayHelper::map(Usertype::find()->where(['not in','id',1])->all(), 'id', 'title');
    } 
    
     public static function getDropDownDataForRegisteration($params = null) 
    { 
      return ArrayHelper::map(Usertype::find()->where(['id'=>[2,3]])->all(), 'id', 'title');
    } 

}