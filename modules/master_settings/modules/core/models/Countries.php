<?php

namespace app\modules\master_settings\modules\core\models;

use Yii;
use app\models\BaseModel;
/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $sortname
 * @property string $name
 * @property int $phonecode
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $is_deleted 1=not_delete, 0=deleted
 * @property int $status 1=active, 0=inactive
 */
class Countries extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sortname', 'name', 'phonecode'], 'required'],
            [['phonecode', 'created_by', 'updated_by', 'is_deleted', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['sortname'], 'string', 'max' => 3],
            [['name'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sortname' => Yii::t('app', 'Sortname'),
            'name' => Yii::t('app', 'Name'),
            'phonecode' => Yii::t('app', 'Phonecode'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'is_deleted' => Yii::t('app', '1=not_delete, 0=deleted'),
            'status' => Yii::t('app', '1=active, 0=inactive'),
        ];
    }
}
