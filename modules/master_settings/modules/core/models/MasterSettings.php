<?php

namespace app\modules\master_settings\modules\core\models;

use app\components\modules\ModuleActiveRecordTrait;
use app\modules\master_settings\events\SampleEvent;
use app\modules\master_settings\Master_Settings;
use yii\db\ActiveRecord;

class SampleModel extends ActiveRecord
{
    use ModuleActiveRecordTrait;

    public function create()
    {
        Master_Settings::triggerEvent(Master_Settings::SAMPLE_EVENT, new SampleEvent());
    }
}