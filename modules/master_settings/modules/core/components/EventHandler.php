<?php

namespace app\modules\master_settings\modules\core\components;

abstract class EventHandler
{
    /**
     * @param \app\modules\social_plugins\events\SampleEvent $event
     */
    public static function sampleEventHandler($event)
    {
        // do something
    }
}