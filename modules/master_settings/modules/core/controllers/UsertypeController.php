<?php

namespace app\modules\master_settings\modules\core\controllers;

use Yii;
use app\modules\master_settings\modules\core\models\Usertype;
use app\modules\master_settings\modules\core\models\UsertypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\AuthItem;
use app\models\AuthAssignment;
use app\controllers\BaseController;

/**
 * UsertypeController implements the CRUD actions for Usertype model.
 */
class UsertypeController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usertype models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsertypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Usertype model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Usertype model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Usertype();

        if ($model->load(Yii::$app->request->post())) {
             $model->created_by=Yii::$app->user->identity->id;
             $model->role=$model->title.' '.'Role';
             $model->save(false);
            return $this->redirect(['index']);
        } 
            return $this->render('create', [
                'model' => $model,
            ]);
        
    }

    /**
     * Updates an existing Usertype model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at=new \yii\db\Expression('NOW()');
            $model->updated_by=Yii::$app->user->identity->id;
            $model->save();
            return $this->redirect(['index']);
        }
            return $this->render('update', [
                'model' => $model,
            ]);
        
    }


    /**
     * Deletes an existing Usertype model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        //echo "<pre>";print_r($model);exit;
        $auth_assign=AuthAssignment::find()->where(['item_name'=>$model->role])->one();
        if($auth_assign===null){
        if($model->delete()){

            $authItem =AuthItem::find()->where(['name'=>$model->role])->one();
            if($authItem!==null){
                 $authItem->delete();
            }
        }
        }else{
            Yii::$app->session->setFlash('danger',Yii::t('app','Failed: This Role already assigned to user,You can not delete'));
            return $this->redirect(['index']);
        }

        
        return $this->redirect(['index']);
    }

    /**
     * Finds the Usertype model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usertype the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($key)
    {
        $id=Yii::$app->encryptor->decrypt($key);
        if (($model = Usertype::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
