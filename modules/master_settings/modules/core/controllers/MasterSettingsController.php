<?php

namespace app\modules\master_settings\modules\core\controllers;

use yii\web\Controller;

class MasterSettingsController extends Controller
{
    public function actionIndex()
    {
    	$data = 'Sample message';
    	return $this->render('sample', [
            'data' => $data,
        ]);
    }
   
}