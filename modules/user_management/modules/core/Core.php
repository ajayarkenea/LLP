<?php

namespace app\modules\user_management\modules\core;

use app\components\modules\VersionModule;
use app\modules\user_management\User_Management;
use app\modules\user_management\modules\core\components\EventHandler;

class Core extends VersionModule
{
    /** @inheritdoc */
    public static function getEventHandlers()
    {
        return [
            User_Management::class=>[
                User_Management::SAMPLE_EVENT=> [EventHandler::class, 'sampleEventHandler'],
            ],
        ];
    }

    /** @inheritdoc */
    public static function getUrlRules()
    {
        return [
            'GET /umt.xeo' => '/user_management/user/index',
        //    'GET /employee.xeo' => '/user_management/employee/index',
            'GET /student.chkDashboard.xeo' => '/user_management/student/student-checklist-dashboard',
            'GET /student.doc.upload.xeo' => '/user_management/student/student-checklist-upload',
            'GET /student.application.form.xeo' => '/user_management/student/student-app-ques',
            'GET /user-api.xeo/<function:\w+>/<params:\w+>' => '/user_management/user-api/index',
           
        ];
    }
}