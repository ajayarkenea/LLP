<?php

namespace app\modules\user_management\modules\core\models;

use Yii;
use yii\base\Model;
use app\modules\user_management\modules\core\models\Employee;

use yii\data\ArrayDataProvider;
use app\models\BaseModel;
use app\modules\master_settings\modules\core\models\Usertype;
use yii\helpers\Url;

 /**
 * This is the model class for table "positions".
 *
 * @property string $id
 * @property string $address
 */
class ProfileSetting extends BaseModel
{
    public $photo;
    public $username;
    public $isNewRecord;
    
    public function rules()
    {
        return [
       // ['photo', 'file', 'extensions' => ['jpg','png']],
       // ['photo', 'file','maxSize' => 1024 * 1024 * 2,
      // 'tooBig' => 'File size should not more then 2 MB...'],
        [['imgfile','photo'],'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'photo' => Yii::t('app', 'Image'),
            
        ];
    }

    public function findProfilePic(){
      if (!\Yii::$app->user->isGuest){
        $baseurl=\Yii::$app->request->BaseUrl;
        switch (Yii::$app->user->identity->user_type) {
          case Usertype::SUPERADMIN:
            return Url::to(['/assets.xeo','id'=> 1]);
            break;
         
          default:
            $employee=Employee::find()->where(['user_id'=>Yii::$app->user->identity->id])->one();
           
              return Url::to(['/assets.xeo','id'=> 1]);
            
            break;
        }
      }
    } 

     

}    