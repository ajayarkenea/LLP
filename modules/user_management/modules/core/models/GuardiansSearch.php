<?php

namespace app\modules\user_management\modules\core\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user_management\modules\core\models\Guardians;


/**
 * GuardiansSearch represents the model behind the search form about `app\models\Guardians`.
 */
class GuardiansSearch extends Guardians
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'form_id', 'cust_field_id', 'user_id', 'relation_id', 'created_by', 'updated_by', 'is_deleted', 'status'], 'integer'],
            [['family_id', 'first_name', 'middle_name', 'last_name', 'gender', 'email', 'mobile', 'work_phone', 'home_phone', 'address1', 'address2', 'address3', 'city', 'state', 'country', 'company', 'designation', 'fax', 'identity_number', 'identity_expiry_date', 'zip_code', 'company_hr_name', 'company_hr_email', 'company_hr_phone', 'company_address','citizenship', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Guardians::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'form_id' => $this->form_id,
            'cust_field_id' => $this->cust_field_id,
            'user_id' => $this->user_id,
            'relation_id' => $this->relation_id,
            'identity_expiry_date' => $this->identity_expiry_date,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'is_deleted' => $this->is_deleted,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'family_id', $this->family_id])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'work_phone', $this->work_phone])
            ->andFilterWhere(['like', 'home_phone', $this->home_phone])
            ->andFilterWhere(['like', 'address1', $this->address1])
            ->andFilterWhere(['like', 'address2', $this->address2])
            ->andFilterWhere(['like', 'address3', $this->address3])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'company', $this->company])
            ->andFilterWhere(['like', 'designation', $this->designation])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'view_path', $this->view_path])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'identity_expiry_date', $this->identity_expiry_date])
            ->andFilterWhere(['like', 'zip_code', $this->zip_code])
            ->andFilterWhere(['like', 'company_hr_name', $this->company_hr_name])
            ->andFilterWhere(['like', 'company_hr_email', $this->company_hr_email])
            ->andFilterWhere(['like', 'company_hr_phone', $this->company_hr_phone])
            ->andFilterWhere(['like', 'company_address', $this->company_address]);

        return $dataProvider;
    }
}
