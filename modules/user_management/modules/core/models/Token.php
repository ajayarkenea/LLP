<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\modules\user_management\modules\core\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use app\models\BaseModel;


/**
 * Token Active Record model.
 *
 * @property integer $user_id
 * @property string  $code
 * @property integer $created_at
 * @property integer $type
 * @property string  $url
 * @property bool    $isExpired
 * @property User    $user
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class Token extends BaseModel
{
    const TYPE_CONFIRMATION      = 0;
    const TYPE_RECOVERY          = 2;
    

      /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'token';
    } 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne($this->module->modelMap['User'], ['id' => 'user_id']);
    }

    /**
     * @return string
     */
    public function getUrl($type)
    {
        
        switch ($type) {
            case self::TYPE_CONFIRMATION:
                $route = '/register/confirm';
                break;
            case self::TYPE_RECOVERY:
                $route = '/register/reset';
                break;

            default:
                throw new \RuntimeException;
        }

        return Url::to([$route], true);
    }

    /**
     * @return bool Whether token has expired.
     */
    public function getIsExpired()
    {
        switch ($this->type) {
            case self::TYPE_CONFIRMATION:
            
                $expirationTime = Yii::$app->appconfig->getConfirmAccountExpiryTime();
                break;
            case self::TYPE_RECOVERY:
                $expirationTime = Yii::$app->appconfig->getRecoveryAccountExpiryTime();
                break;
             
            default:
                throw new \RuntimeException;
        }
        return ($this->created_at + $expirationTime) < time();
    }

    // /** @inheritdoc */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->setAttribute('created_at', time());
            //$this->setAttribute('code', \Yii::$app->security->generateRandomString(3));
        }

        return parent::beforeSave($insert);
    }

    
}