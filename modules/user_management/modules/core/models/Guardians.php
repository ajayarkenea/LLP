<?php

namespace app\modules\user_management\modules\core\models;

use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use app\models\BaseModel;
use app\modules\admission\modules\core\models\Enquiry;
use app\modules\admission\modules\core\models\Applications;
use app\modules\master_settings\modules\core\models\Relationship;
use app\modules\master_settings\modules\core\models\Gender;
use app\modules\master_settings\modules\core\models\Country;



/**
 * This is the model class for table "guardians".
 *
 * @property integer $id
 * @property integer $form_id
 * @property integer $cust_field_id
 * @property string $family_id
 * @property integer $user_id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property integer $relation_id
 * @property string $gender
 * @property string $email
 * @property string $mobile
 * @property string $work_phone
 * @property string $home_phone
 * @property string $address1
 * @property string $address2
 * @property string $address3
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $company
 * @property string $designation
 * @property string $photo
 * @property string $view_path
 * @property string $fax
 * @property string $identity_number
 * @property string $identity_expiry_date
 * @property string $zip_code
 * @property string $company_hr_name
 * @property string $company_hr_email
 * @property string $company_hr_phone
 * @property string $company_address
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $is_deleted
 * @property integer $status
 */
class Guardians extends BaseModel
{
     public $create_account;
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['checkmail'] = ['email'];
        $scenarios['nocheckemail'] = ['relation_id','first_name','middle_name','last_name','gender','mobile','home_phone','address1','address2',
        'zip_code','city','state','country','company','designation','identity_number','identity_expiry_date','fax','work_phone','company_hr_name','company_hr_email','company_hr_phone','company_address',
        'citizenship'];
      
        return $scenarios;
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'guardians';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             [['first_name','email','relation_id', 'last_name','gender','mobile','home_phone','address1','city','country','company','citizenship'
              ],'required'],
            ['email', 'email'],
            ['email', 'checkEmailList'],
             //['title', 'required', 'on' => 'create']
            //'message' => \Yii::t('app', 'This email address has already been taken')],
            [['form_id', 'cust_field_id', 'user_id', 'relation_id', 'created_by', 'updated_by', 'is_deleted', 'status'], 'integer'],
            [['identity_expiry_date', 'created_at', 'updated_at','assets_id','create_account'], 'safe'],
            [['company_address','citizenship'], 'string'],
            [['family_id', 'first_name', 'middle_name', 'last_name', 'gender', 'mobile', 'work_phone', 'home_phone', 'city', 'state', 'country', 'zip_code', 'company_hr_name', 'company_hr_email', 'company_hr_phone'], 'string', 'max' => 20],
            [['email', 'fax'], 'string', 'max' => 50],
            [['address1', 'address2', 'address3', ], 'string', 'max' => 255],
            [['identity_number', 'designation', 'company'], 'string', 'max' => 100],  //guardians-1-email
           
        ];
    }

    /**
     * @inheritdoc
     */
    public function checkEmailList($attribute, $params) {
            $user=User::find()->where(['email'=>$this->email])->one();
             if ($user!==null) {
                $this->addError($attribute, Yii::t('app', ''.$this->email.' is already exists... '));
                }
        }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'form_id' => Yii::t('app', 'Form ID'),
            'cust_field_id' => Yii::t('app', 'Cust Field ID'),
            'family_id' => Yii::t('app', 'Family ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'middle_name' => Yii::t('app', 'Middle Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'relation_id' => Yii::t('app', 'Relationship'),
            'gender' => Yii::t('app', 'Gender'),
            'email' => Yii::t('app', 'Email'),
            'mobile' => Yii::t('app', 'Mobile Phone'),
            'work_phone' => Yii::t('app', 'Work Phone'),
            'home_phone' => Yii::t('app', 'Home Phone'),
            'address1' => Yii::t('app', 'Current Home Address'),
            'address2' => Yii::t('app', 'Permanent Address'),
            'address3' => Yii::t('app', 'Address3'),
            'city' => Yii::t('app', 'City'),
            'state' => Yii::t('app', 'State'),
            'country' => Yii::t('app', 'Country'),
            'company' => Yii::t('app', 'Working Company'),
            'designation' => Yii::t('app', 'Job Title'),
            'fax' => Yii::t('app', 'Fax'),
            'identity_number' => Yii::t('app', 'Iqama Number'),
            'identity_expiry_date' => Yii::t('app', 'Iqama Expiry Date (MM/DD/YYYY)'),
            'zip_code' => Yii::t('app', 'Zip Code'),
            'company_hr_name' => Yii::t('app', 'Company HR Name'),
            'company_hr_email' => Yii::t('app', 'Company HR Email'),
            'company_hr_phone' => Yii::t('app', 'Company HR Phone'),
            'company_address' => Yii::t('app', 'Company Address'),
            'citizenship' => Yii::t('app', 'Citizenship (Passport)'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * profileData is find data for profile and sent to profile view
     * @return [array]
     *@author name RG
     *@param $id integer
     */
    public static function guardianProfile()
    {    
        $model=Guardians::find()->where(['user_id'=>Yii::$app->user->identity->id])->asArray()->one();
           return $model;
      
    }

    
    /**
     * profileData is find data for profile and sent to profile view
     * @return [array]
     *@author name RG
     *@param $id integer
     */
     public static function familyMember()
    {    
               
       $model=Guardians::find()->where(['family_id'=>Yii::$app->user->identity->familyid])->asArray()->all();
        
        $dataprovider = new ArrayDataProvider([
            'allModels' => $model,
            'key'=>'id',
            'pagination' => [
            'pageSize' => 100,
            ],
            ]);
        return $dataprovider;   
    
      
    }

       

    public function getRelationship()
    {
        return $this->hasOne(Relationship::className(), ['id' => 'relation_id']);
    }

    public function getGenderTitle()
    {
        return $this->hasOne(Gender::className(), ['id' => 'gender']);
    }

     public function getCountryTitle()
    {
        return $this->hasOne(Country::className(), ['id' => 'country']);
    }

     public static function getDropDownData($model=null) 
    { 
        if($model->isNewRecord!=true){
          return ArrayHelper::map(Relationship::find()->all(), 'id', 'title');
        }else{
          $subQuery=Guardians::find()->select('relation_id')->where(['family_id'=>Yii::$app->user->identity->familyid]);
            return ArrayHelper::map(Relationship::find()->where(['not in', 'id',$subQuery])->all(), 'id', 'title');
      }
    } 
    /* Getter for person full name */
    public function getFullName() {
        return $this->first_name . ' ' . $this->last_name;
    }

     /**
     * profileData is find data for profile and sent to profile view
     * @return [array]
     *@author name RG
     *@param $id integer
     */
     public static function getGuardianByFamilyId($familyid)
    {    
               
       $model=Guardians::find()->where(['family_id'=>$familyid])->asArray()->all();
       $retVal=[];
       for ($i=0; $i < count($model); $i++) { 
          $retVal[$i]['id']=$model[$i]['id'];
          $retVal[$i]['family_id']=$model[$i]['family_id'];
          $retVal[$i]['user_id']=$model[$i]['user_id'];
          $retVal[$i]['name']=$model[$i]['first_name'].' '.$model[$i]['last_name'];
          $retVal[$i]['relation']=$model[$i]['relation_id'];
          $retVal[$i]['gender']=$model[$i]['gender'];
          $retVal[$i]['email']=$model[$i]['email'];
       }
      
        return $retVal;   
    
      
    }



}
