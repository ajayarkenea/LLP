<?php

namespace app\modules\user_management\modules\core\models;

use Yii;
use app\components\modules\ModuleActiveRecordTrait;
use app\modules\user_management\events\SampleEvent;
use app\modules\user_management\User_Management;
use app\models\BaseModel;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\base\Security;
use yii\web\IdentityInterface;
use app\modules\master_settings\modules\core\models\Usertype;
use yii\rbac\DbManager;
use yii\log\Logger;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use yii\web\Controller;
use app\models\Password;
use yii\helpers\Url;
use app\modules\master_settings\modules\core\models\Gender;
use app\modules\master_settings\modules\core\models\UserToken;
use app\modules\settings\modules\core\models\Type;


class User extends BaseModel implements IdentityInterface
{
    use ModuleActiveRecordTrait;

    public function create()
    {
        User_Management::triggerEvent(User_Management::SAMPLE_EVENT, new SampleEvent());
    }
     
    public $isNewRecord;
    public $fullName;
    public $passwd;
   
  

  
     /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
       return [

       [['confirmed_at', 'blocked_at', 'updated_by', 'status', 'created_by','flag'], 'integer'],
        [['user_type'], 'required'],
             //username rules
       ['username', 'filter', 'filter' => 'trim'],
       ['username', 'match', 'pattern' => '/^[-a-zA-Z0-9_\.@]+$/'],
       //['username', 'required'],
       ['username', 'unique',
       'message' => \Yii::t('app', 'This username has already been taken')],
       ['username', 'string', 'min' => 3, 'max' => 20],
            // email rules
       ['email', 'filter', 'filter' => 'trim'],
       ['email', 'required'],
       ['email', 'email'],
       ['email', 'unique',
       'message' => \Yii::t('app', 'This email address has already been taken')],
            
        [['first_name','last_name','gender'], 'required'],
        [['created_at', 'updated_at','user_type'],'safe'],
       
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
       return [
            'email'    => \Yii::t('app', 'Login'),
            'username' => \Yii::t('app', 'Username'),
            'password_hash' => \Yii::t('app', 'Password'),
            'email'=> Yii::t('app', 'Email'),
            'email_type' => Yii::t('app', 'Account Type'),
            'user_type' => Yii::t('app', 'User Type'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'company_name' => Yii::t('app', 'Company Name'),
            'gender' => Yii::t('app', 'Gender'),
            'mobile' => Yii::t('app', 'Mobile'),
            'city_id' => Yii::t('app', 'City'),
            'state_id' => Yii::t('app', 'State'),
            'country_id' => Yii::t('app', 'Country'),
            'comapny_type_id' => Yii::t('app', 'Company Type'),
            'landline_off' => Yii::t('app', 'Landline(office)'),
            'landline_res' => Yii::t('app', 'Landline(resident)'),
            'access_type_id' => Yii::t('app', 'Access Type'),
            'iuser_company' => Yii::t('app', 'Companies'),
            
        ];
    }
    
   
    /**
     * @return \yii\db\ActiveQuery
     */
        public function getUserType()
        {
            return $this->hasOne(Usertype::className(), ['id' => 'user_type']);
        }
     /**
     * Finds a user by the given id.
     *
     * @param  integer     $id User id to be used on search.
     * @return models\User
     */
     public static function findUserById($id)
    {
        return static::findOne(['id' => $id]);
    }
      
    
    /**
     * @return \yii\db\ActiveQuery
     */

    /** INCLUDE USER LOGIN VALIDATION FUNCTIONS**/
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);

    }
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type=null)
    {
        if (UserToken::isTokenValid($token))
        {
            // TODO: get api user
            return static::findOne(1);
        }
        throw new \yii\web\UnauthorizedHttpException('Login failed');
    }

    /**
     * Finds user by username
     *
     * @param string     $username
     * @return static|null
     */
    public static function findUserByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }


    /**
     * Finds a user by the given email.
     *
     * @param  string      $email Email to be used on search.
     * @return models\User
     */
    public static function findUserByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds a user by the given username or email.
     *
     * @param  string      $usernameOrEmail Username or email to be used on search.
     * @return models\User
     */
    public static function findUserByUsernameOrEmail($usernameOrEmail)
    {
        if (filter_var($usernameOrEmail, FILTER_VALIDATE_EMAIL)) {
            return User::findUserByEmail($usernameOrEmail);
        }

        return User::findUserByUsername($usernameOrEmail);
    }

    /**
     * Finds user by password reset token
     *
     * @param string     $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token
        ]);
    }

    
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        return Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->getSecurity()->generateRandomKey();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->getSecurity()->generateRandomKey() . '_' . time();
    }
    

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    /** EXTENSION MOVIE **/

    /**
     * Aftersave called everytime any save happen, create or update
     * Assign role to new or updated users based on user type
     * @param $insert
     * @param $changedAttributes
     * @return [boolean]
     */
    public function afterSave($insert,$changedAttributes)
    {
        $role = Usertype::findOne($this->user_type)['role'];

        $obj = (object)array('name' => $role);
       // echo "<pre>"; print_r($this->user_type); print_r($role); print_r($obj);exit;
        $r=new DbManager;

        $r->init();

        $r->revokeAll($this->id);

        $r->assign($obj, $this->id);

        if (parent::afterSave($insert,$changedAttributes)) {
            return true;
        } else {
            return false;
        }
    }

 /**
     * Generates user-friendly random password containing at least one lower case letter, one uppercase letter and one
     * digit. The remaining characters in the password are chosen at random from those three sets.
     * @see https://gist.github.com/tylerhall/521810
     * @param $length
     * @return string
     */
    public static function generate($length) {
    $str = "";
    $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
        $rand = mt_rand(0, $max);
        $str .= $characters[$rand];
    }
    return $str;
    }
      /**
     * This method attempts user confirmation. It uses finder to find token with given code and if it is expired
     * or does not exist, this method will throw exception.
     *
     * If confirmation passes it will return true, otherwise it will return false.
     *
     * @param  string  $code Confirmation code.
     */
    public function attemptConfirmation($code)
    {
       $model=new Password;
        /** @var Token $token */
        $token = Token::find()->where([
            'user_id' => $this->id,
            'type'    => Token::TYPE_CONFIRMATION,
            'code'    =>$code,
            ])->one();
   
        if ($token === null || $token->isExpired) {
            \Yii::$app->session->setFlash('danger', \Yii::t('app', 'The confirmation link is invalid or expired. Please try requesting a new one.'));
            return false;


        } else {
            $token->delete();
            $hash=$this->generate(6);
            $this->password_hash=$this->setPassword($hash);
            $this->confirmed_at = time();
            $hash=$this->generate(6);
            $this->password_hash=$this->setPassword($hash);
            //\Yii::getLogger()->log('User has been confirmed', Logger::LEVEL_INFO);
    
            if ($this->save(false)) {
                // $sync=SyncEmail::find()->where(['user_id'=>$this->id])->one();
                // $hash= Yii::$app->encryptor->decrypt($sync->userdata);
                // Yii::$app->sendmail->sendWelcomeMessage($sync->email,$sync->username, $hash);
             return true;
               

                // \Yii::$app->session->setFlash('success', \Yii::t('app', 'Thank you, registration is now completed.Your password sent to email'));
                
            } else {
                \Yii::$app->session->setFlash('danger', \Yii::t('app', 'Something went wrong and your account has not been confirmed.'));
                  return false;
            }
        }

    }
      /**
     * setLoginOrRegister
     */
    public static function setLogin($attributes)
    {
        $email=$attributes['emails'][0]['value'];;
        $model=new User();
        $user = $model->findUserByEmail($email);
        if($user===null){
             \Yii::$app->session->setFlash('danger', \Yii::t('app', 'We did not find any account with this '.$email.' email.Please clcik on create account'));
            return false;
        }else{
             Yii::$app->user->login($user);
      }    
    }

      /**
     * setLoginOrRegister
     */
    public static function setLoginOrRegister($attributes)
    {
        $email='';
        $first_name='';
        $last_name='';
        $gender='';

         $email=$attributes['emails'][0]['value'];;
         $first_name=$attributes['name']['givenName'];
         $last_name=$attributes['name']['familyName'];
         $isPlusUser=$attributes['isPlusUser'];
        
         if(isset($attributes['gender'])){
           $gender=Gender::getNamebyId($attributes['gender']);
         }
        // $image=$attributes['image']['url'];
        // $gimage= explode('.jpg',$image);
    
        $model=new User();
        $user = $model->findUserByEmail($email);
        if($user===null){
           $register=$model->registerSocialUser($email,$first_name,$last_name,$gender);
      }else{
             Yii::$app->user->login($user);
      }    
    }
    
     /**
     * registerSocialUser
     */
    public function registerSocialUser($email,$first_name,$last_name,$gender)
    {
         $model=new User();
         $parent=new Guardians;
         
         $baseurl=\Yii::$app->request->BaseUrl;
         $web= \Yii::getAlias('@webroot');
         $name=$first_name.' '.$last_name;
         // $model->username=User::generateUsername($email);
         $model->email=$email;
         $hash=$model->generate(6);
         $model->password_hash=$model->setPassword($hash);
         $model->confirmed_at=time();
         $model->user_type=User::GUARDIAN;
         $model->flag=User::INACTIVE;
          if($model->save(false)){
            $time=time();
            $parent->first_name=$first_name;
            $parent->last_name=$last_name;
            $parent->gender=$gender;
            $parent->email=$email;
            $parent->assets_id=User::DEFAULT_ASSET_ID;
            $parent->unique_id='GUA-'.$time;
            $parent->family_id='FAM-'.$time;
            $parent->user_id=$model->id;
              if($parent->save(false)){
                  $assetsUserDir = \Yii::getAlias('@assetsUserDir'); 
                    if (!mkdir($assetsUserDir.'/'.$parent->unique_id.'', 0777, true)){
                        die('Failed to create folders...');
                    }
              Yii::$app->user->login($model);
              Yii::$app->sendmail->sendWelcomeMessage($email, $hash, $name);
              return  \Yii::$app->session->setFlash('success', \Yii::t('app', 'Thank you for sign up with us.Your password sent to your email '.$email.''));
              }
          }else {
          \Yii::$app->session->setFlash('danger', \Yii::t('app', 'Technical Error ...Please try Again or Contact to our support Team'));
          }
    }

     /**
     * Generates new username based on email address, or creates new username
     * like "user1".
     */
    public static function generateUsername($email=null)
    {
        // try to use name part of email
        $username = explode('@', $email);
        $user=User::findUserByUsername($username[0]) ;
        if($user===null){
            return $username[0];
        } else{
            $row = (new Query())
                ->from('user')
                ->select('MAX(id) as id')
                ->one();
            
            return $username = 'user' . ++$row['id'];
        }
    }
    

    public function getfullName($id=null)
    {
       $model=Profile::find()->where(['user_id'=>$this->id])->one();
       return  $model->first_name . ' ' . $model->last_name;
        
        
    }

     /**
     * @return \yii\db\ActiveQuery
     */
        public function getUserProfile()
        {
            return $this->hasOne(Profile::className(), ['user_id' => 'id']);
        }
    
     public function getFamilyId()
    {   
        
        $model = Guardians::find()->where(['user_id'=>$this->id])->one();
        if ($model !==null){
             return $model->family_id;
        }else{
             $time=time();
             return 'FAM-'.$time;
        } 
    }

    public function getUsertypes()
    {
        return $this->hasOne(Usertype::className(), ['id' => 'user_type']);
    }

    public function getTypes()
    {
        return $this->hasOne(Type::className(), ['id' => 'type_id']);
    }
        
   
}