<?php

namespace app\modules\user_management\modules\core\models;

use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use app\modules\master_settings\modules\core\models\Usertype;
use yii\rbac\DbManager;
use app\models\BaseModel;
use app\modules\training\modules\core\models\TrainerTechnology;
/**
 * This is the model class for table "employee".
 *
 * @property integer $id
 * @property integer $form_id
 * @property integer $cust_field_id
 * @property integer $user_id
 * @property string $photo
 * @property string $fname
 * @property string $mname
 * @property string $lname
 * @property string $dob
 * @property string $email
 * @property string $phone
 * @property integer $department_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $is_deleted
 * @property integer $status
 */
class Employee extends BaseModel
{
    public $photo;
    public $technology_id;
    public $commercial_per_day;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fname','lname','email','usertype_id'], 'required'],
            [['email'], 'email'],
          //  [['email'], 'checkEmailList'],
            [['native_city_id', 'current_city_id', 'user_id','department_id', 'created_by', 'updated_by', 'is_deleted', 'status','usertype_id'], 'integer'],
            [['dob', 'created_at', 'updated_at','photo','resume_assets_id','native_city_id', 'current_city_id'], 'safe'],
            [['photo', 'fname', 'mname', 'lname', 'email'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 50],
            ['resume_assets_id', 'file','maxSize' => 1024 * 1024 * 10,
             'tooBig' => 'File size should not more then 10 MB...'],
            ['resume_assets_id', 'file', 'extensions' => ['pdf','doc','docx']], 
           
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
        
            'user_id' => Yii::t('app', 'User ID'),
            'usertype_id' => Yii::t('app', 'User Type'),
            'photo' => Yii::t('app', 'Photo'),
            'resume_assets_id' => Yii::t('app', 'Resume'),
            'fname' => Yii::t('app', 'First name'),
            'mname' => Yii::t('app', 'Middle name'),
            'lname' => Yii::t('app', 'Last name'),
            'dob' => Yii::t('app', 'Dob'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'department_id' => Yii::t('app', 'Department'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    
    /**
        *@author Swati
         * [getFullName get fullname for drop down ]
         * @return [string]
         */
        public function getFullName()
        {
            return $this->fname.' '.$this->lname;
        }


   /**
     * profileData is find data for profile and sent to profile view
     * @return [array]
     *@author name RG
     *@param $id integer
     */
    public static function employeeProfile()
    {    
        $model=Employee::find()->where(['user_id'=>Yii::$app->user->identity->id])->asArray()->one();
           return $model;
      
    }

    /**
     * profileData is find data for profile and sent to profile view
     * @return [array]
     *@author name RG
     *@param $id integer
     */
     public static function employeeEnquiry()
    {    
               
       $model=Enquiry::find()->asArray()->all();
        
        $dataprovider = new ArrayDataProvider([
            'allModels' => $model,
            'key'=>'id',
            'pagination' => [
            'pageSize' => 100,
            ],
            ]);
        return $dataprovider;   
    
      
    }

    /**
     * profileData is find data for profile and sent to profile view
     * @return [array]
     *@author name RG
     *@param $id integer
     */
     public static function employeeApplication()
    {    
               
       $model=Application::find()->asArray()->all();
        
      // echo "<pre>"; print_r($model);exit;
        $dataprovider = new ArrayDataProvider([
            'allModels' => $model,
            'key'=>'id',
            'pagination' => [
            'pageSize' => 100,
            ],
            ]);
        return $dataprovider;   
    
      
    }

    /**
     * profileData is find data for profile and sent to profile view
     * @return [array]
     *@author name RG
     *@param $id integer
     */
     public static function employeeMember()
    {    
               
       $model=Guardians::find()->asArray()->all();
        
       //echo "<pre>"; print_r($model);exit;
        $dataprovider = new ArrayDataProvider([
            'allModels' => $model,
            'key'=>'id',
            'pagination' => [
            'pageSize' => 100,
            ],
            ]);
        return $dataprovider;   
    
      
    }

    /**
    *@param Instance of file list
    *@return Dataprovider
    *@author RG
    **/
    public function getVerifyCsv($data)
    {
       $retVal=[];
       $error=false;
       $count=count($data);

      for($i=2;$i<=$count;$i++){

        if(($data['1']['E'])!='3008'){
            Yii::$app->session->setFlash('danger',' Please download sample file and fill the data and again upload...!');
            return $error;
        }

            if(($data[$i]['A']=='') || ($data[$i]['B']=='') ){
               Yii::$app->session->setFlash('danger','Line number ' .$i.' Please fill the mandatory fields and try again...');
               return $error; 
            }


                $email=$data[$i]['B'];
                if(($this->checkCsvData($email))==true){
                    Yii::$app->session->setFlash('danger',''.$email.'  already exists Please remove exists data and again try...!');
                    return $error;
                }

                // if($count>=101){
                //    Yii::$app->session->setFlash('danger','Please make sure data should not be more than 100');
                //    return $error; 
                // }

                   // else{
                  $retVal[$i]['fname']=$data[$i]['A']; 
                  $retVal[$i]['email']=$data[$i]['B'];
 
      }
            $dataprovider = new ArrayDataProvider([
            'allModels' => $retVal,
            'pagination' => [
            'pageSize' => 100,
             ],
             ]);
            return $dataprovider;
    }

    /**
    *@param email [string]
    *@return Boolean
    *@author RG
    *checkCsvData is checking duplication data from db
    **/
    public function checkCsvData($email){
         if(Employee::findOne(['email'=>$email])!==null){
            return true;
        }else{
            return false;  
        }
    }

      /**
    *@param data [array]
    *@author RG
    *checkCsvData is saving data in Employee db
    **/
    public function saveCsvData($data)
    {
        $count=count($data);
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            for($i=2;$i<($count +2);$i++){
                $model=new Employee;
                $model->fname=$data[$i]['fname'];
                $model->email=$data[$i]['email'];
                // $model->class=$data[$i]['class'];
                // $model->guardian_email=$data[$i]['guardian_email'];
                // $model->country_code=Student::INDIA;
                // $model->guardian_phone=$data[$i]['guardian_phone'];
                $model->created_by=Yii::$app->user->identity->id;
                $model->save(false);
            }
            $transaction->commit(); 
            Yii::$app->session->setFlash('success','Csv file uploaded successfully. ');
        } catch (Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('danger','There is some Technical Issue please try again....!');
        }
    }

    public static function getDropDownData($param=null){
      return ArrayHelper::map(self::find()->where(['status'=>Self::ACTIVE,'is_deleted'=>Self::ISNOTDELETE])->all(), 'id', "fullName");
    }

     /** EXTENSION MOVIE **/

    /**
     * Aftersave called everytime any save happen, create or update
     * Assign role to new or updated users based on user type
     * @param $insert
     * @param $changedAttributes
     * @return [boolean]
     */
    public function afterSave($insert,$changedAttributes)
    {
        if($insert==false){

        $role = Usertype::findOne($this->usertype_id)['role'];

        $obj = (object)array('name' => $role);
        
        $r=new DbManager;

        $r->init();

        $r->revokeAll($this->user_id);

        $r->assign($obj, $this->user_id);
        }
        if (parent::afterSave($insert,$changedAttributes)) {
            return true;
        } else {
            return false;
        }
    }
    
     /**
     * @inheritdoc
     */
    public function getEmployeeTrainer()
    {
        return $this->hasMany(TrainerTechnology::className(), ['employee_id' => 'id']);
    }
   

}
