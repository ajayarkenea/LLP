<?php

namespace app\modules\user_management\modules\core\controllers;

use Yii;
use app\modules\master_settings\modules\core\models\Usertype;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use app\models\Password;
use yii\base\Model;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\modules\user_management\modules\core\models\ProfileSetting;
use yii\web\UploadedFile;
use app\modules\user_management\modules\core\models\Guardians;
use app\modules\user_management\modules\core\models\Employee;
use yii\data\ActiveDataProvider;
use app\controllers\BaseController;
use app\modules\user_management\modules\core\models\User;
use app\modules\assets\modules\core\models\Assets;
use app\modules\user_management\modules\core\models\Token;

/**
 * ProgramController implements the CRUD actions for Program model.
 */
class ProfileSettingController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    //seting of change password
    public function actionSetUsername()
    {   
        $upload=new ProfileSetting();
        $model =new Password();
        $this->performAjaxValidation($model);
        $path=$upload->findProfilePic();
               if ($model->load(Yii::$app->request->post())){
            $model->setUsername();
            Yii::$app->session->setFlash('success','Username has been changed successfully...');
             return $this->redirect(['/site/index']);
        }
        return $this->render('setting', [
         'upload' => $upload,
         'path'=>$path,
         'model'=>$model,
        
        ]);
    
    }
     //seting of change password
    public function actionSetEmail()
    {   
        $upload=new ProfileSetting();
        $model =new Password();
        $this->performAjaxValidation($model);
        $path=$upload->findProfilePic();
               if ($model->load(Yii::$app->request->post())){
            $model->setEmail();
            Token::deleteAll('type = :t and user_id = :id', [':t'=>Token::TYPE_RESET_EMAIL_CONFIRMATION,':id' =>Yii::$app->user->identity->id]);
            Yii::$app->session->setFlash('success','Login Email and profile email has been changed successfully...');
             return $this->redirect(['/site/index']);
        }
        return $this->render('setting', [
         'upload' => $upload,
         'path'=>$path,
         'model'=>$model,
        
        ]);
    
    }

   //seting of change password
    public function actionSendMailConfirmationToken($email)
    {   
      
      $user=User::find()->where(['email'=>$email])->one();
      if($user!==null){
        Token::deleteAll('type = :t and user_id = :id', [':t'=>Token::TYPE_RESET_EMAIL_CONFIRMATION,':id' => $user->id]);
        return 'exist';
      }
      $token= new Token();
      $token->id=null;
      $token->user_id=Yii::$app->user->identity->id;
      $token->code=User::generate(6);
      $token->type=Token::TYPE_RESET_EMAIL_CONFIRMATION;
      $token->isNewRecord=true;
      if($token->save(false)){
      
      Yii::$app->sendmail->sendConfirmationCode($email,$token->code);  
       return 'email';
      }
    }

    
    //seting of change password
    public function actionSetting()
    {   
      
      $model =new Password();
      $upload=new ProfileSetting();
      $model->username=$model->findUsername();
      $model->email=$model->findEmail();
      $baseurl=\Yii::$app->request->BaseUrl;
      $path=$upload->findProfilePic(); 
      $this->performAjaxValidation($model); 
      if ($model->load(Yii::$app->request->post()))
      {
      // $model->setPassword($model->newPassword);
         $model->savePassword();
         Yii::$app->session->setFlash('success','Password has been changed successfully...');
         Yii::$app->user->logout(false);
          return $this->goHome();
        }
        
        return $this->render('setting', [
         'model' => $model,
         'upload' => $upload,
         'path'=>$path,

       
       
        ]);
    
    }

 
     //seting of upload profile image
    public function actionUpload()
    {   
        $model=new Password();
        $upload=new ProfileSetting();
 
        $baseurl=\Yii::$app->request->BaseUrl;
        $path=$upload->findProfilePic();
      
         if ($upload->load(Yii::$app->request->post()))
         {
        if (is_string($upload->photo) && strstr($upload->photo, 'data:image')) { 
           $data = $upload->photo;
           list($type, $string) = explode(';', $data);
           $filetype= explode('/', $type);
           $ext=$filetype[1];
           $img = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $upload->photo));
        }else{
            Yii::$app->session->setFlash('danger','Please try again');
            return $this->redirect(['/site/index']);
        }

            switch (Yii::$app->user->identity->user_type) {
              //superadmin
              case Usertype::SUPERADMIN:
          
                  return $this->redirect(['/site/index']);
              break;
                 //guardian     
              case Usertype::GUARDIAN:
                $guardian= Guardians::find()->where(['user_id'=>Yii::$app->user->identity->id])->one(); 
                
                  $assetsId = Assets::uploadImage($img,$ext,2,'profile','profile pic',$guardian->unique_id);
                  $guardian->assets_id=$assetsId;
                if($guardian->save(false)){
                     Yii::$app->session->setFlash('success','Your Image changed Successfully... ');          
                }else{
                    Yii::$app->session->setFlash('danger',Yii::t('app','Failed: Some Technical Error. Try again'));
                }
                 Yii::$app->session->setFlash('success','Photo updated successfully... ');
            return $this->redirect(['/site/index']);
            break;

            case Usertype::STUDENT:
            $student=Student::find()->where(['user_id'=>Yii::$app->user->identity->id])->one(); 
            $assetsObject = [
                    'fileName' => $filelist,
                    'type'=> 1, //student or user or school
                    'title'=> 'profile',
                    'description'=> 'profile pic',
                    'folder'=>$student->unique_id, //it is folder for specific user or student
                  ];
                  $assetsId = Assets::upload($assetsObject);
                  $student->assets_id=$assetsId;
            if($student->save(false)){
                     Yii::$app->session->setFlash('success','Your Image changed Successfully... ');          
                }else{
                    Yii::$app->session->setFlash('danger',Yii::t('app','Failed: Some Technical Error. Try again'));
                }
                 Yii::$app->session->setFlash('success','Photo updated successfully... ');
            break;  
            default:
            $employee=Employee::find()->where(['user_id'=>Yii::$app->user->identity->id])->one();
            $assetsObject = [
                    'fileName' => $filelist,
                    'type'=> 2, //student or user or school
                    'title'=> 'profile',
                    'description'=> 'profile pic',
                    'folder'=>$employee->unique_id, //it is folder for specific user or student
                  ];
                  $assetsId = Assets::upload($assetsObject);
                  $employee->assets_id=$assetsId;
                if($employee->save(false)){
                     Yii::$app->session->setFlash('success','Your Image changed Successfully... ');          
                }else{
                    Yii::$app->session->setFlash('danger',Yii::t('app','Failed: Some Technical Error. Try again'));
                }
              break;
        }
 
            //}
          }
        return $this->render('setting', [
         'upload' => $upload,
         'model' => $model,
         'path'=>$path,
      
        ]);
    
    }

    protected function performAjaxValidation(Model $model)
    {
        if (Yii::$app->request->isAjax && !Yii::$app->request->isPjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                echo json_encode(ActiveForm::validate($model));
                Yii::$app->end();
            }
        }
    }
   
}
