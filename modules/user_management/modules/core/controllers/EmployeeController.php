<?php

namespace app\modules\user_management\modules\core\controllers;

use Yii;
use app\modules\user_management\modules\core\models\Employee;
use app\modules\user_management\modules\core\models\User;
use app\modules\user_management\modules\core\models\EmployeeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\user_management\modules\core\models\Token;
use yii\web\UploadedFile;
use yii\base\Model;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\DynamicModel;
use app\modules\user_management\modules\core\models\Csv;
use app\modules\assets\modules\core\models\Assets;
use yii\helpers\Url;
use app\modules\settings\modules\core\models\Prefix;
use app\modules\training\modules\core\models\TrainerTechnology;
/**
 * EmployeeController implements the CRUD actions for Employee model.
 */
class EmployeeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Employee models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmployeeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Employee model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Employee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
       
        $model = new Employee();
        //  $this->performAjaxValidation($model);  
        $baseurl=\Yii::$app->request->BaseUrl;
        $path=Url::to(['/assets.xeo','id'=>1]);
        $web= \Yii::getAlias('@webroot');
        if ($model->load(Yii::$app->request->post())) {
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {

                $user = new User();
                $user->id=null;
                $user->email=$model->email;
                $user->user_type=$model->usertype_id;
                $user->isNewRecord=true;

                if($user->save(false))
                {
                    $token= new Token();
                    $token->id=null;
                    $token->user_id=$user->id;
                    $token->code=$user->generate(20);
                    $token->type=Token::TYPE_CONFIRMATION;
                    $token->isNewRecord=true;
                    if($token->save(false)){
                      $time=time();
                      $model->unique_id='EMP-'.$time;
                      $model->user_id=$user->id;
                      $model->created_by=Yii::$app->user->identity->id;
                      $filelist = UploadedFile::getInstance($model,'photo');
                       $assetsUserDir = \Yii::getAlias('@assetsUserDir'); 
                           if (!mkdir($assetsUserDir.'/'.$model->unique_id.'', 0777, true)) {
                      die('Failed to create folders...');
                    }
                     
                        if($filelist){
                   
                     $assetsObject = [
                    'fileName' => $filelist,
                    'type'=> 2, //student or user or school
                    'title'=> 'profile',
                    'description'=> 'profile pic',
                    'folder'=>$model->unique_id, //it is folder for specific user or student
                  ];
                  $assetsId = Assets::upload($assetsObject);

                }else{
                    $assetsId =Employee::DEFAULT_ASSET_ID;
                 }

                 $employee_prefix=Prefix::find()->where(['title' => 'employee'])->one();
                 $model->alias=$employee_prefix->prefix.'-'.$time;
                 $model->assets_id=$assetsId;
                 $model->save(false);
                                                         
                  }

              }

             // Yii::$app->sendmail->sendConfirmationMessage($user->id,$user->email, $token->code);     
              $transaction->commit();
              Yii::$app->session->setFlash('success','New Employee created successfully... ');
              return $this->redirect(['employee/index']);
          }
          catch (Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('danger',Yii::t('app','Failed: Some Technical Error. Try again'));
                }
            }
            return $this->render('create', [
                'model' => $model,
                'path'=>$path,
               
                ]);
    }


    /**
     * Updates an existing Employee model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
      
        $model = $this->findModel($id);
        $modelAssets = $this->findAssetsModel($model->assets_id);
        $this->performAjaxValidation($model);  
        $baseurl=\Yii::$app->request->BaseUrl;
        $path=$path=Url::to(['/assets.xeo','id'=>$model->assets_id]);
        if ($model->load(Yii::$app->request->post())) {
        //  $modelAssets->delete();
          $filelist = UploadedFile::getInstance($model,'photo');
                if($filelist){
                     $assetsObject = [
                    'fileName' => $filelist,
                    'type'=> 2, //student or user or school
                    'title'=> 'profile',
                    'description'=> 'profile pic',
                    'folder'=>$model->unique_id, //it is folder for specific user or student
                  ];
                  $assetsId = Assets::upload($assetsObject);
                }else{
                                          $assetsId=1;
                 }

                
                 $model->assets_id=$assetsId;
          $model->updated_by=Yii::$app->user->identity->id;
          $model->save();
          Yii::$app->session->setFlash('success','Employee updated successfully... ');
          return $this->redirect(['employee/index']);
        } else {
        return $this->render('update', [
            'model' => $model,
            'path'=>$path,
            ]);
        }
    }

    /**
     * Deletes an existing Employee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Employee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employee the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($key)
    {
        $id=Yii::$app->encryptor->decrypt($key);
        if (($model = Employee::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

     /**
     * Finds the Employee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employee the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findAssetsModel($id)
    {
        if (($model = Assets::findOne($id)) !== null) {
            return $model;
        } else {
           return 1;
        }
    }

    protected function performAjaxValidation(Model $model)
    {
        if (Yii::$app->request->isAjax && !Yii::$app->request->isPjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                echo json_encode(ActiveForm::validate($model));
                Yii::$app->end();
            }
        }
    }

    /**
     * Creates a new Employee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     /**
     * actionUploadform render the view for to modal with ajax
     */
    public function actionUpload()
    {    
        $model= new Csv();
        return $this->renderAjax('uploadForm', [
            'model' => $model,
          
        ]);
    }

      /**
    *@author RG
    *actionSaveData is getting data from ajax request and calling saveCsvData function
    **/
    public function actionDownload()
    {    
        $path= Yii::$app->getBasePath();      
        Yii::$app->response->xSendFile( $path.'/web/excel-samples/emp.xlsx' ); 
        
    }

    /**
    *@return dataprovider object
    *@author RG
    *Reading data from csv file and render to grid
    **/
    public function actionCsv()
    {    

        $model = new Csv();
        $employee=new Employee;
        $filelist = UploadedFile::getInstance($model, 'file');

        if(!$filelist){
             throw new NotFoundHttpException('Your request can not be processed.');
        }
         if($filelist->name!='employee.xlsx'){
                  Yii::$app->session->setFlash('danger','Please Download sample copy and do not rename and try again...');
                 return $this->redirect(['employee/index']);
             }
       
           $path= Yii::$app->getBasePath();

          $files = glob($path.'/web/upload/employee/*'); 
         // echo "<pre>";print_r($files);exit;
          foreach($files as $file){
          if(is_file($file)) {
              if (file_exists($path.'/web/upload/employee/'.$filelist->name)) {
              chmod($path.'/web/upload/employee/'.$filelist->name,0777);
              unlink($path.'/web/upload/employee/'.$filelist->name);
              } 
            }
          }
         
          $filelist->saveAs($path.'/web/upload/employee/'.$filelist->name);
        
          if(chmod($path.'/web/upload/employee/'.$filelist->name,0777)){
           
          $objPHPExcel = \PHPExcel_IOFactory::load($path.'/web/upload/employee/employee.xlsx');
          $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                     // echo "<pre>"; print_r($sheetData);exit; 
          $dataProvider=$employee->getVerifyCsv($sheetData);
          }else{
             throw new NotFoundHttpException('Technical Error or page not found.');
          }
        if($dataProvider==null){
          return $this->redirect(['employee/index']); 
        }
         return $this->render('csvGrid', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
    *@author RG
    *actionSaveData is getting data from ajax request and calling saveCsvData function
    **/
    public function actionSaveData()
    {    
       $data=json_decode(Yii::$app->request->post('data'),true);
       $model=new Employee;
       $status=$model->saveCsvData($data);
        return $this->redirect(['employee/index']); 
    }




    
}
