<?php

namespace app\modules\user_management\modules\core\controllers;

use Yii;
use app\modules\user_management\modules\core\models\User;
use app\modules\user_management\modules\core\models\UserSearch;
use app\modules\user_management\modules\core\models\Guardians;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SocialPluginsController implements the CRUD actions for SocialPlugins model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
       public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if(!isset($model->blocked_at)||($model->blocked_at==null)){
             $model->blocked_at=2;
            }else{
             $model->blocked_at=1;
            }
    
        if ($model->load(Yii::$app->request->post())) {
            if($model->blocked_at==2){
             $model->blocked_at=NULL;
            }else{
               $model->blocked_at=time();
            }
             $model->blocked_at;
            if(isset($model->passwd) && ($model->passwd!='')){
              $model->password_hash=Yii::$app->getSecurity()->generatePasswordHash($model->passwd);
            }
         
              
             if($model->save(false)){
                 $guardian=Guardians::find()->where(['user_id'=>$model->id])->one();
                 if($guardian!==null){
                $guardian->email=$model->email;
                $guardian->save(false);
            }
         
             }
             Yii::$app->session->setFlash('success','User data updated successfully... ');
            return $this->redirect(['/umt.xeo']);
        } else {
            return $this->render('update', [
                'model' => $model,
              
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * actionLoginAs switch users
     * @param integer $id
     * @return mixed
     */
    public function actionLoginAs($key)
    {
        $id=Yii::$app->encryptor->decrypt($key);
        $initialId = Yii::$app->user->getId(); //here is the current ID, so you can go back after that.
        if ($id == $initialId) {
             \Yii::$app->session->setFlash('danger', \Yii::t('app', 'You are already login with this user'));
             return $this->redirect(['/site/index']); //redirect to any page you like.
        } else {
            $user = User::findOne($id);
            $duration = 0;
            Yii::$app->user->switchIdentity($user, $duration); //Change the current user.
            Yii::$app->session->set('user.idbeforeswitch',$initialId); //Save in the session the id of your admin user.
              \Yii::$app->session->setFlash('success', \Yii::t('app', 'You are successfully login as user'));
            return $this->redirect(['/site/index']); //redirect to any page you like.
        }
    }
    
     /**
     * Finds the SocialPlugins model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SocialPlugins the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($key)
    {   
        $id=Yii::$app->encryptor->decrypt($key);
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
