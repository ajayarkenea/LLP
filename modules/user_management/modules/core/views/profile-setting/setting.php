<?php

use yii\helpers\Html;
use kartik\widgets\FileInput;
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use kartik\editable\Editable;
use yii\widgets\Pjax;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use app\modules\user_management\modules\core\models\User;
use app\assets\SettingAsset;
use dosamigos\croppie\CroppieWidget;
SettingAsset::register($this);


/* @var $this yii\web\View */
/* @var $model app\models\Zone */
/* @var $form yii\widgets\ActiveForm */
?>
               

<div class="col-sm-12 col-md-12 col-lg-10">
    <div class="panel">
        <div class="panel-body">
          <h2 class="title-hero">
                    Profile Setting
                </h2>
            
            <div class="example-box-wrapper">
                <ul class="nav-responsive nav nav-tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab">Change Image</a></li>
                    <li><a href="#tab2" data-toggle="tab">Change Password</a></li>
                    <li><a href="#tab3" data-toggle="tab">Change Username</a></li>
                    <li><a href="#tab4" data-toggle="tab">Change Email</a></li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                      
                          <div class="tour-form">
                              <?php $form = ActiveForm::begin([
                              'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
                              'id'                     => 'upload-form',
                              'action'=>['upload'],
                              //'enableAjaxValidation'   => true,
                              //'enableClientValidation' => false,
                              'fieldConfig' => [
                                'template' => "{label}\n<div class=\"col-xs-8\">{input}{error}</div>",
                                'labelOptions' => ['class' => 'col-xs-2 control-label'],
                              ],
                            ]); ?>
                            <div class="row" >
                             <div class="col-md-12 " style="height: 10px;"></div>
                              <h4 class="col-md-8" >
                                <i class="fa fa-camera-retro "></i>
                                <strong> Image Upload</strong>
                              </h4>
                              <div class="col-md-12 " style="height: 10px;"></div>
                              <div class="col-md-12">
                                 
<?php echo $path;?>
                                  <?php  echo $form->field($upload, 'photo')->widget(\bilginnet\cropper\Cropper::className(), [
    /*
     * elements of this widget
     *
     * buttonId          = #cropper-select-button-$uniqueId
     * previewId         = #cropper-result-$uniqueId
     * modalId           = #cropper-modal-$uniqueId
     * imageId           = #cropper-image-$uniqueId
     * inputChangeUrlId  = #cropper-url-change-input-$uniqueId
     * closeButtonId     = #close-button-$uniqueId
     * cropButtonId      = #crop-button-$uniqueId
     * browseInputId     = #cropper-input-$uniqueId // fileinput in modal
    */
    'uniqueId' => 'image_cropper', // will create automaticaly if not set

    // you can set image url directly
    // you will see this image in the crop area if is set
    // default null
    'imageUrl' =>  "".$path."",     
    'cropperOptions' => [
        'width' => 100, // must be specified
        'height' => 100, // must be specified

        // optional
        // url must be set in update action
        'preview' => [
            'url' => "".$path."",    // (!empty($model->image)) ? Yii::getAlias('@uploadUrl/'.$model->image) : null
            'width' => 100, // must be specified // you can set as string '100%'
            'height' => 100, // must be specified // you can set as string '100px'
        ],

        // optional // default following code
        // you can customize 
        'buttonCssClass' => 'btn btn-primary',

        // optional // defaults following code
        // you can customize 
        'icons' => [
            'browse' => '<i class="fa fa-image"></i>',
            'crop' => '<i class="fa fa-crop"></i>',
            'close' => '<i class="fa fa-crop"></i>',       
            'zoom-in' => '<i class="fa fa-search-plus"></i>',
            'zoom-out' => '<i class="fa fa-search-minus"></i>',
            'rotate-left' => '<i class="fa fa-rotate-left"></i>',
            'rotate-right' => '<i class="fa fa-rotate-right"></i>',
            'flip-horizontal' => '<i class="fa fa-arrows-h"></i>',
            'flip-vertical' => '<i class="fa fa-arrows-v"></i>',
            'move-left' => '<i class="fa fa-arrow-left"></i>',
            'move-right' => '<i class="fa fa-arrow-right"></i>',
            'move-up' => '<i class="fa fa-arrow-up"></i>',
            'move-down' => '<i class="fa fa-arrow-down"></i>',
        ]
    ],

    // optional // defaults following code
    // you can customize 
    'label' => 'Browse', 
    
    // optional // default following code
    // you can customize 
    'template' => '{button}{preview}',

 ]);?>
                                </div>
                            </div>
                            <div class="bg-default content-box text-center pad20A mrg25T col-md-6 col-md-offset-3">

                                <?= Html::submitButton( Yii::t('app', '<i class="ace-icon fa fa-plus"> Update</i>'), ['class' =>  'btn btn-success']) ?>
                                <button type="reset" class="btn btn-info">
                                    <i class="ace-icon fa fa-undo bigger-110"></i>
                                    Reset
                                </button>
                            </div>
                            <?php ActiveForm::end(); ?>
                          </div>
                        </div>
                        
                  <div class="tab-pane" id="tab2">
                     <?php $form = ActiveForm::begin([
                              'options' => ['class' => 'form-horizontal'],
                              'id'                     => 'password-form',
                                 'action'=>['setting'],
                               'enableAjaxValidation'   => true,
                               'enableClientValidation' => false,
                              'fieldConfig' => [
                              'template' => "{label}\n<div class=\"col-xs-8\">{input}{error}</div>",
                              'labelOptions' => ['class' => 'col-xs-4 control-label'],
                              ],
                            ]); ?>
                            <div class="row" >
                             <div class="col-md-12 " style="height: 10px;"></div>
                              <h4 class="col-md-8" >
                                <i class="lg fa fa-lock"></i>
                                <strong> Password</strong>
                              </h4>
                              
                              <div class="col-md-6">
                                <div  id="password">
                                  <?= $form->field($model, 'password')->passwordInput(['maxlength' => 256,'placeholder'=>'Enter Current Password here']) ?>
                                </div>
                                <div  id="newpassword">
                                  <?= $form->field($model, 'newPassword')->passwordInput(['maxlength' => 256,'placeholder'=>'Enter New Password here']) ?>
                                </div>
                                <div  id="confirmpassword">
                                  <?= $form->field($model, 'confirmPassword')->passwordInput(['maxlength' => 256,'placeholder'=>'Enter Confirm Password here']) ?>
                                </div>
                              </div>
                            </div>
                            <div class="bg-default content-box text-center pad20A mrg25T col-md-6 col-md-offset-3">

                                <?= Html::submitButton( Yii::t('app', '<i class="ace-icon fa fa-plus"> Update</i>'), ['class' =>  'btn btn-success']) ?>
                                <button type="reset" class="btn btn-info">
                                    <i class="ace-icon fa fa-undo bigger-110"></i>
                                    Reset
                                </button>
                            </div> 
                          <?php ActiveForm::end(); ?>
                        </div>
                  <div class="tab-pane" id="tab3">
                   <?php $form = ActiveForm::begin([
                              'options' => ['class' => 'form-horizontal'],
                              'id'      => 'pas-form',
                              'action'  =>['set-username'],
                              'enableAjaxValidation'   => true,
                              'enableClientValidation' => false,
                              'fieldConfig' => [
                              'template' => "{label}\n<div class=\"col-xs-8\">{input}{error}</div>",
                              'labelOptions' => ['class' => 'col-xs-4 control-label'],
                              ],
                            ]); ?>
                            <div class="row" >
                              <div class="col-md-12 " style="height: 10px;"></div>
                              <h4 class="col-md-8" >
                                <i class="lg fa fa-user"></i>
                                <strong> Form</strong>
                              </h4>
                            
                              <div class="col-md-6">
                                <div  id="password">
                                  <?= $form->field($model, 'username')->textInput(['maxlength' => 256,'placeholder'=>'Enter Username here']) ?>
                                </div>
                              </div>
                            </div>
                            <div class="bg-default content-box text-center pad20A mrg25T col-md-6 col-md-offset-3">
                                <?= Html::submitButton( Yii::t('app', '<i class="ace-icon fa fa-plus"> Update</i>'), ['class' =>  'btn btn-success']) ?>
                                <button type="reset" class="btn btn-info">
                                    <i class="ace-icon fa fa-undo bigger-110"></i>
                                    Reset
                                </button>
                            </div> 
                          <?php ActiveForm::end(); ?>
                        </div>
                        <div class="tab-pane" id="tab4">
                   <?php $form = ActiveForm::begin([
                              'options' => ['class' => 'form-horizontal'],
                              'id'      => 'email-form',
                              'action'  =>['set-email'],
                              'enableAjaxValidation'   => true,
                              'enableClientValidation' => false,
                              'fieldConfig' => [
                              'template' => "{label}\n<div class=\"col-xs-8\">{input}{error}{hint}</div>",
                              'labelOptions' => ['class' => 'col-xs-4 control-label'],
                              ],
                            ]); ?>
                            <div class="row" >
                              <div class="col-md-12 " style="height: 10px;"></div>
                              <h4 class="col-md-8" >
                                <i class="lg fa fa-envelope"></i>
                                <strong> Change Email Form</strong>
                              </h4>
                            
                              <div class="col-md-6">
                                <div  id="password">
                                 <?= $form->field($model, 'email', [
    'addon' => [
        'append' => [
            'content' => Html::button('Verify Email', ['class'=>' snedToken btn btn-primary']), 
            'asButton' => true
        ]
    ]
])->textInput(['maxlength' => 256,'placeholder'=>'Enter Email here'])->hint('This email change will reflect login email as well as your profile email also.Please click on send confirmation token for new email '); ?>
                                  </div>
                                  </div>
                                  <div class="col-md-10">
                                  <div class="col-md-7">
                                   <?= $form->field($model, 'token')->textInput(['maxlength' => 256,'placeholder'=>'Enter Token here']);?>
                                   </div>
                                                                   
                              </div>
                            </div>
                            <div class="bg-default content-box text-center pad20A mrg25T col-md-6 col-md-offset-3">
                                <?= Html::submitButton( Yii::t('app', '<i class="ace-icon fa fa-plus"> Update</i>'), ['class' =>  'btn btn-success']) ?>
                                <button type="reset" class="btn btn-info">
                                    <i class="ace-icon fa fa-undo bigger-110"></i>
                                    Reset
                                </button>
                            </div> 
                          <?php ActiveForm::end(); ?>
                        </div>
                  </div>

                  </div>
            </div>
        </div>
    </div>
              