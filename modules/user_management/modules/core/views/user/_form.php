<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\master_settings\modules\core\models\Usertype;
use kartik\widgets\Select2;
use app\assets\UserAsset;
UserAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

     <?php $form = ActiveForm::begin([
     'options' => ['class' => 'bv-form'],
     'id'      => 'user-form',
     // 'enableAjaxValidation'   => true,
     // 'enableClientValidation' => false,
     'fieldConfig' => [
     'template' => "{label}\n<div class=\"col-xs-8\">{input}{hint}{error}</div>",
     'labelOptions' => ['class' => 'col-sm-4 control-label'],
    ],
     ]); ?>
    <div class="row">
        <div class="col-md-6">
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'passwd')->textInput(['maxlength' => true]) ?>
    <?php echo $form->field($model, 'user_type')->widget(Select2::classname(), [
            'data' => UserType::getDropDownData(),
            'language' => 'en',
              'options' => ['placeholder' => Yii::t('app','Select Usertype...')],
                'pluginOptions' => [
                 'allowClear' => false
                ],
                //'disabled' => !$model->isNewRecord,
    ]);?>
    <?php echo $form->field($model, 'blocked_at')->widget(Select2::classname(), [
            'data' =>[1=>'block',2=>'unblock'],
            'language' => 'en',
               'options' => ['placeholder' => Yii::t('app','Select Usertype...')],
                'pluginOptions' => [
                   'allowClear' => false
                ],
                //'disabled' => !$model->isNewRecord,
    ])->label('Blocked Status');?>

</div>
</div>
    <div class="form-group btn-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success ' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
