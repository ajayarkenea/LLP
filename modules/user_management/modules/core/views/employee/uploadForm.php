<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use app\assets\EmployeeAsset;

EmployeeAsset::register($this);
?>

   <div class="upload-form">
    <!-- start ActiveForm -->
   <?php $form = ActiveForm::begin([
        'id'=>'csv-form',
        'action'=>['csv'],
        //'enableAjaxValidation'=>false,
        'options' => ['class' => 'form-horizontal',
        'enctype' => 'multipart/form-data'
  
        ],
        'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-xs-10\">{input}{error}</div>",
        'labelOptions' => ['class' => 'col-xs-1 control-label'],
        ],
        ]); ?>
      <div class="row" >
    <div class="form-column col-md-12" >Instructions:-
    <p class="help-box medium text-muted">For bulk upload please download sample file and fill data and upload.</p>
    <?= Html::a( '  Download Sample file here', ['/employee/download']) ?>
    </div>

       <div class="col-md-12 " style="height: 30px;"></div>

        <div class="form-column col-md-12" > 
         
         <?php echo $form->field($model, 'file')->widget(FileInput::classname(), [
          'name' => 'csvfile',
          'pluginOptions' => [
          'browseClass' => 'btn btn-success',
          'uploadClass' => 'btn btn-info',
          'removeClass' => 'btn btn-danger',
          'showPreview' => false,
          'showCaption' => true,
          'showTextbox' => false,
          'removeIcon' =>'<i class="fa fa-trash"></i>',
          ]
          ]);
          ?>
        </div>
     </div>
     <div class="form-actions">
               

                 <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove bigger-110"></i>Close</button>

                <?php if ($model->isNewRecord): ?>
                <button type="reset" class="btn btn-info">
                    <i class="fa fa-undo bigger-110"></i>
                    Reset
                </button>
                <?php endif ?>
            </div> 
              <?php ActiveForm::end(); ?>
  </div>




