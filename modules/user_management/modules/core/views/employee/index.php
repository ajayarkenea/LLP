<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use app\assets\EmployeeAsset;
use app\models\Employee;
use yii\helpers\ArrayHelper;
use mdm\admin\components\Helper;
EmployeeAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Employees');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index">

    <?php Pjax::begin(); ?>    

 <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            //'id',
            //'form_id',
            //'cust_field_id',
            //'user_id',
          
            // 'photo',
             'fname',
            // 'mname',
             'lname',
             'dob',
             'email:email',
            // 'phone',
            // 'department_id',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'is_deleted',
            // 'status',

            ['class' => '\kartik\grid\ActionColumn',
                     'template' => Helper::filterActionColumn('{view}&nbsp;{update}&nbsp;{delete}'),                    
                    'urlCreator' => function ($action, $model, $url, $index) {
                         $hash=Yii::$app->encryptor->encrypt($model->id); 
                         if ($action === 'view') {
                            return Url::to(['employee/view','id'=>$hash]);
                         }else if($action === 'update'){
                          return Url::to(['employee/update','id'=>$hash]);
                         }else if($action === 'delete'){
                          return Url::to(['employee/delete','id'=>$hash]);
                         }
                    },
            ],

        ],
         'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="white fa fa-users "></i> Employee </h3>',
            'type'=>'primary',
            'before'=>Html::a(Yii::t('app', '<i class="glyphicon glyphicon-plus"></i> Create {modelClass}', ['modelClass' => 'Employee',]), ['create'], ['class' => 'btn btn-primary']) 
            .' '.Html::button('<i class="fa fa-upload"></i> Upload ', [
                                                     'title' => Yii::t('app', 'Upload'),'href'=>"#",'class'=>'uploadmodal btn btn-warning']),

            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Search', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>true,
            'pager'=>false
            ],
            'toolbar' => [
              //'{export}'
              ],
            'exportConfig' => [
            GridView::HTML =>[],
            //  GridView::PDF =>[],
            GridView::TEXT => [],
            GridView::EXCEL => [],
            ],
            'bootstrap'=>true,
            'bordered'=>false,
            'striped'=>true
    ]); ?>




<?php Pjax::end(); ?>
    
</div>
