<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
	'modelClass' => 'Employee',
	]) . $model->fname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Employees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fname, 'url' => ['view', 'id' => Yii::$app->encryptor->encrypt($model->id)]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="employee-update">

	<div class="grey panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
		</div>
		<div class="panel-body">

			<div class="col-lg-12">

				<?= $this->render('_form', [
					'model' => $model,
					 'path'=>$path,

					]) ?>
				</div>
			</div>
		</div>
	</div>
