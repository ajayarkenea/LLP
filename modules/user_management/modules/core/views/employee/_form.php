<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\School;
use app\modules\settings\modules\core\models\Department;
use kartik\widgets\FileInput;
use kartik\datecontrol\DateControl;
use kartik\date\DatePicker;
use kartik\widgets\Select2;
use app\modules\settings\modules\core\models\Classes;
use app\modules\settings\modules\core\models\AcadamicYear;
use app\modules\master_settings\modules\core\models\Usertype;
use app\modules\master_settings\modules\core\models\Cities;
use app\assets\EmployeeAsset;
use yii\web\JsExpression;
use wbraganca\dynamicform\DynamicFormWidget;
EmployeeAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Employee */
/* @var $form yii\widgets\ActiveForm */

?>
<?php // $path=Yii::$app->request->BaseUrl;//Yii::$app->getBasePath();?>

<div class="employee-form">
   <?php $form = ActiveForm::begin([
      'options' => ['class' => 'form-horiuzontal',
         'enctype' => 'multipart/form-data'
      ],
      'id' => 'dynamic-form',               
      // 'enableAjaxValidation'   => true,  // this will enable ajax validation for form
      // 'enableClientValidation' => false,
   ]); ?>

         <div class="row">
         <div class="col-md-12">
            <div class="col-md-3 ">
               <?php echo $form->field($model, 'photo')->widget(FileInput::classname(),[
                  'name' => 'photo',
                   'options'=>[
                    'multiple'=>false,
                    ],
                    'pluginOptions' => [
                      'initialPreview'=>[
                        "".$path."",        
                      ],
                      'browseClass' => 'btn btn-raised btn-primary',
                      'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                      'browseLabel' =>  'Select ',
                      'removeLabel'=>'Remove',
                      'removeClass' => 'btn btn-raised btn-warning',
                      'showRemove' => false,
                      'showUpload' => false,
                      'removeIcon' => '<i class="glyphicon glyphicon-minus"></i> ',
                      'initialPreviewAsData'=>true,
                      'overwriteInitial'=>true,
                              //'maxFileSize'=>1024
                    ]
                                    ]);//->hint('Recent pic of applicant');
                                  ?>
                          
            </div>
            
               <div class="col-md-4">
                  <?php echo $form->field($model, 'usertype_id')->widget(Select2::classname(), [
                    'data' => UserType::getDropDownData(),
                    'language' => 'en',
                    'options' => ['placeholder' => Yii::t('app','Select Usertype...')],
                    'pluginOptions' => [
                    'allowClear' => false
                    ],
                    //'disabled' => !$model->isNewRecord,
                  ]);?>
              
                  <?php echo $form->field($model, 'department_id')->widget(Select2::classname(), [
                    'data' => Department::getDropDownData(),
                    'language' => 'en',
                    'options' => ['placeholder' => Yii::t('app','Select Department...')],
                    'pluginOptions' => [
                    'allowClear' => false
                    ],
                    //'disabled' => !$model->isNewRecord,
                    ]);?>
                 
                    <?php 
                    echo $form->field($model, 'dob')->widget(DatePicker::classname(), [
                      'options' => ['placeholder' => 'Enter birth date ...'],
                      'pluginOptions' => [
                         'startDate'=> '0d',
                         'format' =>\Yii::$app->formatter->dateFormat,
                          'autoclose'=>true
                      ]
                  ]);?>
                 
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                 <?php echo $form->field($model, 'native_city_id')->widget(Select2::classname(), [
    'initValueText' => empty($model->native_city_id) ? '' :  Cities::findOne($model->native_city_id)->title, // set the initial display text
    'options' => ['placeholder' => 'Search for a city ...'],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 3,
        'language' => [
            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
        ],
        'ajax' => [
            'url' => \yii\helpers\Url::to(['/master_settings/cities/citylist']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term}; }')
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('function(city) { return city.text; }'),
        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
    ],
]);?>
            </div>
           
               <div class="col-md-4">
                  <?= $form->field($model, 'fname')->textInput(['maxlength' => true]) ?>
               
                  <?= $form->field($model, 'mname')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'lname')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'phone')->textInput() ?>
                    <?php echo $form->field($model, 'current_city_id')->widget(Select2::classname(), [
    'initValueText' => empty($model->current_city_id) ? '' :  Cities::findOne($model->current_city_id)->title, // set the initial display text
    'options' => ['placeholder' => 'Search for a city ...'],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 3,
        'language' => [
            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
        ],
        'ajax' => [
            'url' => \yii\helpers\Url::to(['/master_settings/cities/citylist']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term}; }')
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('function(city) { return city.text; }'),
        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
    ],
]);?>

 
               </div>
            
            </div>
            
            </div>
     
      <div class="bg-default content-box text-center pad20A mrg25T col-md-6 col-md-offset-3">
         <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ace-icon fa fa-plus"> Save</i>') : Yii::t('app', '<i class="ace-icon fa fa-pencil"> Update</i>'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

         <?= Html::a('<i class="ace-icon fa fa-remove"> Cancel</i>', ['employee/index'], ['class'=>'btn btn-danger']); ?>
         <?php if ($model->isNewRecord): ?>
         <button type="reset" class="btn btn-info">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
         </button>
         <?php endif ?>
      </div>
    <?php ActiveForm::end(); ?>

</div>
   