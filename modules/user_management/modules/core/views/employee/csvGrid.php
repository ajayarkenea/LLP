<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\assets\CsvAsset;
use app\assets\EmployeeAsset;

EmployeeAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Employee');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php  $json=json_encode($dataProvider->allModels); //echo "<pre>"; print_r($dataProvider->allModels);
?>
<head>
    <script type="text/javascript">
       var data = <?php echo $json ?>;
    </script>
</head>
<div class="nomination-index">
    <div class="panel panel-default">

    <div class="panel-body">
      <div class="row">
      <div class="col-lg-12">
       <?=  GridView::widget([
    'dataProvider'=> $dataProvider,
   
    'columns' =>[
     [ 'class' => '\kartik\grid\SerialColumn'],

            
            'fname',
            'email',
            'mname',
            

      
        ],
         'toolbar' => [
                       '{export}',

                      ], 
             'striped' => false,
    'panel' => [
        'heading'=>'<h3 class="panel-title"><i class="white fa fa-ticket"></i> Employee</h3>',
        'type'=>'default',

        'before'=>Html::a(Yii::t('app', '<i class="white fa fa-arrow-left"></i>  Back {modelClass}', ['modelClass' => 'Employee']), ['index'], ['class' => 'btn btn-primary']).' '.
        Html::button('<i class="white fa fa-save"></i>  Save', [
                        'title' => Yii::t('app', 'Save'),'href'=>"#",'class'=>'savedata btn btn-success ']),
         'showFooter'=>true,

    
        'pager'=>false
    ],
           'exportConfig' => [
               GridView::HTML =>[],
            //   GridView::PDF =>[],
               GridView::TEXT => [],
               GridView::EXCEL => [],
           ]
]);

?>    
      </div>
      </div>
    </div>
  </div>
</div>

