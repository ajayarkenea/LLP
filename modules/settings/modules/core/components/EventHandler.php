<?php

namespace app\modules\settings\modules\core\components;

abstract class EventHandler
{
    /**
     * @param \app\modules\social_plugins\events\SampleEvent $event
     */
    public static function sampleEventHandler($event)
    {
        // do something
    }
}