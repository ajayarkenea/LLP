<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\dynagrid\DynaGrid;
use kartik\editable\Editable;
use kartik\widgets\SwitchInput;
use app\assets\ConfigAsset;
use app\modules\settings\modules\core\models\Config;
 ConfigAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel app\models\ConfigSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Configs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-index">
    <?php Pjax::begin(); ?>  
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'kartik\grid\SerialColumn'],
                [
                    'attribute'=>'description',
                    'vAlign'=>'middle',
                ],
                [
                    'attribute' => 'value',
                    'value' => function($model, $key, $index, $column) {
                        $config=Config::findDisplayValue($model->field); 
                        return Html::a(
                        '<i class="fa fa-thumb"></i>'.$config.'',
                            '#', 
                            [
                                'id'=>'grid-custom-button',
                                'data-pjax'=>true,
                                'data'=>$model->id,
                                'class'=>'clk',
                            ]
                        );
                    },
                    'filter'=>false,
                    'format'=>'raw',
                ],
            ],

            'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="white fa fa-users "></i> Config  </h3>',
            'type'=>'primary',
             'before'=>false ,
            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Search', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>true,
            'pager'=>false
             ],
            'toolbar' => [
              //'{export}'
              ],
            'exportConfig' => [
            GridView::HTML =>[],
            //  GridView::PDF =>[],
            GridView::TEXT => [],
            GridView::EXCEL => [],
            ]
    ]); ?>

<?php Pjax::end(); ?>
</div>
