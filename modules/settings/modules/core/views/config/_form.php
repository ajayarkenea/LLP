<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Questions;
use app\models\Questionnaire;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model app\models\QuesCategory */

$this->title = '';
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Questionnaires'), 'url' => ['index']];
$this->params['breadcrumbs'][] ='';
?>

                            <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <i class="fa fa-file"></i> Fill Form
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-body">

                         <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL,
                         //   'enableAjaxValidation'   => true,'enableClientValidation' => false,
                          'formConfig'=>['labelSpan'=>3]]);
                                                            echo Form::widget([
                                                              'model'       => $model,
                                                              'form'        => $form,
                                                              'columns'     => 1,
                                                              'attributes'  =>  $data
                                                            ]);?>

                              <div class="clearfix">
                         <div class="form-actions">'
                             <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove bigger-110"></i>Close</button>
                              <?php echo Html::submitButton('Submit', ['type'=>'button', 'class'=>'btn btn-primary']); ?>  
                              </div>
                              </div>
                                
                                <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                                   