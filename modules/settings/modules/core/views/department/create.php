<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Department */

$this->title = Yii::t('app', 'Create Department');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Departments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-create">

     
        <div class="col-lg-12">
          
<?= $this->render('_form', [
        'model' => $model,
       
        
        
    ]) ?>

</div>
</div>





</div>
