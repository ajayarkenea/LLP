<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Department */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel">
  <div class="panel-body">
  <?php $form = ActiveForm::begin([
  'options' => ['class' => 'form-horizontal bordered-row'],
  'id'                     => 'password-form',
  //'enableAjaxValidation'   => true,
  // 'enableClientValidation' => false,
  'fieldConfig' => [
   'template' => "{label}\n<div class=\"col-xs-9\">{input}{error}</div>",
   'labelOptions' => ['class' => 'col-sm-3 control-label'],
  ],
  ]); ?>
     <div class="row">
        <div class="col-md-6">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        </div>

        <div class="bg-default content-box text-center pad20A mrg25T col-md-6 col-md-offset-3">
                       <?= Html::a('<i class="ace-icon fa fa-remove"> Cancel</i>', ['department/index'], ['class'=>'btn  btn-danger']);?>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ace-icon fa fa-plus"> Save</i>') : Yii::t('app', '<i class="ace-icon fa fa-pencil"> Update</i>'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
     <!--    <div class="col-md-12" style="height: 30px;"></div>
        <div class="form-group">
          <div class="col-md-10 col-md-offset-1">
           
          </div>
        </div>
         <div class="col-md-12" style="height: 10px;"></div>
      </fieldset>  -->
    <?php ActiveForm::end(); ?> 
  </div>
</div>

