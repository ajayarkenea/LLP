<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Department */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Department',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Departments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => Yii::$app->encryptor->encrypt($model->id)]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="department-update">


<div class="grey panel panel-info">
    <div class="panel-heading">
    	  <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
     </div>
    <div class="panel-body">
     
        <div class="col-lg-12">
          
<?= $this->render('_form', [
        'model' => $model,
       
        
        
    ]) ?>
</div>
</div>
</div>


   

</div>
