<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ClassesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Department');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="department-index">

<?php Pjax::begin(); ?>   

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],


           
            'title:ntext',
            
            
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
            // 'is_delete',

            ['class' => '\kartik\grid\ActionColumn',
                                        
                  'template' => '{update}&nbsp;{delete}',

'urlCreator' => function ($action, $model, $url, $index) {
                         $hash=Yii::$app->encryptor->encrypt($model->id); 
                         if ($action === 'view') {
                            return Url::to(['department/view','id'=>$hash]);
                         }else if($action === 'update'){
                          return Url::to(['department/update','id'=>$hash]);
                         }else if($action === 'delete'){
                          return Url::to(['department/delete','id'=>$hash]);
                         }
                    },
            ],

        ],
         'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="white fa fa-users "></i> Department </h3>',
            'type'=>'primary',
            'before'=>Html::a(Yii::t('app', '<i class="glyphicon glyphicon-plus"></i> Create {modelClass}', ['modelClass' => 'Department',]), ['create'], ['class' => 'btn btn-primary']) ,
            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Search', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>true,
            'pager'=>false
            ],
            'toolbar' => [
              //'{export}'
              ],
            'exportConfig' => [
            GridView::HTML =>[],
            //  GridView::PDF =>[],
            GridView::TEXT => [],
            GridView::EXCEL => [],
            ]
    ]); ?>







<?php Pjax::end(); ?></div>




   