<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\modules\settings\modules\core\models\EmailTemplate */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row ">
    <div class="col-md-9">
<div class="panel">
  <div class="panel-body">
    <?php $form = ActiveForm::begin([
      'options' => ['class' => ''],
      'id' => 'password-form',
      //'enableAjaxValidation'   => true,
      //'enableClientValidation' => false,
      // 'fieldConfig' => [
      // 'template' => "{label}\n<div class=\"col-xs-9\">{input}{error}</div>",
      // 'labelOptions' => ['class' => 'col-sm-3 control-label'],
      // ],
      ]); ?>
      <div class="row ">
       <div class="col-md-9">
        
          <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'readOnly' => !$model->isNewRecord && Yii::$app->user->identity->username != 'superadmin']) ?>
      
          <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'body')->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
            'preset' => 'standard'
           ]) ?>

           <?= $form->field($model, 'bcc_email' )->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'variables')->textarea(['rows' => 6, 'readOnly' => !$model->isNewRecord  && Yii::$app->user->identity->username != 'superadmin']) ?>

          <?= $form->field($model, 'status')->widget(SwitchInput::classname(), ['pluginOptions'=>[
            'handleWidth'=>60,
            'onText'=>'Active',
            'offText'=>'Inactive'
           ]]); ?> 

             </div>

            </div>
            <div class="bg-default content-box text-center pad20A mrg25T col-md-6 col-md-offset-3">

             <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ace-icon fa fa-plus"> Save</i>') : Yii::t('app', '<i class="ace-icon fa fa-pencil"> Update</i>'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
             <?= Html::a('<i class="ace-icon fa fa-remove"> Cancel</i>', ['/et.list.xeo'], ['class'=>'btn  btn-danger']);?>
           </div>

           <?php ActiveForm::end(); ?> 
         </div>
       </div>
   </div>

       <div class="col-md-3">
<div class="panel panel-warning">
  <div class="panel-heading">
    <i class="fa fa-dollar"></i> Variables
      <div class="clearfix"></div>
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <p>Following variables names can be used in the email template.</p>
        <table class="table table-bordered" >
          <thead>
            <tr>
              <th>Variables</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><code>$parent_name</code></td>
              <td>To fetch parent name</td>
            </tr>
            <tr>
              <td><code>$student_name</code></td>
              <td>To fetch student name</td>
            </tr>
            <tr>
              <td><code>$academic_year</code></td>
              <td>To fetch academic Year</td>
            </tr>
            <tr>
              <td><code>$grade</code></td>
              <td>To fetch grade</td>
            </tr>
            <tr>
              <td><code>$application_url</code></td>
              <td>To fetch application url</td>
            </tr>
          </tbody>
        </table>
    </div>  

  </div>
               


 </div>
 </div>
 </div>
