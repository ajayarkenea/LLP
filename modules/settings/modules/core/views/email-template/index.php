<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\settings\modules\core\models\EmailTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Email Templates');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-template-index">

    <?php Pjax::begin(); ?>    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],


            //'id',
            'title',
            'variables',
            'subject',
            //'body:ntext',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'status',

            ['class' => '\kartik\grid\ActionColumn',
                                        
                  'template' => '{view}&nbsp;{update}',

                    'urlCreator' => function ($action, $model, $url, $index) {
                         $hash=Yii::$app->encryptor->encrypt($model->id); 
                         if ($action === 'view') {
                            return Url::to(['email-template/view','id'=>$hash]);
                         }else if($action === 'update'){
                          return Url::to(['email-template/update','id'=>$hash]);
                         }else if($action === 'delete'){
                          return Url::to(['email-template/delete','id'=>$hash]);
                         }
                    },
            ],

        ],

         'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="white fa fa-users "></i> Email Template </h3>',
            'type'=>'primary',
            'before'=>Html::a(Yii::t('app', '<i class="glyphicon glyphicon-plus"></i> Create {modelClass}', ['modelClass' => 'EmailTemplate',]), ['create'], ['class' => 'btn btn-primary']) ,
            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Search', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>true,
            'pager'=>false
          ],
            'toolbar' => [
              //'{export}'
              ],
            'exportConfig' => [
            GridView::HTML =>[],
            //  GridView::PDF =>[],
            GridView::TEXT => [],
            GridView::EXCEL => [],
            ]
    ]); ?>

<?php Pjax::end(); ?>
  
</div>
