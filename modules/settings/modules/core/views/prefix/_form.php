<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\settings\modules\core\models\Prefix */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="panel">
  <div class="panel-body">
    <?php $form = ActiveForm::begin([
      'options' => ['class' => ''],
      'id' => 'password-form',
      //'enableAjaxValidation'   => true,
      //'enableClientValidation' => false,
      // 'fieldConfig' => [
      // 'template' => "{label}\n<div class=\"col-xs-9\">{input}{error}</div>",
      // 'labelOptions' => ['class' => 'col-sm-3 control-label'],
      // ],
      ]); ?>
      <div class="row ">
       <div class="col-sm-12">
        
        <div class="col-md-6">
          <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

         <?= $form->field($model, 'prefix')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'counter')->textInput() ?>
          </div>
        </div>

        
            </div>
            <div class="bg-default content-box text-center pad20A mrg25T col-md-6 col-md-offset-3">

             <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '<i class="ace-icon fa fa-plus"> Save</i>') : Yii::t('app', '<i class="ace-icon fa fa-pencil"> Update</i>'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
             <?= Html::a('<i class="ace-icon fa fa-remove"> Cancel</i>', ['/prefix.list.xeo'], ['class'=>'btn  btn-danger']);?>
           </div>

           <?php ActiveForm::end(); ?> 
         </div>
       </div>
