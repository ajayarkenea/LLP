<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\settings\modules\core\models\PrefixSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Prefixes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prefix-index">

    <?php Pjax::begin(); ?>    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            'title',
            'prefix',
            'counter',
            //'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'is_deleted',
            // 'status',

            ['class' => '\kartik\grid\ActionColumn',
                                        
                  'template' => '{update}',

                    'urlCreator' => function ($action, $model, $url, $index) {
                         $hash=Yii::$app->encryptor->encrypt($model->id); 
                         if ($action === 'view') {
                            return Url::to(['prefix/view','id'=>$hash]);
                         }else if($action === 'update'){
                          return Url::to(['prefix/update','id'=>$hash]);
                         }else if($action === 'delete'){
                          return Url::to(['prefix/delete','id'=>$hash]);
                         }
                    },
            ],

        ],

         'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="white fa fa-users "></i> Prefix </h3>',
            'type'=>'primary',
             'before'=>Html::a(Yii::t('app', '<i class="glyphicon glyphicon-plus"></i> Create {modelClass}', ['modelClass' => 'Prefix',]), ['create'], ['class' => 'btn btn-primary']) ,
            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Search', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>true,
            'pager'=>false
          ],
            'toolbar' => [
              //'{export}'
              ],
            'exportConfig' => [
            GridView::HTML =>[],
            //  GridView::PDF =>[],
            GridView::TEXT => [],
            GridView::EXCEL => [],
            ]
    ]); ?>

<?php Pjax::end(); ?>
  
</div>
