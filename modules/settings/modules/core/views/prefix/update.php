<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\settings\modules\core\models\Prefix */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Prefix',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Prefixes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="prefix-update">

    <div class="grey panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
			</div>
		<div class="panel-body">

			<div class="col-lg-12">
				<?= $this->render('_form', [
					'model' => $model,

					]) ?>
			</div>
		</div>
	</div>

</div>
