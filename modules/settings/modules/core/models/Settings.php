<?php

namespace app\modules\settings\modules\core\models;

use app\components\modules\ModuleActiveRecordTrait;
use app\modules\settings\events\SampleEvent;
use app\modules\settings\Settings;
use yii\db\ActiveRecord;

class Settings extends ActiveRecord
{
    use ModuleActiveRecordTrait;

    public function create()
    {
        Settings::triggerEvent(Settings::SAMPLE_EVENT, new SampleEvent());
    }
}