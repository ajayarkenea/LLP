<?php

namespace app\modules\settings\modules\core\models;

use Yii;
use kartik\editable\Editable;
use kartik\widgets\SwitchInput;
use app\models\BaseModel;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\settings\modules\core\models\Timezone;
use app\modules\settings\modules\core\models\Statuses;
use app\modules\admission\modules\core\models\Applications;
use yii\helpers\Html;
/**
 * This is the model class for table "config".
 *
 * @property integer $id
 * @property string $field
 * @property string $value
 * @property strng $module_name
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $status
 * @property integer $is_deleted
 */
class Config extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'required'],
            [['module_name', 'created_by', 'updated_by', 'status', 'is_deleted'], 'integer'],
            [['created_at', 'updated_at','value'], 'safe'],
            [['field', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'field' => Yii::t('app', 'Field'),
            'value' => Yii::t('app', 'Value'),
            'module_name' => Yii::t('app', 'Module '),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'status' => Yii::t('app', 'Status'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
        ];
    }
    /**
    *@uses 
    *@author SW
    *@param [ques_id]string
    **/
    public function generate($id){
      $data= Config::findOne($id);
      $retVal=[];
      $field=$data->field;
      $retVal['field']=$this->findConfigValue('static');
      $retVal['value']=$this->findConfigValue($field);
      
        return $retVal;
      }
    /**
     * findConfigValue update value for activate theme.
     * This method will call from ajax 
     * @param value of theme ex. 1, 2, 3
     * @author AJ
     */
     public function findConfigValue($field)
    {
      
        switch ($field) {
         
          case 'Connect with Facebook':
          case 'Connect with Twitter':
          case 'Connect with Gmail':
          
            return [
                      'name'=>'field',
                      'type'=>'radioButtonGroup',
                      'widgetClass'=> '',
                      'hint'=> '',
                      'options'=>[
                                    'inline'=>true,
                                  ],
                       'items'=>[1=>'Active', 0=>'Inactive'],
                    ];
                  break;
           case 'date_format':
                return [
                      'name'=>'field',
                      'type'=>'widget',
                      'widgetClass'=> '\kartik\widgets\Select2',
                      'options'=>[
                                   'options' => [ 'placeholder' => 'Select dateformat...'],
                                  'data'=>\Yii::$app->params['dateFormats'],],
                      ];
                  break;
                  case 'company_logo':
                  case 'favicon':
                  case 'school_logo':
                  case 'district_logo':
                  case 'mail_header_logo':
                  case 'mail_footer_logo':

                  return [
                      'name'=>'field',
                      'type'=>'widget',
                      'widgetClass'=> '\kartik\widgets\FileInput',
                          'pluginOptions' => [
                          'showPreview' => false,
                          'showCaption' => true,
                          'showRemove' => true,
                          'showUpload' => false
                      ],
                      
                      ];
                  break;
                   case 'company_name':
                   case 'confirm_account_time':
                   case 'app_url':
                   case 'google_analytic':
                  return [
                      'name'=>'field',
                      'type'=>'textInput',
                      
                      ];
                   break;
                   case 'langauges':
                   return [
                      'name'=>'field',
                      'type'=>'widget',
                      'widgetClass'=> '\kartik\widgets\Select2',
                      'options'=>[
                                   'options' => [ 'placeholder' => 'Select Langauge...'],
                                  'data'=> \Yii::$app->params['langauges']],
                      ];    
                  break;
                  case 'time_zone':
                   return [
                      'name'=>'field',
                      'type'=>'widget',
                      'widgetClass'=> '\kartik\widgets\Select2',
                      'options'=>[
                                   'options' => [ 'placeholder' => 'Select timezone...'],
                                  'data'=> Timezone::getDropDownData()],
                         'pluginOptions' => [
                              'allowClear' => true,
                              'minimumInputLength' => 3,
                          ],          
                      ];    
                  break;         
                  case 'static':
                  return [
                      'name'=>'hidden',
                      'type'=>'staticInput',
                      
                      ];
                  break;

                }
    }

    /**
     * findDisplayValue update value for activate theme.
     * This method will call from ajax 
     * @param value of theme ex. 1, 2, 3
     * @author AJ
     */
     public static function findDisplayValue($field)
    {       
        $model=Self::find()->where(['field'=>$field])->one();
          if(($field=='Connect with Gmail')){
               
                if($model->value==1){
                 return Self::gethtmlstatus('lg','success','','Activate');  
                }elseif($model->value!=1){
                  return Self::gethtmlstatus('lg','danger','','Deactivate');  
                }else{
                  return 'Not Set';
                }
          }elseif(($field=='date_format')||($field=='company_name') ||($field=='confirm_account_time') ||($field=='recovery_account_time')||($field=='app_url') ||($field=='langauges') ||($field=='time_zone') || ($field=='google_analytic')){
               if(($model->value==null) ||(!isset($model->value))){
                  return 'Not Set';
               }else{
                return $model->value;
               }

          }elseif(($field=='company_logo') ||($field=='school_logo') ||($field=='district_logo') ||($field=='favicon') ||($field=='mail_header_logo') ||($field=='mail_footer_logo')){
               return Html::img( Url::to([''.$model->value.'']),
                        ['width' => '50px','height' => '50px','class'=>'meta-image img-bordered img','alt'=>$field]);
          }

           
    }
    /**
     * isDisplayed update value for activate theme.
     * @param value of theme ex. 1, 2, 3
     * @author AJ
     */
     public static function isDisplayed($field){
         $model=Self::find()->where(['field'=>$field])->one();
         if(($field=='Connect with Gmail')){
           
            if($model!==null){
            if($model->value==1){
                return true;
            }else{
                 return false;
            }
          }
        }
    }

    public static function loginPluginWidget(){
       $arr=\Yii::$app->params['emsPlugins'];
        $retVal=[];
        $icons=[
                'Enquiry Enable On login'=>'glyph-icon font-red icon-bullhorn',
                'Application Enable On login'=>'glyph-icon font-primary icon-file',
                'Tour Enable On login'=>'glyph-icon font-green icon-globe',
              ];
        foreach($arr as $key => $value) {
          //do something with your $key and $value;
          $config=Config::isDisplayed($key);
          if($config==1){
            
            $retVal[$key]=$value;
          }        
        }
      $content=[];
      foreach ($retVal as $key => $value) {
        $exp=explode(' ', $key);
        $title= $exp[0];
        $col=12/count($retVal);
        $url= Url::toRoute($value);
        $content[]= '<li class="col-md-'.$col.'"><a href="'.$url.' " class="list-group-item"  target = "_blank"><i class="'.$icons[$key].'"></i>'.$title.'
                    </a></li>';

      }
      $main_col=4*count($content);
        if(count($content)>0){
        
          $header_html='<div class="panel col-md-'.$main_col.'">
            <div class="panel-body">
                 
                 <div class="example-box-wrapper">
                 <div class="mrg10A"></div>
                 <ul class="list-group list-group-separator row list-group-icons">';
                 $footer_html='  </div>
            </div>
        </div>';
      echo $header_html. implode('', $content).$footer_html;
      }else{
        return false;
      }
  }

   
  public static function LogoHtml(){
     $schoolLogo=Self::getSchoolLogoHtml();
     $districtLogo=Self::getDistrictLogoHtml();
     $base=Url::base(true);
     if(($schoolLogo!==false) && ($districtLogo!==false)){
     
       echo '<div class="content-box border-top border-green clearfix login-box">
       <div class=" col-md-6 logo-bg">'.$schoolLogo.'</div>
         <div class=" col-md-6 pull-right">'.$districtLogo.'</div>
        </div>';

      } elseif(($schoolLogo===false) && ($districtLogo!==false)){
           echo '<div class="content-box border-top border-green clearfix login-box">
           <div class=" col-md-6 logo-center">'.$districtLogo.'</div>
        </div>';
      } elseif(($schoolLogo!==false) && ($districtLogo===false)){
           echo '<div class="content-box border-top border-green clearfix login-box">
           <div class=" col-md-6 logo-center">'.$schoolLogo.'</div>
        </div>';
      }else{
        return false;
      }

  }

  public static function getSchoolLogoHtml(){
    $schoolLogo=Config::find()->where(['field'=>'school_logo'])->one();
    if($schoolLogo!==null){
      $base=Url::base(true);
      $url= Url::toRoute("/site/index");
      return '<a href="'.$url.'"  title="">
                <img src="'.$base.$schoolLogo->value.'" class="pull-left login-logo-left-margin-90" alt="" id="logoinlogo" >
              </a>';
    }else{
      return false;
    }
  }

  public static function getDistrictLogoHtml(){
    $districtLogo=Config::find()->where(['field'=>'district_logo'])->one();
    if($districtLogo!==null){
    $base=Url::base(true);
     $url= Url::toRoute("/site/index");
     return '<a href="'.$url.'"  title="">
                  <img src= "'.$base.$districtLogo->value.'" class=" pull-right login-logo-right-margin-90" alt="" id="logoinlogo" >
              </a>';
            }else{
              return false;
            }
  }


}
