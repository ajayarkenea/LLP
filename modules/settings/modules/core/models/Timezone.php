<?php

namespace app\modules\settings\modules\core\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\BaseModel;
/**
 * This is the model class for table "timezone".
 *
 * @property integer $id
 * @property string $country_code
 * @property string $country_name
 * @property string $timezone_name
 * @property string $gmt_offset
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $is_deleted
 * @property integer $status
 */
class Timezone extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'timezone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_code', 'country_name', 'timezone_name', 'gmt_offset'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by', 'is_deleted', 'status'], 'integer'],
            [['country_code', 'country_name', 'timezone_name', 'gmt_offset'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'country_code' => Yii::t('app', 'Country Code'),
            'country_name' => Yii::t('app', 'Country Name'),
            'timezone_name' => Yii::t('app', 'Timezone Name'),
            'gmt_offset' => Yii::t('app', 'Gmt Offset'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
    
    /**
   * [getDropDowData generic function to get drop down data]
   *  in the respective model there should be a function called
   *  getFullName which should return the value for drop down
   * @return [array]
   */
  public static function getDropDownData($params=null)
  {
    $res = ArrayHelper::map(self::find()->where(['status'=>Self::ACTIVE,'is_deleted'=>Self::ISNOTDELETE])->all(), 'timezone_name', "timezone_name");

    //Sorting based on value
   // asort($res);

    return $res;

  }

}
