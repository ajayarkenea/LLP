<?php

namespace app\modules\settings\modules\core\controllers;

use Yii;
use app\modules\settings\modules\core\models\Config;
use app\modules\settings\modules\core\models\ConfigSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use app\controllers\ConfigDashboardController;
use yii\web\UploadedFile;
use app\modules\assets\modules\core\models\Assets;
use yii\imagine\Image;
use yii\web\Response;
use yii\base\Model;
use yii\widgets\ActiveForm;
/**
 * ConfigController implements the CRUD actions for Config model.
 */
class ConfigController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Config models.
     * @return mixed
     */
    public function actionIndex($id=null)
    {
        $searchModel  = new ConfigSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);
        $model        = new Config;
        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'model'        => $model,
        ]);
    }

    /**
     * Displays a single Config model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Config model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = $this->findModel();

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Config model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Config model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Config model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Config the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Config::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionForm($id){
        $model = $this->findModel($id);
        $data=$model->generate($id);
        $model->field=$model->field;
        $model->value=$model->value;
      //   var_dump($this->performAjaxValidation($model));
        if ($model->load(Yii::$app->request->post())) {
            if(($model->field=='company_logo')|| ($model->field=='favicon')|| ($model->field=='school_logo')|| ($model->field=='district_logo') ||($model->field=='mail_header_logo')||($model->field=='mail_footer_logo')){
             $filelist = UploadedFile::getInstance($model,'value');
                if($filelist){

                    $web= Url::base(true);
                    $filelist->saveAs('upload/branding/'.$model->field.'.'.$filelist->extension);
                    $model->value='/upload/branding/'.$model->field.'.'.$filelist->extension;
                    }else{
                      $web= Url::base(true);
                      $model->value='/upload/branding/default.jpg';
                    }
                
                      }else{
             $model->value=$model->value;
         }
             $model->save(false);
             return $this->redirect(['/config.xeo?id=settings']);
        } else {
            return $this->renderAjax('_form', [
                'model' => $model,
                'data'  => $data,
               

            ]);
        }    
    }
     /**
     * Performs ajax validation.
     * @param Model $model
     * @throws \yii\base\ExitException
     */
    protected function performAjaxValidation(Model $model)
    {
        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            echo json_encode(ActiveForm::validate($model)); 
            \Yii::$app->end();
        }
    }

   

   
}
