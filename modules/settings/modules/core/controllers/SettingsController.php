<?php
namespace app\modules\settings\modules\core\controllers;
use yii\web\Controller;
use app\modules\settings\modules\core\models\Config;
use app\modules\settings\modules\core\models\ConfigSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\controllers\ConfigDashboardController;
class SettingsController extends Controller
{

	 /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
  
   
}