<?php

namespace app\modules\settings\modules\core;

use app\components\modules\VersionModule;
use app\modules\settings\Settings;
use app\modules\settings\modules\core\components\EventHandler;

class Core extends VersionModule
{
    /** @inheritdoc */
    public static function getEventHandlers()
    {
        return [
            Settings::class=>[
                Settings::SAMPLE_EVENT=> [EventHandler::class, 'sampleEventHandler'],
            ],
        ];
    }

    /** @inheritdoc */
    public static function getUrlRules()
    {
        return [
            'GET /settings.xeo' => '/settings/settings/index',
            'GET /config.xeo' => '/settings/config/index',
            'GET /ay.list.xeo' => '/settings/acadamic-year/index',
            'GET /class.list.xeo' => '/settings/classes/index',
            'GET /st.list.xeo' => '/settings/statuses/index',
            'GET /et.list.xeo' => '/settings/email-template/index',
            'GET /prefix.list.xeo' => '/settings/prefix/index',
            'GET /school.list.xeo' => '/settings/school/index',
            'GET /department.xeo' => '/settings/department/index',
            'GET /workdays.xeo' => '/settings/work-days/index',

            
          
        ];
    }
}