<?php 
namespace app\controllers;

use Yii;
use app\modules\user_management\modules\core\models\User;
use app\models\Registeration;
use yii\web\Response;
use yii\base\Model;
use yii\widgets\ActiveForm;
use yii\web\Controller;
use app\modules\user_management\modules\core\models\Token;
use app\models\Usertype;
use app\models\Password;
use app\models\ResendForm;
use app\models\RecoveryForm;
use yii\web\NotFoundHttpException;

class RegisterController extends Controller
{   
    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ],
        ];
    }

    /**
     * Register user's account. If creates user account successful and shows success message. Otherwise
     * shows error message.
    */
    public function actionRegister()
    {
        $model = \Yii::createObject([
            'class'    => Registeration::className(),
        ]);   
        $this->performAjaxValidation($model);
          if ($model->load(Yii::$app->request->post())){
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
               // echo "<pre>"; print_r(Yii::$app->request->post()); exit;
                $model->register();
                $transaction->commit();
              \Yii::$app->session->setFlash('success', \Yii::t('app', 'A link to complete your registration has been sent to your email '.$model->email.''));
              
                return $this->redirect(['site/login']);
        } catch (Exception $e) {
                $transaction->rollBack();
                return $this->redirect(['site/login']);
                Yii::$app->session->setFlash('danger',Yii::t('app','Failed: Some Technical Error. Try again'));
            }
        }
        return $this->render('register', [
            'model'  => $model,
            
        ]);
    }

    /**
     * Confirms user's account. If confirmation was successful logs the user and shows success message. Otherwise
     * shows error message.
     * @param  integer $id
     * @param  string  $code
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionConfirm($id, $code)
    {
        $user = User::findUserById($id);
         $model=new Password();
        if ($user === null) {
            throw new NotFoundHttpException;
        }
        $status=$user->attemptConfirmation($code);
        if($status===true){
            return $this->redirect(['register/setting','id'=>Yii::$app->encryptor->encrypt($id)]);  
        }else{
             return $this->redirect(['site/login']);
        }
            
    }


    /**
     * Displays page where user can request new confirmation token. If resending was successful, displays message.
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionResend()
    {
        $model = \Yii::createObject(ResendForm::className());

        $this->performAjaxValidation($model);
        if ($model->load(\Yii::$app->request->post()) && $model->resend()) {
            return $this->redirect(['site/login']);
        }

         return $this->render('resend', [
            'model'  => $model,
           
        ]);
    }

    /**
     * Shows page where user can request password recovery.
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRequest()
    {

        $model = \Yii::createObject([
            'class'    => RecoveryForm::className(),
            'scenario' => 'request',
        ]);
        $this->performAjaxValidation($model);

        if ($model->load(\Yii::$app->request->post()) && $model->sendRecoveryMessage()) {
            return $this->redirect(['site/login']);
        }

        return $this->render('request', [
            'model' => $model,
        ]);
    }

     /**
     * Displays page where user can reset password.
     * @param  integer $id
     * @param  string  $code
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionReset($id, $code)
    {
        /** @var Token $token */
         $token = Token::find()->where([
           'user_id' => $id,
            'code'    => $code,
            'type'    => Token::TYPE_RECOVERY,
        ])->one();

        if ($token === null || $token->isExpired ) {
            \Yii::$app->session->setFlash('danger', \Yii::t('app', 'Recovery link is invalid or expired. Please try requesting a new one.'));
            return $this->redirect(['site/login']);
        }

        $model = \Yii::createObject([
            'class'    => RecoveryForm::className(),
            'scenario' => 'reset',
        ]);

        $this->performAjaxValidation($model);

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->resetPassword($token,$token->user_id)) {
            return $this->redirect(['site/login']);      
           
        }

        return $this->render('reset', [
            'model' => $model,
        ]);
    }

    /**
     * Performs ajax validation.
     * @param Model $model
     * @throws \yii\base\ExitException
     */
    protected function performAjaxValidation(Model $model)
    {
        
        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            echo json_encode(ActiveForm::validate($model));
            \Yii::$app->end();
        }
    }

    public function actionSetting($id)
    {   
        $model=new Password();
        $user = User::findUserById(Yii::$app->encryptor->decrypt($id));
        if ($user === null) {
            throw new NotFoundHttpException;
        }
        $this->performAjaxValidation($model); 
        if ($model->load(Yii::$app->request->post()))
        {
            $model->savePassword($user->id);
            Yii::$app->session->setFlash('success','Password has been set successfully...');
            //Yii::$app->user->logout(false);
            return $this->goHome();
        }
        
        return $this->render('setPassword', [
         'model' => $model,
          
        ]);
    
    }

     /**
     * Registerform get register form
     * @return form
     * @param null
     * @author AJ
     */
  
    public function successCallback($client)
    {   
        $attributes = $client->getUserAttributes();
        if($attributes!==null){
          User::setLoginOrRegister($attributes,$client->id);
        }else{
          return $this->redirect(['site/login']);
           \Yii::$app->session->setFlash('danger', \Yii::t('app', 'Technical Error ...Please try Again or Contact to our support Team'));
        }
       
    }

 
}