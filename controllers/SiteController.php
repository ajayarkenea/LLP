<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\base\Model;
use yii\widgets\ActiveForm;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SchoolImage;
use yii\authclient\AuthAction;
use yii\helpers\Url;
use app\models\Registeration;
use app\models\TblTree;
use app\models\Permission;
use app\modules\user_management\modules\core\models\User;
use app\modules\master_settings\modules\core\models\Usertype;
use app\modules\master_settings\modules\core\models\Gender;
use Aws\S3\S3Client;
use yii\web\UploadedFile;
use yii\helpers\Json ;
use yii\web\NotFoundHttpException;
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
         if (!\Yii::$app->user->isGuest){
         
       switch (Yii::$app->user->identity->user_type) {

                 case Usertype::SUPERADMIN:
                  
                  return $this->render('superadmin_dashboard.php');
                  break;

                  case Usertype::ADVISOR:
                  
                  return $this->render('advisor_dashboard.php');
                  break;
                   case Usertype::CUSTOMER:
                  
                  return $this->render('customer_dashboard.php');
                  break;
                 default:
                 return $this->redirect(['site/login']);
                 break;
            }
       }else{
         return $this->redirect(['site/login']);
       }

    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $modelContact = new ContactForm();
        $model = new LoginForm();
        $modelRegister = \Yii::createObject([
            'class'    => Registeration::className(),
        ]); 
        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        
        return $this->render('login', [
            'model' => $model,
          //  'modelContact' => $modelContact,
          //  'modelRegister' => $modelRegister
        ]);
    }
   
    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(['site/index']);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $modelContact = new ContactForm();
        if ($modelContact->load(Yii::$app->request->post())) {
            if($modelContact->save(false)){
              return 'message'; 
            }
        }
        return $this->render('contact', [
            'modelContact' => $modelContact,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

       /**
     * Performs ajax validation.
     * @param Model $model
     * @throws \yii\base\ExitException
     */
    protected function performAjaxValidation(Model $model)
    {
        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            echo json_encode(ActiveForm::validate($model));
            \Yii::$app->end();
        }
    }

     /**
     * Registerform get register form
     * @return form
     * @param null
     * @author RG
     */
  
    public function successCallback($client)
    {   
        $attributes = $client->getUserAttributes();
        if($attributes!==null){
          User::setLoginOrRegister($attributes);
        }else{
          return $this->redirect(['site/login']);
           \Yii::$app->session->setFlash('danger', \Yii::t('app', 'Technical Error ...Please try Again or Contact to our support Team'));
        }
             
    }


    
    public function actionLockScreen($url=null)
    {
   
       // if(isset(Yii::$app->user->identity->email)){
            $model = new LoginForm(); 
            $model->login = Yii::$app->user->identity->email;
             //Yii::$app->user->logout();    //set default value 
            return $this->renderAjax('lockScreen', [
                'model' => $model,
                //'previous'=>$url
            ]);  
        //}
            //   else{
            //return $this->redirect(['login']);
       // }
    }

     public function actionGetFiles($id=0){
       // echo $id;exit;
        $bredcrumbs=[];
        $data=[];
        $find=TblTree::find()->orderBy(['type'=>SORT_DESC])->where(['user_id'=>Yii::$app->user->identity->id])->andFilterWhere(['parent'=>$id])->asArray()->all();
    
        $data['list']=$find;
        $data['id']=$id;
       echo json_encode($data); exit;
    }

    public function actionGetBackFiles($id=0){
        $bredcrumbs=[];
        $data=[];
        $findParent=TblTree::find()->orderBy(['order_by'=>SORT_ASC])->where(['user_id'=>Yii::$app->user->identity->id])->andFilterWhere(['id'=>$id])->asArray()->one();
        //echo "<pre>"; print_r($findParent); exit;
        $findAll=TblTree::find()->orderBy(['order_by'=>SORT_ASC])->where(['user_id'=>Yii::$app->user->identity->id])->andFilterWhere(['parent'=>$findParent['parent'],'lvl'=>$findParent['lvl']])->asArray()->all();
     
        $data['list']=$findAll;
        $data['id']=$findParent['parent'];
       echo json_encode($data); exit;
    }

    /**
     * Creates a new Employee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     /**
     * actionUploadform render the view for to modal with ajax
     */
    public function actionCreateFolder()
    {    
        $model= new TblTree();
        if ($model->load(Yii::$app->request->post())){
          $response=[];
            $id=$model->id; 
            //find user has any root folder or not or it is 0 no parent or first folder
            if($id==0){
              $findParent=TblTree::find()->orderBy(['id'=>SORT_DESC])->where(['user_id'=>Yii::$app->user->identity->id,'type'=>'folder'])->andFilterWhere(['parent'=>$id])->asArray()->one();
              if(!empty($findParent)){
                $order_by=$findParent['order_by']+1;
              }else{
                $order_by=1;
              }  
              $lvl=0;
              $parent=0;
              
              $path=$model->name.'-'.Yii::$app->user->identity->id.'/';
              //user has already one folder then else will call
            }else{
              //find its parent folder using parent order by id asc order
              $findParent=TblTree::find()->orderBy(['id'=>SORT_DESC])->where(['user_id'=>Yii::$app->user->identity->id,'type'=>'folder'])->andFilterWhere(['parent'=>$id])->asArray()->one();
           
                //if no child node found
                if(empty($findParent)){
                
                    $data=TblTree::find()->orderBy(['id'=>SORT_ASC])->where(['user_id'=>Yii::$app->user->identity->id,'type'=>'folder'])->andFilterWhere(['id'=>$id])->asArray()->one();
                    $lvl=$data['lvl']+1;
                    $parent=$id;
                    $order_by=1;
                    $path=$data['path'].$model->name.'/';
                //if child node found
                }else{

                    $data=TblTree::find()->where(['user_id'=>Yii::$app->user->identity->id,'type'=>'folder'])->andFilterWhere(['id'=>$id])->asArray()->one();
                    $lvl=$findParent['lvl'];
                    $parent=$findParent['parent'];
                    $order_by=$findParent['order_by']+1;
                    $path=$data['path'].$model->name.'/';
                }
              } 
              $model->id=null;
              $model->lvl=$lvl;     
              $model->order_by=$order_by; 
              $model->parent=$parent; 
              $model->type='folder'; 
              $model->path=$path; 
              $model->user_id=Yii::$app->user->identity->id;
              $model->name=$model->name;
          
              if($model->save(false)){
                //config
                $s3 = new S3Client([
                    'version'     => 'latest',
                    'region'      => 'us-west-2',
                    //'version' => '2012-10-17', 
                    'bucket'=>'llp-arkenea',
                    'credentials' => [
                        'key'    => 'AKIAIOEMNY2ANX7KRBKA',
                        'secret' => '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',
                    ],]);

                // //create folder asw s3
                try{
                    $res=$s3->putObject(array( 
                        
                                   'Bucket' => 'llp-arkenea',
                                   'Key'    => $path,
                                   'Body'   => "",
                                   'ACL'    => 'private'
                                  ));
                    //print_r($res);
                    } catch (AwsException $e) {
                    // Display error message
                    echo $e->getMessage();
                    echo "\n";
                    exit;
                }
              $find=TblTree::find()->orderBy(['id'=>SORT_ASC])->where(['user_id'=>Yii::$app->user->identity->id])->andFilterWhere(['parent'=>$model->parent])->asArray()->all();
             // echo "<pre>"; print_r($find); exit;
              $response['message']='added successful';
              $response['name']=$model->name;
              $response['id']=$model->parent;
              $response['list']=$find;
              echo json_encode($response);
              exit;
              }
        }
        return $this->renderAjax('createFolder', [
            'model' => $model,
          
        ]);
    }
    
    /**
     * Creates a new Employee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     /**
     * actionUploadform render the view for to modal with ajax
     */
    public function actionUpload()
    {    
        $model= new TblTree();
       // $this->performAjaxValidation($model);
        if ($model->load(Yii::$app->request->post())){
            $filelist = UploadedFile::getInstance($model,'path');
            $response=[];
            $id=$model->id;
            if($id==0){
              $findParent=TblTree::find()->orderBy(['id'=>SORT_DESC])->where(['user_id'=>Yii::$app->user->identity->id])->andFilterWhere(['parent'=>$id])->asArray()->one();
            
              if(!empty($findParent)){
                $order_by=$findParent['order_by']+1;
              }else{
                $order_by=1;
              }  
              $lvl=0;
              $parent=0;
              
              $path=$filelist->name;
              //user has already one folder then else will call
            }else{
                 $findParent=TblTree::find()->orderBy(['id'=>SORT_DESC])->where(['user_id'=>Yii::$app->user->identity->id])->andFilterWhere(['parent'=>$id])->asArray()->one();
               
                //if no child node found
                 if(empty($findParent)){
                
                    $data=TblTree::find()->orderBy(['id'=>SORT_ASC])->where(['user_id'=>Yii::$app->user->identity->id,'type'=>'folder'])->andFilterWhere(['id'=>$id])->asArray()->one();
                    $lvl=$data['lvl']+1;
                    $parent=$id;
                    $order_by=1;
                    $path=$data['path'].$filelist->name;
                //if child node found
                }else{

                    $data=TblTree::find()->where(['user_id'=>Yii::$app->user->identity->id,'type'=>'folder'])->andFilterWhere(['id'=>$id])->asArray()->one();
                    $lvl=$findParent['lvl'];
                    $parent=$findParent['parent'];
                    $order_by=$findParent['order_by']+1;
                    $path=$data['path'].$filelist->name;
                }
              }
              $model->id=null;
               $model->lvl=$lvl; 
               $model->order_by=$order_by;
               $model->parent=$parent;  
               $model->path=$path;
               $model->type='file';
               $model->user_id=Yii::$app->user->identity->id;
               $model->name=$filelist->name;
               $model->mime_type=$filelist->getExtension();
               if($model->save(false)){
                //config
                 $s3 = new S3Client([
                    'version'     => 'latest',
                    'region'      => 'us-west-2',
                   
                    'bucket'=>'llp-arkenea',
                    'credentials' => [
                        'key'    => 'AKIAIOEMNY2ANX7KRBKA',
                        'secret' => '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',
                    ],]);

                // //create folder asw s3
                try{
                    $res=$s3->putObject(array( 
                        
                                   'Bucket' => 'llp-arkenea',
                                   'Key'    => $path,
                                   'Body'   => "",
                                   'ACL'    => 'public-read',
                                    'SourceFile' => $filelist->tempName,
                                    'StorageClass' => 'REDUCED_REDUNDANCY'
                                  ));
                   
                    } catch (AwsException $e) {
                    // Display error message
                    echo $e->getMessage();
                    echo "\n";
                    exit;
                }
            return Json::encode([
                'files' => [
                    [  
                        'id'=> $model->parent,
                        'name' => $filelist->name,
                        'size' => 111,
                        'deleteUrl' => 'image-delete?name=' .  $filelist->name,
                        'deleteType' => 'POST',
                    ],
                ],
            ]);
              }
              return '';
        }
        return '';
    }
    /**
     * Creates a new Employee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     /**
     * actionUploadform render the view for to modal with ajax
     */
    public function actionFileUpload()
    {    
        $model= new TblTree();
        return $this->renderAjax('uploadFile', [
            'model' => $model,
          
        ]);
    }
    /**
     * Creates a new Employee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     /**
     * actionUploadform render the view for to modal with ajax
     */
    public function actionShareFile(){ 
        if(!empty(Yii::$app->request->post('id'))){
        $id=Yii::$app->request->post('id');
        
        $find=  TblTree::find()->where(['id'=>$id])->one();
        $model= new Permission;
        
        $model->tbl_tree_id=Yii::$app->encryptor->encrypt($id); 
        return $this->renderAjax('share_file', [
            'model' => $model,
            'find' => $find,
          
        ]);
        }else{
         throw new NotFoundHttpException('The requested page does not exist.');
        
        }
    }
    /**
     * Creates a new Employee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     /**
     * actionUploadform render the view for to modal with ajax
     */
    public function actionShareFolder()
    {    
        $model= new TblTree();
        return $this->renderAjax('uploadFile', [
            'model' => $model,
          
        ]);
    }

    public function actionSavePermission()
    {    
        $model= new Permission();
        $response=[];
        if ($model->load(Yii::$app->request->post())){
            $tbl_tree_id=Yii::$app->encryptor->decrypt($model->tbl_tree_id); 
            $find=Permission::find()->where(['tbl_tree_id'=>$tbl_tree_id,'user_id'=>$model->user_id])->one();
            if($find==null){
                $find=new Permission;
            }
            $find->tbl_tree_id=$tbl_tree_id;
            $find->user_id=$model->user_id;
            $find->read=$model->read;
            $find->write=$model->write;
            $find->download=$model->download;
            if($find->save(false)){
               $response['message']='successful'; 
           }else{
            $response['message']='error';
           }
            
              
              echo json_encode($response);exit;
       
           } 
       
    }
      public function actionGetSharedFiles($id){
        $bredcrumbs=[];
        $data=[];
        $find=Permission::find()->joinwith(['permissionTableTree'=>function($query){
            $query->orderBy(['type'=>SORT_DESC]);
        }])->where([Permission::tableName().'.user_id'=>Yii::$app->user->identity->id])->andFilterWhere(['tbl_tree_id'=>$id])->asArray()->all();
        $retVal=[];
        for ($i=0; $i < count($find); $i++) { 
           $retVal[$i]['name']=$find[$i]['permissionTableTree']['name'];
           $retVal[$i]['lvl']=$find[$i]['permissionTableTree']['lvl'];
           $retVal[$i]['user_id']=$find[$i]['permissionTableTree']['user_id'];
           $retVal[$i]['order_by']=$find[$i]['permissionTableTree']['order_by'];
           $retVal[$i]['parent']=$find[$i]['permissionTableTree']['parent'];
           $retVal[$i]['type']=$find[$i]['permissionTableTree']['type'];
           $retVal[$i]['mime_type']=$find[$i]['permissionTableTree']['mime_type'];
        }
        $data['list']=$retVal;
        $data['id']=$id;
       echo json_encode($data); exit;
    }

 


}
