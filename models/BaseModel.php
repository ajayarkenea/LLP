<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Employee;
use app\models\Guardians;


/**
 * Base Model class for all generic function for model
 * and also model constant in all models
 */
class BaseModel extends \yii\db\ActiveRecord
{
  const ACTIVE = 1;
  const INACTIVE = 0;
  const DISABLE = 0;
  const ENABLE = 1;
  const ISNOTDELETE = 1;
  const ISDELETE = 0;
  const EMAIL = 1;
  const PHONE = 2;
  const YES=0;
  const NO=1;
  //user realted constant
  const SUPERADMIN = 1;
  const ADVISOR=2;
  const CUSTOMER=3;
  
  
  // For Checkbox 
  const CHECKED=1;
  const UNCHECKED=0;
 //For work days

  const DEFAULT_ASSET_ID=1;

   //question_fields_type_id
  const FIELD_TEXT=1;
  const FIELD_TEXTAREA=2;
  const FIELD_SELECT2=3;
  const FIELD_LIST_BOX=4;  //inactive
  const FIELD_PASSWORD=5;  //inactive
  const FIELD_CHECKBOX=6;
  const FIELD_RADIO=7;
  const FIELD_CHECK_BOX_LIST=8;
  const FIELD_RADIO_LIST=9;
  const FIELD_MULTISELECT=10;  //inactive
  const FIELD_FILEINPUT=11;     //inactive
  const FIELD_RADIO_GROUP=12;
  const FIELD_CHECK_BOX_GROUP=13;
  const FIELD_CALENDAR=14;
  const FIELD_TIME=15;            //inactive
  const FIELD_HIDDEN_INPUT=16; 
  /**
   * [getDropDowData generic function to get drop down data]
   *  in the respective model there should be a function called
   *  getFullName which should return the value for drop down
   * @return [array]
   */
  public static function getDropDownData($params=null)
  {
    $res = ArrayHelper::map(self::find()->where(['status'=>Self::ACTIVE,'is_deleted'=>Self::ISNOTDELETE])->all(), 'id', "fullName");

    //Sorting based on value
    asort($res);

    return $res;

  }
    /**
     * [getFullName get fullname for drop down ]
     * @return [string]
     */
    public function getFullName()
    {
        return $this->title;
    } 


 /**
     * htmlstatus is generic function to get html view for status
     * @return [string]
     *@author name AJ
     *@param $id integer
     *       $size [sm,lg,xs,xlg]
     *       $color[success,danger,primary,warning,grey.light,pink,yellow,purple]
     *       $icon string [font-awesome]
     *       $type string [title]
     */
     public static function gethtmlstatus($size=null,$color=null,$icon=null,$type=null)
    {    
          
       return '<span class="label label-'.$size.' label-'.$color.' arrowed-in"><i class="ace-icon fa fa-'.$icon.' bigger-120"></i>'.Yii::t('app',$type).'</span>';
         
    }
    

     /**
     * gettitle use for get value without  activedataprovider 
     * Use directly with pass id            
     * @return [string]
     */
     public static function fetchTitle($id)
    {
        $value=Self::findOne($id);
        return $value['title'];
    }

    /**
     * @return string
     */
    public static function getFlashMessage($param)
    {
        if ($param==Self::TYPE_CONFIRMATION) {
            return \Yii::t('app', 'A message has been sent to your email address. It contains your password and a confirmation link that you must click to complete registration.');
        } else if ($param==Token::TYPE_INVITATION) {
            return \Yii::t('app', 'Invitation link is invalid or expired.Pleas request to your sender again');
        } else if ($param==Self::REG) {
            return \Yii::t('app', 'A message has been sent to email address. It contains a confirmation link that user must click to complete registration.');
        } else {
            return \Yii::t('app', 'Welcome! Registration is complete.');
        }
    }

    /**
     * @param array strict 
     * @author AJ
     * @return bool Whether the user is blocked to any click,event,button or field based on user_type.
     */
    public static function isBlocked(array $ut)
    {   
      $user_type=Yii::$app->user->identity->user_type;
      if (in_array($user_type, $ut)) {
        return true;
      }
      
    }

    public static function randomInt($length,$prefix) {
    $str = "";
    $characters = array_merge(range('0','9'));
    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
        $rand = mt_rand(0, $max);
        $str .= $characters[$rand];
    }
    return $prefix.'-'.$str;
    }

    public static function getNamebyId($title)
    {
      if($title == "") {
        return null;
      }
      $obj=Self::find()->where(['title'=>$title])->one();
        return $obj['id'];
      
    }

    
    /* get one model data As object
    *@author AJ
    *@return [object]
    */
    public static function getModelOneDataObject(){
     return Self::find()->where(['status'=>Self::ACTIVE,'is_deleted'=>Self::ISNOTDELETE])->One();
    }
    /**get all model data As object
    *@author AJ
    *@return [object]
    **/
    public static function getModelAllDataObject(){
     return Self::find()->where(['status'=>Self::ACTIVE,'is_deleted'=>Self::ISNOTDELETE])->All();
    }
    /**get one model data As array
    *@author AJ
    *@return [array]
    **/
    public static function getModelOneDataArray(){
     return Self::find()->where(['status'=>Self::ACTIVE,'is_deleted'=>Self::ISNOTDELETE])->asArray()->One();
    }
    /** get all model data As array
    * @author AJ
    *@return [array]
    **/
    public static function getModelAllDataArray(){
     return Self::find()->where(['status'=>Self::ACTIVE,'is_deleted'=>Self::ISNOTDELETE])->asArray()->All();
    }

    /**
    *@author 
    *
    **/
    public static function findFullName($user_id){
      if ($user_id!==null){
        $user=User::find()->where(['id'=>$user_id])->one();
        $userType=$user->user_type;
      }else{
        $userType=Yii::$app->user->identity->user_type;
      }  
        switch ($userType) {

          case Usertype::SUPERADMIN:
           return 'Super Admin';
          break;
          case Usertype::GUARDIAN:
            $guardian=Guardians::getModelOneDataObject();
            if($guardian!==null){
             return $guardian->first_name.''.$guardian->middle_name.''.$guardian->last_name;
            }else{
              return 'not found';
            } 
          break;
          case Usertype::STUDENT:
            $student=Student::getModelOneDataObject();
            if($student!==null){
             return $student->first_name.''.$student->middle_name.''.$student->last_name;
            }else{
              return 'not found';
            } 
          break;
          default:
            $employee=Employee::getModelOneDataObject();
            if($employee!==null){
             return $employee->fname.''.$employee->mname.''.$employee->lname;
            }else{
              return 'not found';
            } 
          break;
        }
      }
      
      public function TempDelete($obj){
         $obj->is_deleted=1;
         $obj->save(false);
      }  

      // find id from table using title
    public static function findId($Id){
       $key=trim($Id);
      $model=Self::findOne(['title'=>$key]);
      if($model==null){
        return null;
      }else{
        return $model->id;
      }
    }

    public static function getAssetsId($id){
      $model=Self::findOne(['id'=>$id]);
       if($model==null){
        return Self::DEFAULT_ASSET_ID;
      }else{
        return $model->assets_id;
      }
    }    

}