<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\models;

use Yii;
use yii\base\Model;
use app\modules\user_management\modules\core\models\User;
use app\modules\user_management\modules\core\models\Token;

/**
 * ResendForm gets user email address and validates if user has already confirmed his account. If so, it shows error
 * message, otherwise it generates and sends new confirmation token to user.
 *
 * @property User $user
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class RecoveryForm extends Model
{
    /** @var string */
    public $email;

    public $newPassword;
    public $confirmPassword;

    /** @var User */
    private $_user;

     /**
     * @param Mailer $mailer
     * @param Finder $finder
     * @param array  $config
     */
   

    /**
     * @return User
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findUserByEmail($this->email);
        }

        return $this->_user;
    }

     /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'email'    => \Yii::t('app', 'Email'),
            'password' => \Yii::t('app', 'Password'),
        ];
    }

    /** @inheritdoc */
  public function scenarios()
    {
        return [
            'request' => ['email'],
            'reset'   => ['confirmPassword','newPassword']
        ];
    }

    /** @inheritdoc */
    public function rules()
    {    
       // print_r($this->_user); 
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist','targetClass' => 'app\modules\user_management\modules\core\models\User',
             'message' => \Yii::t('app', 'Invalid credentials!')],
            // ['email', function () {
            //     if ($this->getIsConfirmed() != false ) {
            //         $this->addError('email', \Yii::t('app', 'This account has already been confirmed'));
            //     }
            // }],
             [['newPassword', 'confirmPassword'], 'required'],
            ['newPassword', 'string', 'min' => 6],
            ['confirmPassword', 'compare','compareAttribute'=>'newPassword', 'message'=>'New Password and Confirm Password are not matching'],
        ];
    }

 

    /**
     * Sends recovery message.
     *
     * @return bool
     */
    public function sendRecoveryMessage()
    {

        if ($this->validate()) {
            
            /** @var Token $token */
            $token = \Yii::createObject([
                'class'   => Token::className(),
                'user_id' => $this->user->id,
                'type'    => Token::TYPE_RECOVERY
            ]);
             
            Token::deleteAll('user_id = :id  AND type = :int', [':id' => $token->user_id,':int'=>Token::TYPE_RECOVERY]);
              $token->code=User::generate(20);
            $token->save(false);
            Yii::$app->sendmail->sendRecoveryMessage($token->user_id,$this->email, $token->code);
            \Yii::$app->session->setFlash('info', \Yii::t('app', 'An email has been sent with instructions for resetting your password'));
            return true;
        }

        return false;
    }

      /**
     * @return bool Whether the user is confirmed or not.
     */
    public function getIsConfirmed()
    {
         $user= User::findUserByEmail($this->email); 
      
            if(!isset($user->confirmed_at)){
                return false;
            }else{
                return true;
            }
       
        
    }

    /**
     * Resets user's password.
     *
     * @param  Token $token
     * @return bool
     */
    public function resetPassword($token,$user_id)
    {
        if (!$this->validate() || $token === null) {
            return false;
        }
          
        if (Self::updatePassword($this->confirmPassword,$user_id)) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Your password has been changed successfully.'));
            $token->delete();
        } else {
            \Yii::$app->session->setFlash('danger', \Yii::t('app', 'An error occurred and your password has not been changed. Please try again later.'));
        }

        return true;
    }

    /**
     * @inheritdoc
     */
     public static function updatePassword($confirmPassword,$user_id)
    {  
        $user=User::findOne($user_id);
        $hash=$user->setPassword($confirmPassword);
        return (bool) $user->updateAttributes(['password_hash' => $hash]);
    }

    
}
