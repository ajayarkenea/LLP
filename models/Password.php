<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\data\ActiveDataProvider;
use app\modules\user_management\modules\core\models\User;
use app\modules\user_management\modules\core\models\Token;
use app\modules\user_management\modules\core\models\Guardians;
use app\models\PasswordCode;

class Password extends BaseModel
{
	 public $password;
	 public $newPassword;
	 public $confirmPassword;
   public $resetcode;
   public $email;
   public $username;
   public $token;

    private $_user = false;
     /**
     * @inheritdoc
     */
    public function rules()
    {
        return  [
        [['password', 'newPassword','confirmPassword','username','email','token'], 'required' ],
        //['login','required',
          //  'message' => \Yii::t('app', 'Please enter your username')],
        //[['login'], 'string', 'max'=>200],
        ['password', 'validator'],
        ['email', 'email'],
        ['token', 'validateToken'],
        ['newPassword', 'same'],
        ['username', 'unique','targetClass' => 'app\modules\user_management\modules\core\models\User',
       'message' => \Yii::t('app', 'This username has already been taken')],

        ['email', 'unique','targetClass' => 'app\modules\user_management\modules\core\models\User',
             'message' => \Yii::t('app', 'Please choose another email.It is in use')],
        //['newPassword', 'unique','compareAttribute'=>'password', 'message'=>'Previous Password and new Password should not ...'],
        ['confirmPassword', 'compare','compareAttribute'=>'newPassword', 'message'=>'New Password and Confirm Password are not matching'],
        
        ];
    }

     /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'email' => Yii::t('app', 'Login/Profile email'),
            
        ];
    }

    /**
    *validate check is current password is matching or not
    *@author AJ
    */
    public function validator($attribute, $params) {
        $user = $this->getUser();
        if (!$user || !$user->validatePassword($this->password)) {
         $this->addError($attribute, Yii::t('app', 'Current password is not matching'));
        }
    }
      /**
    *same check previous and new password should be different
    *@author RG
    */
    public function validateToken($attribute, $params)
    {
        $token=Token::find()->where(['type'=>Token::TYPE_RESET_EMAIL_CONFIRMATION,'user_id' => Yii::$app->user->identity->id,'code'=>$this->token])->one();
      if($token===null){
        
      $this->addError($attribute,Yii::t('app', 'Code is invalid or exoired.Please try Again...'));
      }

    }

    /**
    *same check previous and new password should be different
    *@author RG
    */
    public function same($attribute, $params)
    {
      
      if($this->newPassword==$this->password){
      
      $this->addError($attribute,Yii::t('app', 'Please choose another new password...'));
      }

    }

    /**
     * Finds user by [[id]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findOne(Yii::$app->user->id);
       }

        return $this->_user;
    }

       
    /**
    *savePassword changing password for user inside profile setting
    *
    */
    public function savePassword($id=null)
    { 
      if($id===null){
          $id=Yii::$app->user->id;
      }
      $user= new User();
      $user=User::findOne($id);
      $user->password_hash=Yii::$app->getSecurity()->generatePasswordHash($this->confirmPassword);
      $user->save(false);
       return;
    }

     /**
    *savePassword changing password for user inside profile setting
    *
    */
    public function setUsername()
    { 
      $user=User::findOne(Yii::$app->user->id);
      $user->username=$this->username;
      $user->save(false);
       return;
    }

     /**
    *savePassword changing password for user inside profile setting
    *
    */
    public function setEmail()
    { 
      $user=User::findOne(Yii::$app->user->id);
      $user->email=$this->email;
      if($user->save(false)){
         $guardian=Guardians::find()->where(['user_id'=>$user->id])->one();
         if($guardian!==null){
          $guardian->email=$this->email;
          $guardian->save(false);
         }
      }  
       return;
    }
     /**
    *savePassword changing password for user inside profile setting
    *
    */
    public function findUsername()
    { 
      $user=User::findOne(Yii::$app->user->id);
      return $user->username;
      
    }

      /**
    *savePassword changing password for user inside profile setting
    *
    */
    public function findEmail()
    { 
      $user=User::findOne(Yii::$app->user->id);
      return $user->email;
      
    }


    /**
    *sendRecoveryMessage send password reset email 
    *@author AJ
    *
    */
    public function sendRecoveryMessage()
    {  
       $user=MdlUser::findByUsername($this->username);
       $office_email=MdlUser::findOne(['user_id'=>$user->id,'is_primary'=>MdlUser::PRIMARY]);
       if($office_email!==null){
       $password=PasswordCode::findOne(['user_id'=>$user->id]);
        if($password===null){
          $this->createPasswordCode($user,$office_email);
        }else{
            PasswordCode::deleteAll('user_id = :id', [':id' => $user->id]);
            $this->createPasswordCode($user,$office_email);
        }
    }else{
           \Yii::$app->session->setFlash('danger', \Yii::t('app', 'Sorry!...You have not primary email'));
    }
    }

    public function savePasswordReset()
    {
        $user= new User();
        $user=User::find()->where(['username'=>$this->username])->all();
        $this->setPassword($this->newPassword);
        $user=User::findOne($user[0]['id']);
        $user->password=$this->password;
        $user->save();
        return;

    }

    /**createPasswordCode save data into db
    * @param [object] user and useremail
    *@author AJ
    *
    **/
    public function createPasswordCode($user,$office_email)
    {
     $password_code= new PasswordCode;
     $password_code->user_id=$user->id;
     $password_code->code=Yii::$app->getSecurity()->generateRandomString(20);
        if($password_code->save()){
          Yii::$app->sendmail->sendRecoveryMessage($user->id,$office_email->title,$password_code->code);
          \Yii::$app->session->setFlash('success', \Yii::t('app', 'An email has been sent with instructions for resetting your password'));
       }
    }

    /**resetPassword change user password
    * @param [object of PasswordCode] $token 
    *@author AJ
    *
    **/
    public function resetPassword($token)
    {
        if ($token === null) {
            return false;
        }
          
        if (MdlUser::resetPassword($this->confirmPassword,$token->user_id)) {
            \Yii::$app->session->setFlash('success', \Yii::t('app', 'Your password has been changed successfully.'));
            $token->delete();
        } else {
            \Yii::$app->session->setFlash('danger', \Yii::t('app', 'An error occurred and your password has not been changed. Please try again later.'));
        }

        return true;
    }
}