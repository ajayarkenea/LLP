<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_tree".
 *
 * @property int $id Unique tree node identifier
 * @property int $root Tree root identifier
 * @property int $lvl Nested set level / depth
 * @property int $parent
 * @property string $unique_id
 * @property string $name The tree node name / label
 * @property int $type 1=folder,2=file
 * @property string $mime_type
 * @property string $icon The icon to use for the node
 * @property int $icon_type Icon Type: 1 = CSS Class, 2 = Raw Markup
 * @property int $active Whether the node is active (will be set to false on deletion)
 * @property int $selected Whether the node is selected/checked by default
 * @property int $disabled Whether the node is enabled
 * @property int $readonly Whether the node is read only (unlike disabled - will allow toolbar actions)
 * @property int $visible Whether the node is visible
 * @property string $path
 */
class TblTree extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_tree';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['root', 'lvl', 'parent', 'type', 'icon_type', 'active', 'selected', 'disabled', 'readonly', 'visible'], 'integer'],
            [['lvl', 'name','file'], 'required'],
            [['unique_id', 'name'], 'string'],
            [['mime_type'], 'string', 'max' => 50],
            [['icon'], 'string', 'max' => 255],
            [['path'], 'string', 'max' => 250],
            [['file','path','id'], 'safe'],
             ['path', 'file','maxSize' => 1024 * 1024 * 1,
             'tooBig' => 'File size should not more then 10 MB...'],
            ['path', 'file', 'extensions' => ['pdf']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'root' => Yii::t('app', 'Root'),
            'lvl' => Yii::t('app', 'Lvl'),
            'parent' => Yii::t('app', 'Parent'),
            'unique_id' => Yii::t('app', 'Unique ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'mime_type' => Yii::t('app', 'Mime Type'),
            'icon' => Yii::t('app', 'Icon'),
            'icon_type' => Yii::t('app', 'Icon Type'),
            'active' => Yii::t('app', 'Active'),
            'selected' => Yii::t('app', 'Selected'),
            'disabled' => Yii::t('app', 'Disabled'),
            'readonly' => Yii::t('app', 'Readonly'),
            'visible' => Yii::t('app', 'Visible'),
            'path' => Yii::t('app', 'Path'),
        ];
    }
}
