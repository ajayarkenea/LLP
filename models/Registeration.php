<?php

namespace app\models;

use Yii;
use app\models\BaseModel;
use app\modules\user_management\modules\core\models\User;
use app\modules\training\modules\core\models\Company;
use app\modules\user_management\modules\core\models\Token;
use app\modules\user_management\modules\core\models\Employee;
use yii\helpers\Url;
/**
 * Registration Form 
 */
class Registeration  extends BaseModel
{   
    
    public $user_type; 
    public $first_name;
    public $middle_name;
    public $last_name;
    public $mobile;
    public $offers;
    public $terms;
    public $company_name;
    public $email;
    public $username;
    public $password_hash;
    public $registration_ip;
     

    /**
    * @inheritdoc
    */
    public function rules()
    {
       return [
       [['email','first_name','last_name','user_type','mobile'], 'required'],
       ['email', 'filter', 'filter' => 'trim'],
       ['email', 'email'],
       ['email', 'unique','targetClass' => 'app\modules\user_management\modules\core\models\User',
       'message' => \Yii::t('app', 'This email address has already been taken')],
       [['user_type','middle_name'],'safe'],
        
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
       return [
            'email'    => \Yii::t('app', 'Login'),
            'username' => \Yii::t('app', 'Username'),
            'password_hash' => \Yii::t('app', 'Password'),
            'email'=> Yii::t('app', 'Email'),
            'user_type' => Yii::t('app', 'Login Type'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'company_name' => Yii::t('app', 'Company Name'),
            'mobile' => Yii::t('app', 'Contact No'),
        
        ];
    }

      /**
     * Creates new confirmation token and sends it to the user.
     *
     * @return bool
     */
    public function register()
    {
        if (!$this->validate()) {
            return false;
        }
            $user  = new User;
            $employee = new Employee;
            $employer = new Company;
            $baseurl=\Yii::$app->request->BaseUrl;
            $web= \Yii::getAlias('@webroot');
            $user = new User();
            $user->email=$this->email;
            $user->user_type=$this->user_type;
            //$user->password_hash=Yii::$app->getSecurity()->generatePasswordHash('mbis2018');
         //   echo $this->user_type; exit;
            if($user->save(false)){
              if($this->user_type==self::EMPLOYER){
                $time=time();
                $employer->unique_id='CMP-'.$time;
                $employer->user_id=$user->id;
                $employer->first_name=$this->first_name;
                $employer->last_name=$this->last_name;
                $employer->name=$this->company_name;
                $employer->email=$this->email;
                $employer->phone=$this->mobile;
                $employer->assets_id=$assetsId =Employee::DEFAULT_ASSET_ID;
                if($employer->save(false)){
                  $assetsUserDir = \Yii::getAlias('@assetsUserDir'); 
                    if (!mkdir($assetsUserDir.'/'.$employer->unique_id.'', 0777, true)){
                     die('Failed to create folders...');
                    }
                }
              }else{
                $time=time();
                $employee->unique_id='EMP-'.$time;
                $employee->user_id=$user->id;
                $employee->fname=$this->first_name;
                $employee->lname=$this->last_name;
                $employee->email=$this->email;
                $employee->phone=$this->mobile;
                $employee->assets_id=$assetsId =Employee::DEFAULT_ASSET_ID;
                if($employee->save(false)){
                  $assetsUserDir = \Yii::getAlias('@assetsUserDir'); 
                    if (!mkdir($assetsUserDir.'/'.$employee->unique_id.'', 0777, true)){
                      die('Failed to create folders...');
                    }
                } 
              }
              $token=new Token;
              $token->user_id=$user->id;
              $token->code=$user->generate(20);
              $token->created_at=time();
              $token->type=Token::TYPE_CONFIRMATION;
                if($token->save(false)){ 
                  Yii::$app->sendmail->sendConfirmationMessage($user->id,$user->email, $token->code);
                }
            }        
           return true;
    }  
    
}
