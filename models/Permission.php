<?php

namespace app\models;

use Yii;
use app\models\BaseModel;
use app\models\TblTree;
use yii\helpers\ArrayHelper;
use app\modules\user_management\modules\core\models\User;
/**
 * This is the model class for table "permission".
 *
 * @property int $id
 * @property int $user_id
 * @property int $tbl_tree_id
 * @property int $read
 * @property int $write
 * @property int $download
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $status
 */
class Permission extends BaseModel
{  
    public $name;
    public $email;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'permission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id','read', 'write', 'download', 'created_by', 'updated_by', 'status'], 'integer'],
            [['created_at', 'updated_at','tbl_tree_id','email'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Email',
            'tbl_tree_id' => 'Tbl Tree ID',
            'read' => 'Read',
            'write' => 'Write',
            'download' => 'Download',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'status' => 'Status',
        ];
    }
     /**
     * @inheritdoc
     */
    public function getPermissionTableTree()
    {
        return $this->hasOne(TblTree::className(), ['id' => 'tbl_tree_id']);
    }
    /**
     * @inheritdoc
     */
     public static function getDropDownData($params = null) 
    { 
      return ArrayHelper::map(User::find()->where(['user_type'=>[3]])->andwhere(['not in','id',Yii::$app->user->identity->id])->all(), 'id', 'email');
    } 
}
