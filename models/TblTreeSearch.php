<?php

namespace app\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblTree;

/**
 * TblTreeSearch represents the model behind the search form of `app\models\TblTree`.
 */
class TblTreeSearch extends TblTree
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'lvl', 'order_by', 'parent', 'icon_type', 'active', 'disabled', 'readonly', 'visible'], 'integer'],
            [['unique_id', 'name', 'type', 'mime_type', 'icon', 'path'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $query = TblTree::find()->orderBy(['lvl'=>SORT_ASC])->where(['user_id'=>Yii::$app->user->identity->id])->andFilterWhere(['parent'=>$id]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'lvl' => $this->lvl,
            'order_by' => $this->order_by,
            'parent' => $this->parent,
            'icon_type' => $this->icon_type,
            'active' => $this->active,
            'disabled' => $this->disabled,
            'readonly' => $this->readonly,
            'visible' => $this->visible,
        ]);

        $query->andFilterWhere(['like', 'unique_id', $this->unique_id])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'mime_type', $this->mime_type])
            ->andFilterWhere(['like', 'icon', $this->icon])
            ->andFilterWhere(['like', 'path', $this->path]);

        return $dataProvider;
    }
}
