<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\models;

use Yii;
use yii\base\Model;
use app\modules\user_management\modules\core\models\User;
use app\modules\user_management\modules\core\models\Token;

/**
 * ResendForm gets user email address and validates if user has already confirmed his account. If so, it shows error
 * message, otherwise it generates and sends new confirmation token to user.
 *
 * @property User $user
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class ResendForm extends Model
{
    /** @var string */
    public $email;

    /** @var User */
    private $_user;

     /**
     * @param Mailer $mailer
     * @param Finder $finder
     * @param array  $config
     */
   

    /**
     * @return User
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findUserByEmail($this->email);
        }

        return $this->_user;
    }
    

    /** @inheritdoc */
    public function rules()
    {    
       // print_r($this->_user); 
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist','targetClass' => 'app\modules\user_management\modules\core\models\User',
             'message' => \Yii::t('app', 'There is no account with this email address')],
            ['email', function () {
                if ($this->getIsConfirmed() != false ) {
                    $this->addError('email', \Yii::t('app', 'This account has already been confirmed'));
                }
            }],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'email' => \Yii::t('app', 'Email'),
        ];
    }

    /**
     * Creates new confirmation token and sends it to the user.
     *
     * @return bool
     */
    public function resend()
    {
        $user=new User; 
        if (!$this->validate()) {
            return false;
        }
        /** @var Token $token */
        $token = \Yii::createObject([
            'class'   => Token::className(),
            'user_id' => $this->user->id,
            'code' => $user->generate(20),
            'type'    => Token::TYPE_CONFIRMATION,
        ]);
         Token::deleteAll('user_id = :id  AND type = :int', [':id' => $token->user_id,':int'=>Token::TYPE_CONFIRMATION]);
         $token->save(false);
        
          Yii::$app->sendmail->sendConfirmationMessage($token->user_id,$this->email, $token->code);
          \Yii::$app->session->setFlash('info', \Yii::t('app', 'A message has been sent to your email address. It contains a confirmation link that you must click to complete registration.'));

        return true;
    }

      /**
     * @return bool Whether the user is confirmed or not.
     */
    public function getIsConfirmed()
    {
         $user= User::findUserByEmail($this->email); 
      
            if(!isset($user->confirmed_at)){
                return false;
            }else{
                return true;
            }
       
        
    }
}
