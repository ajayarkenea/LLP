<?php

namespace app\models;

use Yii;
use yii\base\Model; 
use app\models\Token;
use app\modules\user_management\modules\core\models\User;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{

    /** @var bool Whether user has to confirm his account. */
    public $enableConfirmation = true;

    /** @var bool Whether to allow logging in without confirmation. */
    public $enableUnconfirmedLogin = false;
    //public $confirmed_at=true;
    public $login;
    public $password;
   // public $otp;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['login', 'password'], 'required'],
            // rememberMe must be a boolean value
            // [['login', 'password'],'on'=>'login'],
          
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
             [['login', 'password'], 'required'],
            ['login', 'trim'],
          
            ['login', function ($attribute) {
                if ($this->user !== null) {
                   // $confirmationRequired = $this->enableConfirmation && !$this->enableUnconfirmedLogin;
                    if ($this->getIsConfirmed()==false) {
                        $this->addError($attribute, \Yii::t('app', 'You need to confirm your email address'));
                    }
                    if ($this->getIsBlocked()==false) {
                        $this->addError($attribute, \Yii::t('app', 'Your account has been blocked'));
                    }
                }
            }],
            ['rememberMe', 'boolean'],
        ];

    }
     /**
     * @return bool Whether the user is confirmed or not.
     */
    public function getIsConfirmed()
    {
         $user= User::findUserByUsernameOrEmail($this->login); 
      
            if(!isset($user->confirmed_at)){
                return false;
            }else{
                return true;
            }
       
        
    }

    /**
     * @return bool Whether the user is blocked or not.
     */
    public function getIsBlocked()
    {   
        $user= User::findUserByUsernameOrEmail($this->login); 
            if(!isset($user->blocked_at)){
                 return true;
            }else{
               return false;
           }
        
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect email or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
         
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function checkUserAuth()
    {
        
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {   
        
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
     /** @inheritdoc */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->_user = User::findUserByUsernameOrEmail($this->login);
            return true;
        } else {
            return false;
        }
    }
    
}
