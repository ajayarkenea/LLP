<?php
namespace app\models;
use Yii;
     
    class Product extends \kartik\tree\models\Tree
    {
        public $logo;
        /**
         * @inheritdoc
         */
        public static function tableName()
        {
            return 'tbl_tree';
        }    
        
        /**
         * Override isDisabled method if you need as shown in the  
         * example below. You can override similarly other methods
         * like isActive, isMovable etc.
         */
        // public function isDisabled()
        // {
        //     if (Yii::$app->user->username !== 'superadmin') {
        //         return true;
        //     }
        //     return parent::isDisabled();
        // }
    }
    ?>