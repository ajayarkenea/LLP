<?php

namespace app\models;

/**
 * @Author: ajkosh
 * @Date:   2018-06-11 13:04:38
 * @Last Modified by:   ajkosh
 * @Last Modified time: 2018-06-11 13:05:12
 */


use Yii;
use yii\base\Model;

class Csv extends Model
{
	public $file;
	public $isNewRecord;
	
	public function rules()
	{
		return [
		[['file'], 'file', 'extensions' => 'xlsx'],
		['file','safe']
		];
	}

	public function attributeLabels()
    {
        return [
            'csvfile' => Yii::t('app', 'Excel File'),
            
        ];
    }

}	