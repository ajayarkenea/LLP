<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblTree */

$this->title = Yii::t('app', 'Create Tbl Tree');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tbl Trees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-tree-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
