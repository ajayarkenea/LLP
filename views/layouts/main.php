<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\Modal;
use cybercog\yii\googleanalytics\widgets\GATracking;
use mdm\admin\components\MenuHelper;
use app\modules\settings\modules\core\models\Config;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
        <title><?= Html::encode($this->title) ?></title>
        <?= Html::csrfMetaTags() ?>
        <?php $this->head() ?>
        <script>
      var baseUrl = "<?php echo \Yii::$app->request->BaseUrl?>";
       var guest= "<?php echo \Yii::$app->user->isGuest?>";
    </script>
    <?= GATracking::widget([
   // 'trackingId' => Yii::$app->appconfig->getGoogleAnalyticId(),
    ]) ?>
    <link rel="shortcut icon" href="<?php echo Yii::$app->appconfig->getFavicon() ?>" type="image/x-icon">
        <link rel="icon" href="<?php echo Yii::$app->appconfig->getFavicon() ?>" type="image/x-icon">
    </head>
    <?php //echo \mgcode\sessionWarning\widgets\SessionWarningWidget::widget([
   //'warnBefore'=>60,
   //'logoutUrl'=>['site/logout','data-method' => 'post'],
//]); ?>
    <?php $items=MenuHelper::getAssignedMenu(Yii::$app->user->id,null, null, true);
    
   
    ?>
    
<body class="smart-style-6 menu-on-top">
<?php $this->beginBody() ?>
   <?php if(!Yii::$app->user->isGuest) { ?>

        <!-- #HEADER -->
  <!-- #HEADER -->
  <div id="loadings" style="display: none;">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
        <header id="header">
            <div id="logo-group">

                <!-- PLACE YOUR LOGO HERE -->
                 <span id="logo"> <img src="<?php echo Yii::$app->appconfig->getComapnyLogo() ?>" alt="LLP"> </span>
                <!-- END LOGO PLACEHOLDER -->

                <!-- Note: The activity badge color changes when clicked and resets the number to 0
                     Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications -->
               <!--  <span id="activity" class="activity-dropdown"> <i class="fa fa-user"></i> <b class="badge"> 21 </b> </span> -->

                <!-- AJAX-DROPDOWN : control this dropdown height, look and feel from the LESS variable file -->
               <!--  <div class="ajax-dropdown"> -->

                    <!-- the ID links are fetched via AJAX to the ajax container "ajax-notifications" -->
                   <!--  <div class="btn-group btn-group-justified" data-toggle="buttons">
                        <label class="btn btn-default">
                            <input type="radio" name="activity" id="ajax/notify/mail.html">
                            Msgs (14) </label>
                        <label class="btn btn-default">
                            <input type="radio" name="activity" id="ajax/notify/notifications.html">
                            notify (3) </label>
                        <label class="btn btn-default">
                            <input type="radio" name="activity" id="ajax/notify/tasks.html">
                            Tasks (4) </label>
                    </div> -->

                    <!-- notification content -->
                   <!--  <div class="ajax-notifications custom-scroll">

                        <div class="alert alert-transparent">
                            <h4>Click a button to show messages here</h4>
                            This blank page message helps protect your privacy, or you can show the first message here automatically.
                        </div>

                        <i class="fa fa-lock fa-4x fa-border"></i>

                    </div> -->
                    <!-- end notification content -->

                    <!-- footer: refresh area -->
                  <!--   <span> Last updated on: 12/12/2013 9:43AM
                        <button type="button" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading..." class="btn btn-xs btn-default pull-right">
                            <i class="fa fa-refresh"></i>
                        </button> </span> -->
                    <!-- end footer -->

               <!--  </div> -->
                <!-- END AJAX-DROPDOWN -->
            </div>

            <!-- #PROJECTS: projects dropdown -->
            <!-- <div class="project-context hidden-xs">

                <span class="label">Projects:</span>
                <span class="project-selector dropdown-toggle" data-toggle="dropdown">Recent projects <i class="fa fa-angle-down"></i></span> -->

                <!-- Suggestion: populate this list with fetch and push technique -->
               <!--  <ul class="dropdown-menu">
                    <li>
                        <a href="javascript:void(0);">Online e-merchant management system - attaching integration with the iOS</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">Notes on pipeline upgradee</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">Assesment Report for merchant account</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="javascript:void(0);"><i class="fa fa-power-off"></i> Clear</a>
                    </li>
                </ul> -->
                <!-- end dropdown-menu-->

          <!--   </div> -->
            <!-- end projects dropdown -->
            
            <!-- #TOGGLE LAYOUT BUTTONS -->
            <!-- pulled right: nav area -->
            <div class="pull-right">
                
                <!-- collapse menu button -->
                <div id="hide-menu" class="btn-header pull-right">
                    <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
                </div>
                <!-- end collapse menu -->
                
                <!-- #MOBILE -->
                <!-- Top menu profile link : this shows only when top menu is active -->
                <ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
                    <li class="">
                        <a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown"> 
                            <img src="<?php echo \Yii::$app->request->BaseUrl?>/images/1.jpg" alt="John Doe" class="online" />  
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="<?php  echo Url::toRoute(['/user_management/profile-setting/setting']);?>" class="padding-10 padding-top-0 padding-bottom-0"><i class="fa fa-cog"></i> Setting</a>
                            </li>
                            <li class="divider"></li>
                            <!-- <li>
                                <a href="#ajax/profile.html" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> <u>P</u>rofile</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="toggleShortcut"><i class="fa fa-arrow-down"></i> <u>S</u>hortcut</a>
                            </li>
                            <li class="divider"></li> -->
                            <li>
                                <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="launchFullscreen"><i class="fa fa-arrows-alt"></i> Full <u>S</u>creen</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php  echo Url::toRoute(['/site/logout']);?>" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout" data-method="post"><i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <!-- logout button -->
                <div id="logout" class="btn-header transparent pull-right">
                    <span> <a href="login.html" title="Sign Out" data-action="userLogout" data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i class="fa fa-sign-out"></i></a> </span>
                </div>
                <!-- end logout button -->

                <!-- search mobile button (this is hidden till mobile view port) -->
                <div id="search-mobile" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
                </div>
                <!-- end search mobile button -->
                
                <!-- #SEARCH -->
                <!-- input: search field -->
                <!-- <form action="#ajax/search.html" class="header-search pull-right">
                    <input id="search-fld" type="text" name="param" placeholder="Find reports and more">
                    <button type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                    <a href="javascript:void(0);" id="cancel-search-js" title="Cancel Search"><i class="fa fa-times"></i></a>
                </form> -->
                <!-- end input: search field -->

                <!-- fullscreen button -->
                <div id="fullscreen" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
                </div>
                <!-- end fullscreen button -->

                <!-- multiple lang dropdown : find all flags in the flags page -->
             <!--    <ul class="header-dropdown-list hidden-xs">
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="img/blank.gif" class="flag flag-us" alt="United States"> <span> US</span> <i class="fa fa-angle-down"></i> </a>
                        <ul class="dropdown-menu pull-right">
                            <li class="active">
                                <a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-us" alt="United States"> English (US)</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-fr" alt="France"> Français</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-es" alt="Spanish"> Español</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-de" alt="German"> Deutsch</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-jp" alt="Japan"> 日本語</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-cn" alt="China"> 中文</a>
                            </li>   
                            <li>
                                <a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-it" alt="Italy"> Italiano</a>
                            </li>   
                            <li>
                                <a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-pt" alt="Portugal"> Portugal</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-ru" alt="Russia"> Русский язык</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"><img src="img/blank.gif" class="flag flag-kr" alt="Korea"> 한국어</a>
                            </li>                       
                            
                        </ul>
                    </li>
                </ul> -->
                <!-- end multiple lang -->

            </div>
            <!-- end pulled right: nav area -->

        </header>
        <!-- END HEADER -->
        <!-- END HEADER -->

        <!-- #NAVIGATION -->
        <!-- Left panel : Navigation area -->
        <!-- Note: This width of the aside area can be adjusted through LESS/SASS variables -->
        <aside id="left-panel">

            <!-- User info -->
            <div class="login-info">
                <span> <!-- User image size is adjusted inside CSS, it should stay as is --> 
                    
                    <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
                        <span>
                            <?php echo Yii::$app->user->identity->username ?>
                        </span>
                        <i class="fa fa-angle-down"></i>
                    </a> 
                    
                </span>
            </div>
            <!-- end user info -->

            <!-- NAVIGATION : This navigation is also responsive

            To make this navigation dynamic please make sure to link the node
            (the reference to the nav > ul) after page load. Or the navigation
            will not initialize.
            -->
            <nav>
                <!-- 
                NOTE: Notice the gaps after each icon usage <i></i>..
                Please note that these links work a bit different than
                traditional href="" links. See documentation for details.
                -->

                <ul style="">
                 <li class="">
                       <a href="<?php  echo Url::toRoute('/site/index');?>" title="Dashboard"><i class="fa-lg fa-fw fa fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
                    </li>
                <?php for ($i=0; $i < count($items); $i++) { ?>
                      
                    <li class="">

                        <a href="#" title="<?php echo $items[$i]['label']?>"> <i class="fa fa-cog" style="color: grey"></i><span class="menu-item-parent"><?php echo $items[$i]['label']?></span><b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b></a>
                          <ul style="display: none;">
                         <?php $sub=$items[$i]['items'];
                             for ($j=0; $j < count($sub) ; $j++) { 
                                   $url=$sub[$j]['url']['0'];
                                   ?>
                            <li class="">
                                <a href="<?php  echo Url::toRoute(''.$url.'');?>" title="<?php echo $sub[$j]['label']?>"><i class="fa fa-cogs" style="color: grey"></i>&nbsp;<span class="menu-item-parent"><?php echo $sub[$j]['label']?></span></a>

                            </li>
                       
                       
                          <?php } ?>
                             </ul> 
                    </li>                
                    <?php } ?>
                </ul>
            </nav>


            <span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

        </aside>
        <!-- END NAVIGATION -->
        
        <!-- MAIN PANEL -->
        <div id="main" role="main">

            <!-- RIBBON -->
            <div id="ribbon">

                <span class="ribbon-button-alignment"> 
                    <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                        <i class="fa fa-refresh"></i>
                    </span> 
                </span>

                <!-- breadcrumb -->
                 <?= Breadcrumbs::widget([
                    
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
                <!-- end breadcrumb -->

                <!-- You can also add more buttons to the
                ribbon for further usability

                Example below:

                <span class="ribbon-button-alignment pull-right">
                <span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
                <span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
                <span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
                </span> -->

            </div>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">
                  <?php } ?>
             <?php   foreach(Yii::$app->session->getAllFlashes() as $key => $message) {
                echo '<div id="alertmsg" class="alert alert-' . $key . '">' . $message . "<a class='close' data-dismiss='alert'>×</a></div>\n";
            }

            ?>
            
                   <?= $content ?>
                
                <!-- end widget grid -->

            </div>
            <!-- END MAIN CONTENT -->
        </div>
        

        <?php if(!Yii::$app->user->isGuest) {?>
             <?php  $session = Yii::$app->session;?>
        <div class="page-footer">
            <div class="">
                 <div class="col-xs-12 col-sm-6">
                    <span class="txt-color-blueLight"> <span class="brand hidden-xs"> Developed & Managed By Company © <?php echo date('Y');?></span></span> 
                </div>
            <?php } ?>
            </div>
            <!-- end row -->
        </div>
        <!-- END FOOTER -->

        <!-- #SHORTCUT AREA : With large tiles (activated via clicking user name tag)
             Note: These tiles are completely responsive, you can add as many as you like -->
        
        <!-- END SHORTCUT AREA -->

        <!--================================================== -->



       
      

<?php $this->endBody() ?>
</body>
</html>

<?php $this->endPage() ?>
     <?php
    Modal::begin([
            'id' => 'draggable',
            'size'=>'modal-lg',
        ]);
   echo  " <div class='panel panel-default'>
  <div class='panel-heading'>    <div class=pull-right'>
  
    </div>
      <header>
                <span class='widget-icon'>  </span>
                <h2> <?= Html::encode($this->title) ?></h2>
        </header>
    
    <div class='clearfix'></div></div>
       
    <div id='modal-body'></div>
 </div>
 ";
    Modal::end();
    ?>  

