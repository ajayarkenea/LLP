<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\master_settings\modules\core\models\Relationship;
use yii\helpers\Url;
use kartik\select2\Select2;
use app\modules\settings\modules\core\models\Config;
use app\assets\LoginAsset;

LoginAsset::register($this);

$this->title = Yii::t('app', 'PulsEdu-Register');
?>


<!DOCTYPE html>
<html lang="en-us" id="extr-page">
<head>

  <title> TecnicaGuru|Registeration</title>
 

  </head>
  
  <body class="animated fadeInDown">
  
   
    <div id="main" role="main">
      <div id="content" class="container">
        <div class="row">
         
          <div class="col-lg-5 col-md-offset-4" id="down-padding">
                        <div class="well no-padding">
                        <?php $form = ActiveForm::begin([
                          'options' => ['class' => 'smart-form client-form'],
                               'id' => 'dynamic-form',
                               'fieldConfig' => [
                               'template' => "\n{input}{error}",
                              ],
                               'enableAjaxValidation'   => true,
                               'enableClientValidation' => false,
                               'action'=>['register/register']]);?>
                       
                                <header>
                                  <img src="<?php echo \Yii::$app->request->BaseUrl?>/images/login_logo.png" class="login-img" alt="">
                             
                                </header>

                                <fieldset>
                                    <section>
                                    
                                     <label class="label"><?php echo  Yii::t('app', 'Registration Form')?></label>
                                        <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                           <?= $form->field($model, 'email', [
                                             'template' => "\n{input}\n{hint}\n{error}"
                                             ])->textInput(['placeholder'=>'Enter Email ']);  ?>
                                                </label>
                                            </section>
                                       <section>
                   
                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                              <?= $form->field($model, 'first_name', [
                                              'template' => "\n{input}\n{hint}\n{error}"
                                              ])->textInput(['placeholder'=>'Enter First Name ']);  ?>
                                             </label>
                                    </section>
                                    <section>
                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                             <?= $form->field($model, 'middle_name', [
                                            'template' => "\n{input}\n{hint}\n{error}"
                                            ])->textInput(['placeholder'=>'Enter Middle Name']);  ?>
                                             </label>
                                    </section>

                                    <section>
                                    <label class="input"> <i class="icon-append fa fa-user"></i>
                                      <?= $form->field($model, 'last_name', [
                                      'template' => "\n{input}\n{hint}\n{error}"
                                      ])->textInput(['placeholder'=>'Enter Last Name']);  ?>
                                        </label>
                                    </section>                                
                                    <section>
                                      <label class="input">
                                        <?php echo $form->field($model, 'gender')->widget(Select2::classname(), [
                                        'data' => ['1' => 'Male',
                                              '2' => 'Female'],
                                        'language' => 'en',
                                        'options' => ['placeholder' => Yii::t('app','Select Gender')],
                                          'pluginOptions' => [
                                            'allowClear' => false
                                          ],
                                          //'disabled' => !$model->isNewRecord,
                                          ]);?>

                                         
                                        </label>   
                                    </section>

                                    
                                   
                                </fieldset>

                               <!--  <fieldset>
                                <section>
                                  
                                  <label class="checkbox">
                                    <input name="terms" id="terms" type="checkbox">
                                    <i></i>I agree with the <a href="#" data-toggle="modal" data-target="#myModal"> Terms and Conditions </a></label>
                                  </section>
                                 

                                </fieldset> --> 
                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                          <?php echo  Yii::t('app', 'Register')?>
                                    </button>
                                </footer>

                            <?php ActiveForm::end(); ?>

                        </div>
                         <p class="text-center">
                                                <?= Html::a(Yii::t('app', 'Already registered?  Sign In '), ['/site/login']) ?>
                                              </p>
                       <!-- <h5 class="text-center">- Or sign up using -</h5>
                         <ul class="list-inline text-center">
                            <li>
                                <a href="<?php //echo Url::toRoute(['/site/auth','authclient'=>'google_login']);?>" class="btn btn-danger btn-circle btn-lg"><i class="fa fa-google"></i></a>

                            </li>
                        </ul> -->
                    </div>
          
        </div>

      </div>
     
    </div>
  
  </body>
</html>


