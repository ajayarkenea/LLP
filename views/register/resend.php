<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\settings\modules\core\models\Config;
use app\assets\LoginAsset;

LoginAsset::register($this);
/* @var $this \yii\web\View */
/* @var $content string */

?>



<!DOCTYPE html>
<html lang="en-us" id="extr-page">
<head>
 
  <title> TecnicaGuru|Resend Confirmation Link Form</title>
  
  </head>
  
  <body class="animated fadeInDown">

   
    <div id="main" role="main">
      <div id="content" class="container">
        <div class="row">
         
 <div class="col-md-5 col-md-offset-4">
                        <div class="well no-padding">
                            <?php $form = ActiveForm::begin([
                                'options' => ['class' => 'smart-form client-form'],
                                            'id'      => 'resend-form',
                                            'enableAjaxValidation'   => true,
                                            'enableClientValidation' => false
                                ]);?>

                                <header>
                                    <img src="<?php echo \Yii::$app->request->BaseUrl?>/images/login_logo.png" class="login-img" alt="">
                                                                 
                                </header>

                                <fieldset>
                                    <section>
                                        <label class="label"> <?php echo  Yii::t('app', 'Resend Confirmation Link Form')?></label>
                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                           <?= $form->field($model, 'email',[
                                          'template' => "\n{input}\n{hint}\n{error}"
                                           ])->textInput(['autofocus' => true,'placeholder' => 'Enter Email Here'])->label('') ?>
                                            </section>
                                           
                                                </fieldset>
                                                <footer>
                                                    <button type="submit" class="btn btn-primary">
                                                     <?php echo  Yii::t('app', 'Continue')?>
                                                    </button>
                                                </footer>

                                           <?php ActiveForm::end(); ?>
                                            </div>
                                              <p class="text-center">
                                                <?= Html::a(Yii::t('app', 'Sign In Here'), ['/site/login']) ?>
                                              </p>
                                         
                                           

                                        </div>
          
        </div>

      </div>
     
    </div>
  
  </body>
</html>
