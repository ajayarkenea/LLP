<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\assets\LoginAsset;
use yii\bootstrap\Carousel;
use app\modules\settings\modules\core\models\Config;
$this->title = Yii::t('app', 'PulsEdu-Set Password');
LoginAsset::register($this);
?>

<!DOCTYPE html>
<html lang="en-us" id="extr-page">
<head>

  <title> TecnicaGuru|Reset Password Form</title>
  

  </head>
  
  <body class="animated fadeInDown">
  
    <div id="main" role="main">
      <div id="content" class="container">
        <div class="row">
         
<div class="col-md-5 col-md-offset-5">
                        <div class="well no-padding">
                            <?php $form = ActiveForm::begin([
                                'options' => ['class' => 'smart-form client-form'],
                                            'id'      => 'resend-form',
                                            'enableAjaxValidation'   => true,
                                            'enableClientValidation' => false
                                ]);?>

                                <header>
                                 <?php echo  Yii::t('app', 'Set Password')?>
                                
                                </header>

                                <fieldset>
                                    <section>
                                         <label class="label"> <?php echo  Yii::t('app', 'Password Set Form')?></label>
                                      <label class="input"> <i class="icon-append fa fa-lock"></i>
                                        <?= $form->field($model, 'newPassword',[
                                                  'template' => "\n{input}{error}",])->passwordInput(['maxlength' => 256,'placeholder' => 'Enter New Password']) ?>
                                                </label>
                                              </section>
                                              <section>
                                                <label class="input"> <i class="icon-append fa fa-lock"></i>
                                              <?= $form->field($model, 'confirmPassword', [
                                                 'template' => "\n{input}{error}"])->passwordInput(['maxlength' => 256,'placeholder' => 'Enter Confirm Password']) ?>
                                           
                                            
                                          
                                            
                                            </section>
                                           
                                                </fieldset>
                                                <footer>
                                                    <button type="submit" class="btn btn-primary">
                                                      <?php echo  Yii::t('app', '  SetPassword ')?>
                                                    </button>         
                                                </footer>

                                           <?php ActiveForm::end(); ?>
                                            </div>
                                              <p class="text-center">
                                                <?= Html::a(Yii::t('app', 'Sign In Here ..'), ['/site/login']) ?>
                                              </p>
                                                                                   

                                        </div>
          
        </div>

            </div>
            
        </div>
          
        </div>
  
  </body>
</html>






