<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use kartik\datecontrol\DateControl;
use app\assets\CustomerAsset;
CustomerAsset::register($this);
?>

   <div class="upload-form">
  <?php $form = ActiveForm::begin([
    'id'=>'folder-form',
        //'action'=>['upload-invoice'],
        //'enableAjaxValidation'=>false,
        'options' => ['class' => 'form-smart',
        'enctype' => 'multipart/form-data'
        ],

        'fieldConfig' => [
          'template' => "{label}\n<div class=\"col-xs-8\">{input}{error}</div>",
          'labelOptions' => ['class' => 'col-xs-2 control-label'],
        ],
        ]); ?>
  <div class="panel panel-default">

      <div class="panel-default"><!-- widgetBody -->

      <div class="panel-heading">

        <span class="panel-title-address">Folder create</span>

        <div class="clearfix"></div>

      </div>

      <div class="panel-body">

        <div class="row">     
        <div class="col-md-8">                      
                    <?= $form->field($model, 'name', [
                        'template' => "\n{input}\n{hint}\n{error}"
                        ])->textInput(['placeholder'=>Yii::t('app', 'Enter Title Here')]);  ?>
                         <?php echo $form->field($model, 'id')->hiddenInput()->label(false);?>
        </div> 
     
                             </div>

      </div>

      </div>
  </div>
           <div class="form-actions">
                          <div class="row">
                            <div class="col-md-7 pull-right">
                              <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' =>  'btn btn-success','id' =>'add-btn']) ?>
                              <?= Html::submitButton(Yii::t('app', 'Remove'), ['class' =>  'btn btn-warning btn-hide','id' => 'rmv-hide']) ?>
                              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove bigger-110"></i>Close</button>
                            </div>
                          </div>
                        </div>
              <?php ActiveForm::end(); ?>
  </div>

<script type="text/javascript">
    
 $('#folder-form').on('beforeSubmit', function(e) {
alert('processing');
    var form = $(this);

    var formData = form.serialize();

    $.ajax({

        url: form.attr("action"),

        type: form.attr("method"),

        data: formData,

        success: function (data) {
   
              $('#draggable').modal('toggle');
             $.getScript("js/customer.js" ) 
  .done(function( script, textStatus ) {
   // alert( textStatus );
  })
  .fail(function( jqxhr, settings, exception ) {
   // alert(exception);
     getFolders();
    $( "div.log" ).text( "Triggered ajaxError handler." );
});
         
        },
        error: function () {
  
            alert("Error Please try again");
            
        }

    });

}).on('submit', function(e){

    e.preventDefault();

});



</script>

