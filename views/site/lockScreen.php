<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\assets\LockAsset;
LockAsset::register($this);
?>
<head>
<style type="text/css">
	

</style>
	</head>

                  <div class="logo">
					<h1 class="semi-bold"><img src="img/logo-o.png" alt=""> SmartAdmin</h1>
				</div>
				<div>
					<img src="img/avatars/sunny-big.png" alt="" width="120" height="120">
					<div>
						<h1><i class="fa fa-user fa-3x text-muted air air-top-right hidden-mobile"></i>John Doe <small><i class="fa fa-lock text-muted"></i> &nbsp;Locked</small></h1>
						<p class="text-muted">
							<a href="mailto:simmons@smartadmin"><?php echo $model->login;?></a>
						</p>

						 <?php $form = ActiveForm::begin([
                 'enableAjaxValidation'   => true,
                   'enableClientValidation' => false,
                   'action'=>['login'],
                   'id' => 'dynamic-form',
                'options' => ['class' => 'lockscreen animated flipInY'],
                
                ]);?>
                
                          <?= $form->field($model, 'password', [
                            'template' => "\n{input}\n{hint}\n{error}"
                            ])->passwordInput(['placeholder'=>Yii::t('app', 'Password')]);  ?>
                             <?= $form->field($model, 'login', [
                        'template' => "\n{input}\n{hint}\n{error}"
                        ])->textInput(['placeholder'=>Yii::t('app', 'Email or Username')]);  ?>
                         
                 
             <footer>
                
                  <button type="submit" class="btn btn-primary">
                    <?php echo  Yii::t('app', ' Sign in   ')?>
                  </button>
                </footer>

                <?php ActiveForm::end(); ?>
						<p class="no-margin margin-top-5">
							Logged as someone else? <a href="login.html"> Click here</a>
						</p>
					</div>

				</div>
				<p class="font-xs margin-top-5">
					Copyright SmartAdmin 2014-2020.

				</p>
            
       