<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use app\assets\EmployeeAsset;
use app\models\Employee;
use yii\helpers\ArrayHelper;
use mdm\admin\components\Helper;
//EmployeeAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Files');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="widget-grid" class="">
<div class="row">
		<article class="col-sm-12 sortable-grid ui-sortable">
			<!-- new widget -->
			<div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" role="widget">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header role="heading" class="ui-sortable-handle">
				

					<ul id="myTab" class="nav nav-tabs pull-leftin">
						<li class="active">
							<a data-toggle="tab" href="#s1" aria-expanded="false"><i class="fa fa-clock-o"></i> <span class="hidden-mobile hidden-tablet">Files</span></a>
						</li>

						<li class="">
							<a data-toggle="tab" href="#s2" aria-expanded="false"><i class="fa fa-facebook"></i> <span class="hidden-mobile hidden-tablet">Upload</span></a>
						</li>

					
					</ul>

				<span class="jarviswidget-loader" style="display: none;"><i class="fa fa-refresh fa-spin"></i></span>
<div class="jarviswidget-ctrls" role="menu">    <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a> </div></header>

				<!-- widget div-->
				<div class="no-padding" role="content">
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">

						test
					</div>
					<!-- end widget edit box -->

					<div class="widget-body">
						<!-- content -->
						<div id="myTabContent" class="tab-content">
							<div class="tab-pane fade active in padding-10 no-padding-bottom" id="s1">
								<div class="row no-space">
									<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            //'id',
            //'form_id',
            //'cust_field_id',
            //'user_id',
          
            // 'photo',
             'name',
            // 'mname',
             // 'lname',
             // 'dob',
             // 'email:email',
            // 'phone',
            // 'department_id',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'is_deleted',
            // 'status',

            // ['class' => '\kartik\grid\ActionColumn',
            //          'template' => Helper::filterActionColumn('{view}&nbsp;{update}&nbsp;{delete}'),                    
            //         'urlCreator' => function ($action, $model, $url, $index) {
            //              $hash=Yii::$app->encryptor->encrypt($model->id); 
            //              if ($action === 'view') {
            //                 return Url::to(['employee/view','id'=>$hash]);
            //              }else if($action === 'update'){
            //               return Url::to(['employee/update','id'=>$hash]);
            //              }else if($action === 'delete'){
            //               return Url::to(['employee/delete','id'=>$hash]);
            //              }
            //         },
            // ],

        ],
         'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="white fa fa-users "></i> Files </h3>',
            'type'=>'primary',
            'before'=>Html::a(Yii::t('app', '<i class="glyphicon glyphicon-plus"></i> Create {modelClass}', ['modelClass' => 'Employee',]), ['create'], ['class' => 'btn btn-primary']) 
            .' '.Html::button('<i class="fa fa-upload"></i> Upload ', [
                                                     'title' => Yii::t('app', 'Upload'),'href'=>"#",'class'=>'uploadmodal btn btn-warning']),

            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Search', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>true,
            'pager'=>false
            ],
            'toolbar' => [
              //'{export}'
              ],
            'exportConfig' => [
            GridView::HTML =>[],
            //  GridView::PDF =>[],
            GridView::TEXT => [],
            GridView::EXCEL => [],
            ],
            'bootstrap'=>true,
            'bordered'=>false,
            'striped'=>true
    ]); ?>
									</div>
								</div>

								

							
							<!-- end s1 tab pane -->

							<div class="tab-pane fade" id="s2">
								
								

							</div>
							<!-- end s2 tab pane -->

						</div>

						<!-- end content -->
					</div>

				</div>
				<!-- end widget div -->
			</div>
			<!-- end widget -->

		</article>
	</div>
</section>
	