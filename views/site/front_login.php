<?php

/**
 * @Author: ajkosh
 * @Date:   2018-06-13 14:29:36
 * @Last Modified by:   ajkosh
 * @Last Modified time: 2018-06-13 14:29:53
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Carousel;
use app\assets\LoginAsset;
use app\modules\settings\modules\core\models\Config;

LoginAsset::register($this);
$this->title = Yii::t('app', 'TecnicaGuru Corporate Trainiing Portal ');
?>

        <div class="row">
        
          <div class="col-lg-5 col-md-offset-4" id="down-padding">
            <div class="well no-padding">
              <?php $form = ActiveForm::begin([
                 'enableAjaxValidation'   => true,
                   'enableClientValidation' => false,
                   'id' => 'dynamic-form',
                'options' => ['class' => 'smart-form client-form'],
                
                ]);?>
                <header>
                   <img src="<?php echo \Yii::$app->request->BaseUrl?>/images/login_logo.png" class="login-img" alt="">

                </header>
                <fieldset>
                  <section>
                    
                    <label class="input"> <i class="icon-append fa fa-user"></i>
                      <?= $form->field($model, 'login', [
                        'template' => "\n{input}\n{hint}\n{error}"
                        ])->textInput(['placeholder'=>Yii::t('app', 'Email or Username')]);  ?>
                  </section>
                  <section>
                   
                      <label class="input"> <i class="icon-append fa fa-lock"></i>
                          <?= $form->field($model, 'password', [
                            'template' => "\n{input}\n{hint}\n{error}"
                            ])->passwordInput(['placeholder'=>Yii::t('app', 'Password')]);  ?>
                          <div class="note">
                            <a href="<?php  echo Url::toRoute('/register/request');?>"><?php echo  Yii::t('app', 'Forgot password?')?></a>
                          </div>
                  </section>
                          
                </fieldset>
             <footer>
                
                  <button type="submit" class="btn btn-primary">
                    <?php echo  Yii::t('app', ' Sign in   ')?>
                  </button>
                </footer>

                <?php ActiveForm::end(); ?>
            </div>
           
          </div>
          <div class=" col-lg-5 col-md-offset-4">
          <div class="well no-padding">
            <br>
              <p class="text-center">
              <?= Html::a(Yii::t('app', 'New Registration'), ['/register/register']) ?>
            </p>
            <p class="text-center">
              <?= Html::a(Yii::t('app', 'Didn\'t receive confirmation message?'), ['/register/resend']) ?>
            </p>
            <h5 class="text-center"> <?php echo  Yii::t('app', ' - Or sign in using - ')?></h5>
              <ul class="list-inline text-center">
                <li>
                  <a href="<?php echo Url::toRoute(['/site/auth','authclient'=>'google_login']);?>" class="btn btn-danger btn-circle btn-lg"><i class="fa fa-google"></i></a>
                </li>
              </ul>
</div>
         </div>
        </div>

    
