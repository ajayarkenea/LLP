<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use kartik\datecontrol\DateControl;
use app\assets\CustomerAsset;
use dosamigos\fileupload\FileUploadUI;
use yii\web\JsExpression;
use app\models\Permission;
use app\modules\user_management\modules\core\models\User;
CustomerAsset::register($this);
?>

   <div class="upload-form">
  <?php $form = ActiveForm::begin([
    'id'=>'folder-form',
       'action'=>'site/save-permission',
        // 'enableAjaxValidation'=> true,
        // 'enableClientValidation' => false,
        'options' => ['class' => 'form-smart',
        'enctype' => 'multipart/form-data'
        ],

        // 'fieldConfig' => [
        //   'template' => "{label}\n<div class=\"col-xs-8\">{input}{error}</div>",
        //   'labelOptions' => ['class' => 'col-xs-2 control-label'],
        // ],
        ]); ?>
  <div class="panel panel-default">

      <div class="panel-default"><!-- widgetBody -->

      <div class="panel-heading">

        <span class="panel-title-address">Permission</span>
         
        <div class="clearfix"></div>

      </div>

      <div class="panel-body">

        <div class="row"> 
        <div class="col-md-8 " >
          <div class="alert alert-info alert-block">
                     
                     <h4 class="alert-heading">Info!</h4>
                      You will share <?= $find->name;?>
                      </div>
            <div class="col-md-8" id="message"></div>          
           </div>  
          <div class="col-md-6">                      
             <?php echo $form->field($model, 'user_id')->widget(Select2::classname(), [
                    'data' => Permission::getDropDownData(),
                    'language' => 'en',
                    'options' => ['placeholder' => Yii::t('app','Select User...'),'multiple'=>false],
                    'pluginOptions' => [
                    'allowClear' => false
                    ],
                    //'disabled' => !$model->isNewRecord,
                    ]);?>
                     <?php echo $form->field($model, 'email')->textInput()->label('Custom');?>
            <?= $form->field($model, 'read')->checkbox(array(
          'label'=>'',
          'labelOptions'=>array('style'=>'padding:5px;'),
          'disabled'=>false
          ))
          ->label('Read Permission'); ?>
          <?= $form->field($model, 'write')->checkbox(array(
          'label'=>'',
          'labelOptions'=>array('style'=>'padding:5px;'),
          'disabled'=>false
          ))
          ->label('Write Permission'); ?>
          <?= $form->field($model, 'download')->checkbox(array(
          'label'=>'',
          'labelOptions'=>array('style'=>'padding:5px;'),
          'disabled'=>false
          ))
          ->label('Download Permission'); ?>
          <?php echo $form->field($model, 'tbl_tree_id')->hiddenInput()->label(false);?>
        </div> 
     
                             </div>

      </div>

      </div>
  </div>
           <div class="form-actions">
                          <div class="row">
                            <div class="col-md-7 pull-right">
                             <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' =>  'btn btn-success','id' =>'add-btn']) ?>
                              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove bigger-110"></i>Close</button>
                            </div>
                          </div>
                        </div>
              <?php ActiveForm::end(); ?>
  </div>
  <script type="text/javascript">
    
 $('#folder-form').on('beforeSubmit', function(e) {
alert('processing');
    var form = $(this);

    var formData = form.serialize();

    $.ajax({

        url: form.attr("action"),

        type: form.attr("method"),

        data: formData,
        dataType: 'json',
        success: function (data) {
       
          if(data.message=="error"){

            var msg=$('<div class="alert alert-danger alert-block">'+
                     
                     '<h4 class="alert-heading">Error!</h4>'+
                     'Technical error.Please Try Again'+
                      '</div>');
          
                    $('#message').append(msg); 
          }else{
           
               $('#draggable').modal('toggle');
          }
   },
        error: function () {
  
            alert("Error Please try again");
            
        }

    });

}).on('submit', function(e){

    e.preventDefault();

});



</script>