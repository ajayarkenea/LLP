<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
use kartik\datecontrol\DateControl;
use app\assets\CustomerAsset;
use dosamigos\fileupload\FileUploadUI;
use yii\web\JsExpression;
CustomerAsset::register($this);
?>

   <div class="upload-form">
  <?php $form = ActiveForm::begin([
    'id'=>'folder-form',
       
        // 'enableAjaxValidation'=> true,
        // 'enableClientValidation' => false,
        'options' => ['class' => 'form-smart',
        'enctype' => 'multipart/form-data'
        ],

        'fieldConfig' => [
          'template' => "{label}\n<div class=\"col-xs-8\">{input}{error}</div>",
          'labelOptions' => ['class' => 'col-xs-2 control-label'],
        ],
        ]); ?>
  <div class="panel panel-default">

      <div class="panel-default"><!-- widgetBody -->

      <div class="panel-heading">

        <span class="panel-title-address">File upload</span>

        <div class="clearfix"></div>

      </div>

      <div class="panel-body">

        <div class="row">     
        <div class="col-md-8">                      
                   <?= FileUploadUI::widget([
    'model' => $model,
    'attribute' => 'path',
    'url' => ['site/upload', 'id' => $model->id],
    'gallery' => false,
    'fieldOptions' => [
        'accept' => ['.png']
            ],
    'clientOptions' => [
        'maxFileSize' => 2000000,
         'acceptFileTypes' => new JsExpression('/(\.|\/)(jpe?g|png|pdf|doc|xls|xlsx|csv)$/i'),
    ],
    // ...
    'clientEvents' => [
        'fileuploaddone' => 'function(e, data) {
                             $.getScript("js/customer.js" )
                            .done(function( script, textStatus ) {
                             // alert( textStatus );
                            })
                            .fail(function( jqxhr, settings, exception ) {
                            // getFolders();
                              $( "div.log" ).text( "Triggered ajaxError handler." );
                          })
                            }',
        'fileuploadfail' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
                            'filedeletedone' => 'function(e, data) {
                             $.getScript("js/customer.js" )
                            .done(function( script, textStatus ) {
                              alert( textStatus );
                            })
                            .fail(function( jqxhr, settings, exception ) {
                              alert(exception);
                              $( "div.log" ).text( "Triggered ajaxError handler." );
                          })
                            }',
        'filedeletefail' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
    ],
]); ?>
                         <?php echo $form->field($model, 'id')->hiddenInput()->label(false);?>
        </div> 
     
                             </div>

      </div>

      </div>
  </div>
           <div class="form-actions">
                          <div class="row">
                            <div class="col-md-7 pull-right">
                            
                              <button type="button" class="btn btn-danger modalClose"><i class="fa fa-remove bigger-110 "></i>Close</button>
                            </div>
                          </div>
                        </div>
              <?php ActiveForm::end(); ?>
  </div>

<script type="text/javascript">
  $('.modalClose').on('click',function(){
  
    $.getScript("js/customer.js" )
                            .done(function( script, textStatus ) {
                                 $('#draggable').modal('toggle');
                            })
                            .fail(function( jqxhr, settings, exception ) {
                               getFolders();
                                  $('#draggable').modal('toggle');
                              $( "div.log" ).text( "Triggered ajaxError handler." );
                          })
})
</script>